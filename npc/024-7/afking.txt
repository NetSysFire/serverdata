// TMW2 scripts.
// Author:
//    TMW-BR
//    Jesusalva
// Description:
//    AFK Cap quest (Lv 47)

024-7,40,24,0	script	AF King	NPC_PLAYER,{
	function AFPrologue;
	function AFGrinding;
	function AFKingGelid;
	function AFItemList;
	function AFDeliver;

	if (BaseLevel < 47) {
		npctalkonce l("*AFK: I am Away From Keyboard*");
		end;
	}
	.@q=getq(FrostiaQuest_AFKCap);
	@flag02474024=false;

	switch (.@q) {
	case 0: // Mission not started
		mesn;
		mesq l("I'm back, I'm here, I'm great! I am the AF King Arthur!");
		next;
		do {
			mesn;
			mesq l("But can I help you?");
			next;
			select
				l("Good, I need to talk with the King."),
				l("You have a nice hat."),
				l("Do you know where I can level up?"),
				rif(@flag02474024, l("I would like a hat like yours.")),
				l("Nothing, good bye!");
			mes "";
			switch (@menu) {
			case 1:
				AFKingGelid();
                break;
			case 2:
				AFPrologue();
				@flag02474024=true;
                break;
			case 3:
				AFGrinding();
                break;
			case 4:
				setq FrostiaQuest_AFKCap, 1;
				AFItemList();
                break;
			default:
				close;
			}
		} while (true);
		break;
	case 1: // Mission accepted
		AFItemList();
		break;
	default: // Mission complete
		npctalkonce l("*AFK: I am Away From Keyboard*");
		end;
		break;
	}

    closedialog;
    goodbye;
    close;

function AFPrologue {
	mesn;
	mesq l("No one is more there than here than me! Look at my hat, it symbolizes that I am an useless player!");
	next;
	mesn;
	mesq l("And if you listen to Tulimshar's Professor lecture wearing it, you'll gain more experience!");
	next;
	mesn;
	mesq l("Basically, no one else disturbs me anymore when I'm busy snoring in class! ^.^");
	next;
	return;
}

function AFGrinding {
	mesn;
	mesq l("Uhh... Not really! I love to kill Yetis, but sometimes they are really shy!");
	next;
	mesn;
	mesq l("They must be hiding in some cave... It's a shame!");
	next;
	return;
}

function AFKingGelid {
	mesn;
	mesq l("Uhm, I'm not the king of Frostia, I'm the king of all useless adventurers.");
	next;
	mesn;
	mesq l("I just go out removing swords from rocks, and then throwing them again at the lake. All that while I drink coffee.");
	next;
	mesn;
	mesq l("If you need to talk with King Gelid, he should be in the castle north here.");
	next;
	return;
}

function AFItemList {
	mesn;
	mesq l("Sure, no problem! For this awesome hat which I can make it myself, you'll need to bring me a few items!");
	next;
	mes ".:: "+l("Item List")+" ::.";
	mesc l("@@/@@ @@", countitem(SilkCocoon), 300, getitemlink(SilkCocoon));
	mesc l("@@/@@ @@", countitem(CobaltHerb), 100, getitemlink(CobaltHerb));
	mesc l("@@/@@ @@", countitem(CottonCloth), 25, getitemlink(CottonCloth));
	mesc l("@@/@@ @@", countitem(BlueDye),      1, getitemlink(BlueDye));
	mesc l("@@/@@ @@", format_number(Zeny), format_number(5000), "GP");
	next;
	// FIXME Deprecate .@ scope on flag
	if (@flag02474024) {
		mesn;
		mesq l("Now I'll be waiting!");
		close;
	}
	select
		l("Okay, I'll be back!"),
		l("I have everything you've asked for.");
	mes "";
	if (@menu == 2)
		AFDeliver();
	return;
}

function AFDeliver {
	inventoryplace AFKCap, 1;
	if (
		countitem(SilkCocoon)  < 300 ||
		countitem(CobaltHerb)  < 100 ||
		countitem(CottonCloth) <  25 ||
		countitem(BlueDye)     <   1 ||
		Zeny < 5000) {
			mesn;
			mesq l("I'll feed you to the Moubootaur %%e");
			setparam(MaxHp, readparam(MaxHp)-100);
			die();
			close;
	}
	Zeny-=5000;
	delitem SilkCocoon, 300;
	delitem CobaltHerb, 100;
	delitem CottonCloth, 25;
	delitem BlueDye, 1;
	getitem AFKCap, 1;
	setq FrostiaQuest_AFKCap, 2;
	mesn;
	mesq l("As I had the spare materials I've already did yours in advance.");
	next;
	mesn;
	mesq l("There you go. Enjoy it!");
    next;
	return;
}

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, AFKCap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe); // TODO
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 0);
    npcsit;

    .sex = G_MALE;
    .distance = 4;
    end;
}
