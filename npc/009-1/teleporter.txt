// TMW2 Script
// Authors:
//    Jesusalva
// Description:
//    Link portals to soul menhirs like the teleporters from old
//    The price is temporary. This feature got in because no ship in Nivalis Port
//    PS. Anise => “Aisen” Anagram


009-1,113,89,0	script	#WarpGateHalin	NPC_NO_SPRITE,1,0,{
    end;

OnTouch:
    TeleporterGate(TP_HALIN);
    close;


OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

