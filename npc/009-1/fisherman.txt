// TMW-2 Script
// Originals: TMW BR
//
// Author:
//    Jesusalva
// Description:
//    Forest Bow Quest. DO NOT RENAME THE NPC.

009-1,116,106,0	script	Fisherman	NPC_FISHERMAN_HALI,{
    .@q=getq(HurnscaldQuest_ForestBow);
    if (BaseLevel < 30) goto L_Weak;
    mesn;
    mesq l("Hello. Ah, bad luck. I am three days without fishing a single carp.");
    next;
    mesn strcharinfo(0);
    menu
        l("Any fishing tips for me?"),-,
        rif(.@q == 1 && getq3(HurnscaldQuest_ForestBow) < 99, l("I need a string, but not just any string, a really good string.")), L_String;

    mes "";
    mesn;
    mesq l("Well, yes. Look this river. The water is not moving.");
    next;
    mesn;
    mesq l("If the water doesn't moves, you will never fish anything. Ah, fat luck...");
    close;

L_Weak:
    mesn;
    mesq l("...");
    next;
    mesn;
    mesq l("I am trying to fish here without success. You're weak, do not bother me.");
    close;

L_String:
    .@k=getq3(HurnscaldQuest_ForestBow);
    mes "";
    switch (.@k) {
        case 0:
            mesn;
            mesq l("Well, that's easy to do. For starters, I need 1.000 GP as payment, and 10 @@ to make the string.", getitemlink(Root));
            next;
            mesn;
            mesq l("That's not everything, though. I am having a bad time fishing. Bring me a @@. I know, it is rare. Good luck with it.", getitemlink(GrassCarp));
            next;
            mesn;
            mesq l("And one last thing I'll want. See these @@? I don't like them, so I'll have you to kill 40 of them for me.", getmonsterlink(FireGoblin));
            next;
            mesn strcharinfo(0);
            mes l("o.o \"You're being unreasonable. That's way too much!\"");
            next;
            mesn;
            mesq l("To make the string you want, I need a special oil, which I make myself. It's hard to do, so I'll spend time I could be using to fish or help the city guard.");
            next;
            mesn;
            mesq l("I'll make your life easier, I'm also accepting 20 @@ or @@ instead the Grass Carp. Go now, pal.", getitemlink(CommonCarp), getitemlink(Roach));
            setq3 HurnscaldQuest_ForestBow, 10;
            close;
            break;
        case 50:
            mesn;
            mesq l("Yo, good joob killing the Fire Goblins. Do you have everything I've asked for?");
            next;
            goto L_Finish;
        default:
            mesn;
            mesq l("You have:");
            if (countitem(GrassCarp) > 0)
                mes l("@@/1 @@", countitem(GrassCarp), getitemlink(GrassCarp));
            else if (countitem(Roach) > countitem(CommonCarp))
                mes l("@@/20 @@ (or @@)", countitem(Roach), getitemlink(Roach), getitemlink(CommonCarp));
            else
                mes l("@@/20 @@ (or @@)", countitem(CommonCarp), getitemlink(CommonCarp), getitemlink(Roach));

            mes l("@@/10 @@", countitem(Root), getitemlink(Root));
            mes l("@@/40 slayed @@", .@k-10, getmonsterlink(FireGoblin));
            mes l("@@/1000 GP", Zeny);
            close;
    }
    close;

L_Finish:
    mesq l("You need:");
    if (countitem(GrassCarp) > 0)
        mes l("@@/1 @@", countitem(GrassCarp), getitemlink(GrassCarp));
    else if (countitem(Roach) > countitem(CommonCarp))
        mes l("@@/20 @@ (or @@)", countitem(Roach), getitemlink(Roach), getitemlink(CommonCarp));
    else
        mes l("@@/20 @@ (or @@)", countitem(CommonCarp), getitemlink(CommonCarp), getitemlink(Roach));

    mes l("@@/10 @@", countitem(Root), getitemlink(Root));
    mes l("40/40 slayed @@", getmonsterlink(FireGoblin));
    mes l("@@/1000 GP", Zeny);
    mes "";
    select
        l("Not yet."),
        rif(countitem(GrassCarp) >= 1, l("Yes, I'll pay with Grass Carp.")),
        rif(countitem(CommonCarp) >= 20, l("Yes, I'll pay with Common Carp.")),
        rif(countitem(Roach) >= 20, l("Yes, I'll pay with Roach."));
    if (@menu == 1)
        close;

    if (countitem(Root) < 10 || Zeny < 1000) {
        mesn;
        mesq l("Well, you forgot the most important: The material I need to do the string.");
        next;
        mesn;
        mesq l("I am not a half-word man, so I won't take your fish. Come back with the money and the Roots, and I'll make the string for you.");
        close;
    }

    switch (@menu) {
    case 2:
        delitem GrassCarp, 1; break;
    case 3:
        delitem CommonCarp, 20; break;
    case 4:
        delitem Roach, 20; break;
    default:
        dispbottom l("Script Error: \"Tux didn't found his fish! Blame Saulc at once!\""); close; end;
    }
    delitem Root, 10;
    Zeny=Zeny-1000;
    setq3 HurnscaldQuest_ForestBow, 99;
    getexp 800, 0; // 10% of max exp
    mes "";
    mesn;
    mesc l("With a knife, the fisherman makes the roots as thin as he can. You wonder if he knows what he is doing.");
    next;
    mesn;
    mesc l("Once the roots are of a desired thickness, he ties them to make a string, and applies an oil on it.");
    next;
    mesn;
    mesc l("After waiting for the oil to dry, he hands you the string.");
    mesq l("Here, thanks to the oil, this string is very sturdy. You better not keep it on your inventory. Good luck!");
    close;

OnKillFireGoblin:
    if (getq(HurnscaldQuest_ForestBow) == 1 && getq3(HurnscaldQuest_ForestBow) >= 10 && getq3(HurnscaldQuest_ForestBow) < 50) {
        setq3 HurnscaldQuest_ForestBow, getq3(HurnscaldQuest_ForestBow)+1;
        dispbottom l("@@/40 Fire Goblins killed", getq3(HurnscaldQuest_ForestBow)-10);
    }
    fix_mobkill(FireGoblin);
    end;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;

}

