// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 015-8: Ancient Hideout mobs
015-8,99,99,69,81	monster	Mouboo	1023,32,30000,30000
015-8,114,95,51,69	monster	Robin Bandit	1153,16,30000,30000
015-8,89,140,62,30	monster	Angry Yellow Slime	1198,12,30000,30000
015-8,97,57,62,46	monster	Old Snake	1199,12,20000,20000
015-8,67,94,34,79	monster	Red Slime	1092,12,30000,30000
015-8,100,56,48,36	monster	Vampire Bat	1063,14,27000,22000
015-8,103,95,69,81	monster	Cave Maggot	1027,24,30000,30000
