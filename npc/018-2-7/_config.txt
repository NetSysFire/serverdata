// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-2-7: Heroes Hold SS - B2F conf

018-2-7,161,45,0	script	#018-2-7_161_45	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-7_161_45"; end;
OnEnable:
OnInit:
	setcells "018-2-7", 161, 45, 163, 45, 1, "018-2-7_161_45";
}

018-2-7,160,23,0	script	#018-2-7_160_23	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-7_160_23"; end;
OnEnable:
OnInit:
	setcells "018-2-7", 160, 23, 160, 37, 1, "018-2-7_160_23";
}

018-2-7,227,45,0	script	#018-2-7_227_45	NPC_HIDDEN,{
	end;
OnDisable:
	delcells "018-2-7_227_45"; end;
OnEnable:
OnInit:
	setcells "018-2-7", 227, 45, 229, 45, 1, "018-2-7_227_45";
}
