// Evol scripts.
// Authors:
//    4144
//    Reid
//    Jesusalva
// Description:
//    Allows to cut a Croconut in multiple parts.

-	script	Croconut	NPC_HIDDEN,{
    close;

OnUse:
    mesc l("Do you want to cut this @@?", getitemlink(Croconut));

    select
        l("Yes."),
        l("No.");
    mes "";
    closeclientdialog;
    switch (@menu) {
    case 1:
        goto L_Weapon;
    case 2:
        getitem Croconut, 1;
        close;
    }
    close;

L_Weapon:
    .@r=rand2(1,5);
    switch (.@r) {
        case 1:
        case 2:
        case 3:
            goto L_TooWeak; break;
        case 4:
            goto L_Weak; break;
        case 5:
            goto L_Good; break;
    }

L_TooWeak:
    .@q = rand2(5);
    if (readparam2(bStr) > 30)
        .@q = .@q + 1;
    if (readparam2(bStr) > 60)
        .@q = .@q + 1;
    if (readparam2(bStr) > 90)
        .@q = .@q + 1;

    if (.@q == 0) goto L_TooWeakLost;
    if ( (.@q == 1) || (.@q == 2) ) goto L_TooWeakFail;
    if ( (.@q >= 3) && (.@q <= 6) ) goto L_Weak;
    if ( (.@q > 6) ) goto L_Good;

L_TooWeakLost:
    dispbottom l("Ops! You destroyed your @@.", getitemlink(Croconut));
    close;

L_TooWeakFail:
    dispbottom l("Well... you did not succeed in opening this @@.", getitemlink(Croconut));

    getitem Croconut, 1;
    close;

L_Weak:
    dispbottom l("You opened the @@ in two parts, but you crushed one of them.", getitemlink(Croconut));

    getitem HalfCroconut, 1;
    close;

L_Good:
     dispbottom l("You perfectly cut your @@ into two edible parts.", getitemlink(Croconut));

    getitem HalfCroconut, 2;
    close;
}
