// TMW2 scripts.
// Authors:
//    Pyndragon
//    Jesusalva
// Description:
//    Warp Crystal

-	script	Warp Crystal	NPC_HIDDEN,{
    close;

function Cooldown {
    mesn;
    mesc l("Successive warps cause time-space distortions and thus, are not allowed.");
    mesc l("You can use it again in @@.", FuzzyTime(TELEPORTER_TIME));
    getitem @itemid, 1;
    close;
}

function ReturnItem {
    getitem @itemid, 1;
    end;
}

OnUse:
    // Receives @dest$
    if (TELEPORTER_TIME > gettimetick(2))
        Cooldown();
    if (BaseLevel < 20) {
        dispbottom l("This is too powerful to you. Get level 20 before attempting to use.");
        ReturnItem();
    }
    // TODO: Are you already at target point?

    // It have at least 20% chance to break
    // Chances begin at 100%, and lower in 0.01% each second
    // It will never be below 15%, which happens after x seconds
    .@timet=limit(0, gettimetick(2)-TELEPORTER_TIME, 3600);
    .@prop=.@timet*2777/1000; // Make it range from 0~10000
    .@adj_breakrate=limit(1500, .@prop, 9500 );
    //debugmes "Adjusted break ratio: %d", .@adj_breakrate;
    if (rand(0,10000) > .@adj_breakrate)
        getitem @itemid, 1;
    else
        getitem BrokenWarpCrystal, 1;

    // Apply Cooldown, same variable as LoF Teleporter, cancel ship travels
    TELEPORTER_TIME=gettimetick(2)+300;
    @timer_navio_running=0;

    // Save new location and warp you there
    EnterTown(@dest$);
    ReturnTown();
    end;
}
