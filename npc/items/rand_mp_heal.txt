// TMW-2 Script.
// Author:
//    Jesusalva
// Description:
//    UGLY WORKAROUND. Legacy Method Only.
// Variables:
//    @min
//    @max
//    @delay


-	script	rand_mp_heal	-1,{

OnUse:
    if (@delay <= 0) {
        Exception("Invalid healing item, deleting without healing effect.");
        end;
    }

    // +1 max MP per 3 Int, +1 min MP per 5 int.
    // Original max MP will be respected
    @max = min(@max*2, @min+(readparam2(bInt)/5));
    @min = min(@max, @min+(readparam2(bInt)/3));

    // Make these abstract % in absolute values
    @min=max(1, MaxHp*@min/100);
    @max=max(3, MaxHp*@max/100);

    // Save the effect
    @mp_healeffect = rand2(@min, @max);
    @mp_healeffect = @mp_healeffect / @delay + 1;
    @mp_healdelay = @delay;

    // Apply the effect and finish
    deltimer .name$+"::OnUpdate";
    addtimer 1000, .name$+"::OnUpdate";

    // Clear stuff
    // @mp_healeffect and @mp_healdelay must be preserved for cross-reading
    @delay=0;
    @min=0;
    @max=0;
    end;

// Script Heart
OnUpdate:
    deltimer .name$+"::OnUpdate";
    heal 0, @mp_healeffect;
    @mp_healdelay-=1;
    if (@mp_healdelay >= 1)
        addtimer 1000, .name$+"::OnUpdate";
    else
        @mp_healeffect=0;
    end;
}

