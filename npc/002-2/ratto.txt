// TMW2 Script.
// Author:
//      Jesusalva
// Originally an Evol script authored by: Ablu, Alastrim, Reid
// Description:
//    Ratto killer.

002-2,0,0,0	script	RattosControl	NPC_HIDDEN,{
    end;

OnRatto1Respawn:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2^1;
    areamonster @MAP_NAME$, 23, 19, 57, 40, strmobinfo(1, @pt_mob), @pt_mob, 1, "RattosControl::OnRatto1Death";
    end;

OnRatto2Respawn:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2^2;
    areamonster @MAP_NAME$, 23, 19, 57, 40, strmobinfo(1, @pt_mob), @pt_mob, 1, "RattosControl::OnRatto2Death";
    end;

OnRatto3Respawn:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2^4;
    areamonster @MAP_NAME$, 23, 19, 57, 40, strmobinfo(1, @pt_mob), @pt_mob, 1, "RattosControl::OnRatto3Death";
    end;

OnRatto4Respawn:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2^8;
    areamonster @MAP_NAME$, 23, 19, 57, 40, strmobinfo(1, @pt_mob), @pt_mob, 1, "RattosControl::OnRatto4Death";
    end;

OnRatto1Death:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2|1;
    .@q2=getq2(ShipQuests_Peter);
    if (.@q2 == 15)
        goto L_Victor;
    addtimer(85000, "RattosControl::OnRatto1Respawn");
    end;

OnRatto2Death:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2|2;
    .@q2=getq2(ShipQuests_Peter);
    if (.@q2 == 15)
        goto L_Victor;
    addtimer(85000, "RattosControl::OnRatto2Respawn");
    end;

OnRatto3Death:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2|4;
    .@q2=getq2(ShipQuests_Peter);
    if (.@q2 == 15)
        goto L_Victor;
    addtimer(85000, "RattosControl::OnRatto3Respawn");
    end;

OnRatto4Death:
    .@q2=getq2(ShipQuests_Peter);
    setq2 ShipQuests_Peter, .@q2|8;
    .@q2=getq2(ShipQuests_Peter);
    if (.@q2 == 15)
        goto L_Victor;
    addtimer(85000, "RattosControl::OnRatto4Respawn");
    end;

L_Victor:
    unitskilluseid(getcharid(3), BS_GREED, 1, getcharid(3)); // Auto-Looting
    warp "002-1@"+LOCATION$, 35, 26;
    deltimer("RattosControl::OnRatto1Respawn");
    deltimer("RattosControl::OnRatto2Respawn");
    deltimer("RattosControl::OnRatto3Respawn");
    deltimer("RattosControl::OnRatto4Respawn");
    doevent("Peter::OnDone");
    end;
}
