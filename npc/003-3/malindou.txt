// TMW-2 Script.
// Author:
//    Saulc
//    Jesusalva
// Notes:
//    Tulim banker, and also handles PCLogin events: CheckClientVersion, and bank
//    Take care of server updates, but instances are defined on their ships.

003-3,36,34,0	script	Malindou	NPC_LLOYD,{
    Banker(.name$, "Tulimshar", 10000);
    close;

OnInit:
    .sex = G_MALE;
    .distance = 4;

    // Update handler (use `date +%s` for this)
    // Current UPDATE value: Qui Jun  7 08:10:55 -03 2018
    if ($UPDATE  < 1528369855) {
        $UPDATE=1528369855;
        debugmes "Warning.";
        debugmes "Warning.";
        debugmes "Warning: This introduces a server update:";
        debugmes "";
        debugmes "* Automatic variable setup";
        debugmes "";
        $HASAN_GP=25;
        $MANA_BINT=30;
        $MANA_BLVL=40;
        $MANA_JLVL=20;
        $VAULT_01738039=25;
        $TULIM_MAYOR$="Jesus Saves";
        $TULIM_TAX=100;
        $TULIM_EXPORT=5000;
        $TULIM_REPUTATION=50;
        $HALIN_MAYOR$="Jesus Saves";
        $HALIN_TAX=100;
        $HALIN_EXPORT=2000;
        $HALIN_REPUTATION=50;
        $HURNS_MAYOR$="Jesus Saves";
        $HURNS_TAX=100;
        $HURNS_EXPORT=3000;
        $HURNS_REPUTATION=50;
        $LOF_MAYOR$="Jesus Saves";
        $LOF_TAX=100;
        $LOF_EXPORT=4000;
        $LOF_REPUTATION=50;
        $NIVAL_MAYOR$="Jesus Saves";
        $NIVAL_TAX=100;
        $NIVAL_EXPORT=3000;
        $NIVAL_REPUTATION=50;
        $FROSTIA_MAYOR$="Jesus Saves";
        $FROSTIA_TAX=100;
        $FROSTIA_EXPORT=3000;
        $FROSTIA_REPUTATION=50;
        $CANDOR_MAYOR$="Jesus Saves";
        $CANDOR_TAX=1000;
        $CANDOR_EXPORT=0;
        $CANDOR_REPUTATION=100;
	    $BCONFN_SPAWN = 70;
	    $BCONFD_SPAWN = 100;
	    $BCONFN_MOBHP = 100;
	    $BCONFD_MOBHP = 90;
	    $BCONFB_EXPR = 100;
	    $BCONFN_EXPR = 5;
	    $BCONFD_EXPR = 0;
	    $BCONFB_DROP = 100;
	    $BCONFN_DROP = 7;
	    $BCONFD_DROP = 0;
		$BETASERVER = false;
		$AUTORESTART = false;
        $FIRESOFSTEAM[1] = 2000;
        $FIRESOFSTEAM[2] = 2000;
        $FIRESOFSTEAM[3] = 2000;
        $FIRESOFSTEAM[4] = 2000;
        $FIRESOFSTEAM[5] = 2000;
    }
    /*
    // Current UPDATE value: Dom Jun 17 21:32:45 -03 2018
    if ($UPDATE < 1529281965) {
        $UPDATE=1529281965;
        debugmes "";
        debugmes "* Please set #BankP for bankers";
        debugmes "* Automatically creatining minimal GP reserves for Hasan Autumn's Quest";
        debugmes "";
        $HASAN_GP=rand(3,12)+rand(3,12)+rand(3,12);
    }
    // Current UPDATE value: Dom Jun 30 22:44:46 -03 2018
    if ($UPDATE < 1530409486) {
        $UPDATE=1530409486;
        debugmes "";
        debugmes "* Clearing invalid (deleted) items";
        debugmes "";
        DelItemFromEveryPlayer(729);
    }
    // Current UPDATE value: Qui Jul 12 02:40:02 -03 2018
    if ($UPDATE < 1531374002) {
        $UPDATE=1531374002;
        debugmes "";
        debugmes "* WARNING! WARNING! WARNING!";
        debugmes "* ITEM ID VIOLATION DETECTED!";
        debugmes "";
        debugmes "* ALL POLISHED EMERALDS BECOME POLISHED AMETHYSTS, AND VICE-VERSA!";
        debugmes "* This should not have an effect ingame.";
        debugmes "";
    }
    // Current UPDATE value: Qui Ago  2 13:58:17 -03 2018
    if ($UPDATE < 1533229097) {
        $UPDATE=1533229097;
        debugmes "";
        debugmes "* ERASING all victories with Fafi Dragon";
        debugmes "";
        query_sql("UPDATE `quest` SET `count1` = '1' WHERE `quest`.`quest_id`="+LoFQuest_Fairy+" AND `count1` = '2'");
        query_sql("UPDATE `quest` SET `count1` = '2' WHERE `quest`.`quest_id`="+LoFQuest_Fairy+" AND `count1` = '3'");
        DelItemFromEveryPlayer(SkullMask);
    }
    // Current UPDATE value: Qua Ago  8 11:04:10 -03 2018
    if ($UPDATE < 1533737050) {
        $UPDATE=1533737050;
        debugmes "";
        debugmes "* REMOVING now rare items.";
        debugmes "";
        DelItemFromEveryPlayer(SilverMirror);
    }
    // Current UPDATE value: Seg Ago 27 20:20:21 -03 2018
    if ($UPDATE < 1535412021) {
        $UPDATE=1535412021;
        debugmes "";
        debugmes "* SETUP mana stone default values.";
        debugmes "";
        $MANA_BINT=30;
        $MANA_BLVL=30;
        $MANA_JLVL=15;
    }
    // Current UPDATE value: Qui Set 06 13:50:37 -03 2018
    if ($UPDATE < 1536252637) {
        $UPDATE=1536252637;
        debugmes "";
        debugmes "* Clearing invalid (deleted) items";
        debugmes "* Initial Vault Values";
        debugmes "";
        DelItemFromEveryPlayer(802);
        DelItemFromEveryPlayer(557);
        $VAULT_01738039=rand(5,25);
    }
    // Current UPDATE value: Qui Set 13 14:17:50 -03 2018
    if ($UPDATE < 1536859070) {
        $UPDATE=1536859070;
        debugmes "";
        debugmes "* Aisen already have a completed node at PETMEMO.";
        debugmes "* This was handled automatically, no special care is required.";
        debugmes "";
    }
    // Current UPDATE value: Dom Set 16 22:21:05 -03 2018
    if ($UPDATE < 1537147265) {
        $UPDATE=1537147265;
        debugmes "";
        debugmes "* My Money quest state modified";
        debugmes "* Foxhound Famine quest state modified";
        debugmes "* Life Delight quest state modified"; // “A True Chef” is the LoF version of this quest
        debugmes "";
        query_sql("UPDATE `quest` SET `count1` = '1' WHERE `quest`.`quest_id`="+ShipQuests_ArpanMoney+" AND `count1` = '2'");
        query_sql("UPDATE `quest` SET `count2` = `count1` WHERE `quest`.`quest_id`="+HalinarzoQuest_Foxhound+" AND `count1` >= '6'");
        query_sql("UPDATE `quest` SET `count1` = '6' WHERE `quest`.`quest_id`="+HalinarzoQuest_Foxhound+" AND `count1` > '6'");
        query_sql("UPDATE `quest` SET `count2` = `count1` WHERE `quest`.`quest_id`="+HalinarzoQuest_LifeDelight);
        query_sql("UPDATE `quest` SET `count1` = '1' WHERE `quest`.`quest_id`="+HalinarzoQuest_LifeDelight+" AND `count1` > '1'");
    }
    // Current UPDATE value: Seg Set 17 16:02:27 -03 2018
    if ($UPDATE < 1537210947) {
        $UPDATE=1537210947;
        debugmes "";
        debugmes "* Referral Hack System";
        debugmes "";
        setarray $REFERRAL_IDS, 0;
    }
    // Current UPDATE value: Ter Out 16 17:17:20 -03 2018
    // This is a hack because I'm too lazy to be bothered with standard SQL Upgrade system
    if ($UPDATE < 1539721040) {
        query_sql("CREATE TABLE IF NOT EXISTS `discord` (`account_id` INT(11) UNSIGNED NOT NULL DEFAULT '0',`discord_id` VARCHAR(255) NOT NULL DEFAULT '',`discord_name` VARCHAR(255) NOT NULL DEFAULT '',`verified` ENUM('0', '1') NOT NULL DEFAULT '0') ENGINE=MyISAM");
        $UPDATE=1539721040;
        //$NIVALIS_LIBDATE=1539721040;
        debugmes "";
        debugmes "* SQL Upgrade (no effect on new installations)";
        //debugmes "* Nivalis Liberation Day event skipped";
        debugmes "";
    }
    // Current UPDATE value: Dom Nov 18 17:40:30 -02 2018
    // Update players position
    if ($UPDATE < 1542570030) {
        query_sql("UPDATE `char` SET `last_x` = '35' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `char` SET `last_y` = '102' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `char` SET `save_x` = '35' WHERE `char`.`save_map`='005-1'");
        query_sql("UPDATE `char` SET `save_y` = '102' WHERE `char`.`save_map`='005-1'");
        $UPDATE=1542570030;
        debugmes "";
        debugmes "* Update Candor players position";
        debugmes "";
    }
    // Current UPDATE value: Sab Dez 15 21:45:15 -02 2018
    // Upgrade every Wooden Sword in a Bug Slayer
    if ($UPDATE < 1544917515) {
        ReplaceItemFromEveryPlayer(WoodenSword, BugSlayer);
        $UPDATE=1544917515;
        debugmes "";
        debugmes "* Replaced Wooden Swords with Bug Slayer";
        debugmes "";
    }
    // Current UPDATE value: Sab Dez 17 13:52:54 -02 2018
    // Fix a critical bug because Nard's ship doors logic was changed. Upgrade premium weapons in rare weapons
    if ($UPDATE < 1545061974) {
        //query_sql("UPDATE `quest` SET `count1` = '3' WHERE `quest`.`quest_id`='0' AND `quest`.`count1` = '2' AND `quest`.`char_id` IN (SELECT `quest`.`char_id` WHERE  `quest`.`quest_id` = '12' AND  `quest`.`count1`>='1')");

        ReplaceItemFromEveryPlayer(MiereCleaver, LEGACY_MiereCleaver);
        ReplaceItemFromEveryPlayer(Broadsword, LEGACY_Broadsword);
        $UPDATE=1545061974;
        debugmes "";
        debugmes "* Replaced Miere Cleaver and Broad Sword with a special version specific for old players";
        debugmes "* Fix critical bug with Nard (not needed on Main Server)";
        debugmes "";
    }
    // Current UPDATE value: Qua Dez 26 21:45:10 -02 2018
    // Update item id
    if ($UPDATE < 1545867910) {
        ReplaceItemFromEveryPlayer(6000, 9990);
        ReplaceItemFromEveryPlayer(6001, 9991);
        ReplaceItemFromEveryPlayer(6004, 9994);
        ReplaceItemFromEveryPlayer(6005, 9995);

        ReplaceItemFromEveryPlayer(9990, 6001);
        ReplaceItemFromEveryPlayer(9991, 6000);
        ReplaceItemFromEveryPlayer(9994, 6005);
        ReplaceItemFromEveryPlayer(9995, 6004);

        $UPDATE=1545867910;
        debugmes "";
        debugmes "* Update Bows IDs";
        debugmes "";
    }
    // Current UPDATE value: Seg Dez 31 11:45:35 -02 2018
    // Update players position (002-x). Update Peter Quest. Update Main quest.
    if ($UPDATE < 1546263935) {
        query_sql("UPDATE `char` SET `last_x` = '53' WHERE `char`.`last_map`='002-1'");
        query_sql("UPDATE `char` SET `last_y` = '38' WHERE `char`.`last_map`='002-1'");
        query_sql("UPDATE `char` SET `last_x` = '53' WHERE `char`.`last_map`='002-3'");
        query_sql("UPDATE `char` SET `last_y` = '38' WHERE `char`.`last_map`='002-3'");
        query_sql("UPDATE `char` SET `last_x` = '53' WHERE `char`.`last_map`='002-4'");
        query_sql("UPDATE `char` SET `last_y` = '38' WHERE `char`.`last_map`='002-4'");

        query_sql("UPDATE `quest` SET `count1` = '2' WHERE `quest`.`quest_id`="+ShipQuests_Peter+" AND `count1` >= '3'");
        query_sql("UPDATE `quest` SET `count1` = '1' WHERE `quest`.`quest_id`="+General_Narrator+" AND `count1` >= '2'");
        $UPDATE=1546263935;
        debugmes "";
        debugmes "* Update players position (Nard's Ship)";
        debugmes "* Update Peter Quest";
        debugmes "";
    }
    // Current UPDATE value: Ter Jan 8 11:14:04 -02 2019
    // Update arrow IDs
    if ($UPDATE < 1546953244) {
        ReplaceItemFromEveryPlayer(CursedArrow, 9990);
        ReplaceItemFromEveryPlayer(IronArrow, 9991);
        ReplaceItemFromEveryPlayer(9990, IronArrow);
        ReplaceItemFromEveryPlayer(9991, CursedArrow);

        // Maybe we should make Tolchi Arrow greater than Training Arrow?

        // PlatinumQuiver - DragonStar - MichelSoul
        // Terranite Quiver was broken in two, they'll keep the weaker version
        ReplaceItemFromEveryPlayer(1174, 9991);
        ReplaceItemFromEveryPlayer(1175, 9992);
        ReplaceItemFromEveryPlayer(1176, 9993);

        ReplaceItemFromEveryPlayer(9991, 1175);
        ReplaceItemFromEveryPlayer(9992, 1176);
        ReplaceItemFromEveryPlayer(9993, 1177);

        $UPDATE=1546953244;
        debugmes "";
        debugmes "* Update Arrows IDs";
        debugmes "* Update Quiver IDs";
        debugmes "";
    }
    // Current UPDATE value: Ter Jan 15 02:07:40 -02 2019
    // Reset all thief/merc ranks back to 1.
    // If
    if ($UPDATE < 1547525260) {

        // Already rank 2? Give enough exp to get rank 2 easily.
        query_sql("UPDATE `char_reg_num_db` SET `value` = '30' WHERE `char_reg_num_db`.`key`='THIEF_EXP' AND `char_reg_num_db`.`char_id` IN (SELECT `char_reg_num_db`.`char_id` WHERE `char_reg_num_db`.`key` = 'THIEF_RANK' AND  `char_reg_num_db`.`value`>='2')");
        // Didn't wanted to rank up? Reset exp to 30!
        query_sql("UPDATE `char_reg_num_db` SET `value` = '30' WHERE `char_reg_num_db`.`key`='THIEF_EXP' AND `char_reg_num_db`.`value` >= '32' AND `char_reg_num_db`.`char_id` IN (SELECT `char_reg_num_db`.`char_id` WHERE `char_reg_num_db`.`key` = 'THIEF_RANK' AND  `char_reg_num_db`.`value`<='1')");
        // Already rank 2? Return to rank 1.
        query_sql("UPDATE `char_reg_num_db` SET `value` = '1' WHERE `char_reg_num_db`.`key`='THIEF_RANK' AND `char_reg_num_db`.`value` >= '2'");

        // The same thing, now for merchants

        // Already rank 2? Give enough exp to get rank 2 easily.
        query_sql("UPDATE `char_reg_num_db` SET `value` = '30' WHERE `char_reg_num_db`.`key`='MERC_EXP' AND `char_reg_num_db`.`char_id` IN (SELECT `char_reg_num_db`.`char_id` WHERE `char_reg_num_db`.`key` = 'MERC_RANK' AND  `char_reg_num_db`.`value`>='2')");
        // Didn't wanted to rank up? Reset exp to 30!
        query_sql("UPDATE `char_reg_num_db` SET `value` = '30' WHERE `char_reg_num_db`.`key`='MERC_EXP' AND `char_reg_num_db`.`value` >= '32' AND `char_reg_num_db`.`char_id` IN (SELECT `char_reg_num_db`.`char_id` WHERE `char_reg_num_db`.`key` = 'MERC_RANK' AND  `char_reg_num_db`.`value`<='1')");
        // Already rank 2? Return to rank 1.
        query_sql("UPDATE `char_reg_num_db` SET `value` = '1' WHERE `char_reg_num_db`.`key`='MERC_RANK' AND `char_reg_num_db`.`value` >= '2'");

        $UPDATE=1547525260;
        debugmes "";
        debugmes "* Merc/Thief ranks reset to 1";
        debugmes "Improving past rank 2 helps on minigame";
        debugmes "";
    }
    // Current UPDATE value: Qui Fev 28 19:42:12 -03 2019
    // Late fix for Nard Ship Bug
    if ($UPDATE < 1551393732) {
        query_sql("UPDATE `quest` SET `count1` = '3' WHERE `quest`.`quest_id`="+ShipQuests_Julia+" AND `count1` <= '2'");
        $UPDATE=1551393732;
        debugmes "";
        debugmes "* Late fix for Nard Ship Bug";
        debugmes "";
    }
    // Current UPDATE value: Qui Mar  7 23:15:54 -03 2019
    // Map Update
    if ($UPDATE < 1552011354) {
        query_sql("UPDATE `char` SET `last_x` = '22' WHERE `char`.`last_map`='014-3'");
        query_sql("UPDATE `char` SET `last_y` = '22' WHERE `char`.`last_map`='014-3'");
        query_sql("UPDATE `char` SET `last_map` = '000-1' WHERE `char`.`last_map`='014-3'");

        query_sql("UPDATE `char` SET `last_x` = '22' WHERE `char`.`last_map`='003-1'");
        query_sql("UPDATE `char` SET `last_y` = '22' WHERE `char`.`last_map`='003-1'");
        query_sql("UPDATE `char` SET `last_map` = '000-1' WHERE `char`.`last_map`='003-1'");
        $UPDATE=1552011354;
        debugmes "";
        debugmes "* Map Updates";
        debugmes "";
    }
    // Current UPDATE value: Sáb Mar 23 11:11:32 -03 2019
    // Soul Menhir savepoint replaced
    if ($UPDATE < 1553350292) {
        query_sql("UPDATE `char` SET `save_map` = '000-1'");
        query_sql("UPDATE `char` SET `save_x` = '22'");
        query_sql("UPDATE `char` SET `save_y` = '22'");
        $UPDATE=1553350292;
        debugmes "";
        debugmes "* Soul Menhir Save Point replaced";
        debugmes "";
    }
    // Current UPDATE value: Sex Abr 26 00:12:35 -03 2019
    // Monster King & Main Storyline System
    if ($UPDATE < 1556248355) {
        query_sql("UPDATE `quest` SET `count3` = '0' WHERE `quest`.`quest_id`="+HurnscaldQuest_Arkim);
        query_sql("UPDATE `quest` SET `count2` = '0' WHERE `quest`.`quest_id`="+HurnscaldQuest_Arkim);
        $GAME_STORYLINE=2;

        $UPDATE=1556248355;
        debugmes "";
        debugmes "* Main Storyline system updated";
        debugmes "* Arkim updated";
        debugmes "";
    }
    // Current UPDATE value: Sab Mai 04 02:21:58 -03 2019
    // Remove Return Crystals
    if ($UPDATE < 1556947318) {
        query_sql("UPDATE `quest` SET `count2` = '7' WHERE `quest`.`quest_id`="+General_Narrator+" AND `count1` >= '6'");
        query_sql("UPDATE `quest` SET `count1` = '5' WHERE `quest`.`quest_id`="+General_Narrator+" AND `count1` >= '6'");

        DelItemFromEveryPlayer(ReturnPotion);

        $UPDATE=1556947318;
        debugmes "";
        debugmes "* Player Storyline modified";
        debugmes "* Return Crystals removed";
        debugmes "";
    }
    // Current UPDATE value: Seg mai 06 08:54:55 -03 2019
    // Remove Legendary Weapons
    if ($UPDATE < 1557143695) {
        DelItemFromEveryPlayer(Lightbringer);
        //DelItemFromEveryPlayer(DemureAxe); // Demure's Exception!
        DelItemFromEveryPlayer(Tyranny);
        DelItemFromEveryPlayer(Runestaff);
        DelItemFromEveryPlayer(AegisShield);

        $UPDATE=1557143695;
        debugmes "";
        debugmes "* Legendary Weapons removed";
        debugmes "";
    }
    // Current UPDATE value: Dom mai 26 23:00:52 -03 2019
    // Shovel Update
    if ($UPDATE < 1558922452) {
        DelItemFromEveryPlayer(527); // Old pumpkish juice nobody have
        ReplaceItemFromEveryPlayer(526, 527);

        $UPDATE=1558922452;
        debugmes "";
        debugmes "* Shovel Update";
        debugmes "";
    }
    // Current UPDATE value: Seg mai 27 10:57:02 -03 2019
    // Picklog Update
    if ($UPDATE < 1558965422) {
        query_sql("ALTER TABLE picklog MODIFY COLUMN `opt_val0` SMALLINT(5) NOT NULL DEFAULT '0';");
        query_sql("ALTER TABLE picklog MODIFY COLUMN `opt_val1` SMALLINT(5) NOT NULL DEFAULT '0';");
        query_sql("ALTER TABLE picklog MODIFY COLUMN `opt_val2` SMALLINT(5) NOT NULL DEFAULT '0';");
        query_sql("ALTER TABLE picklog MODIFY COLUMN `opt_val3` SMALLINT(5) NOT NULL DEFAULT '0';");
        query_sql("ALTER TABLE picklog MODIFY COLUMN `opt_val4` SMALLINT(5) NOT NULL DEFAULT '0';");

        // Replace all BugSlayer, ShortGladius, RealBronzeGladius and Backsword
        // With a version containing the critical damage option (bCritAtkRate)
        // auction table have no faulty items, nor do carts nor do I care w/ rodex
        // by the time this query was writen
        // IOPT_CRITDMG id is 199
        query_sql("UPDATE `inventory` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+BugSlayer);
        query_sql("UPDATE `storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+BugSlayer);
        query_sql("UPDATE `guild_storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+BugSlayer);

        query_sql("UPDATE `inventory` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+ShortGladius);
        query_sql("UPDATE `storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+ShortGladius);
        query_sql("UPDATE `guild_storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+ShortGladius);

        query_sql("UPDATE `inventory` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+RealBronzeGladius);
        query_sql("UPDATE `storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+RealBronzeGladius);
        query_sql("UPDATE `guild_storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+RealBronzeGladius);

        query_sql("UPDATE `inventory` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+Backsword);
        query_sql("UPDATE `storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+Backsword);
        query_sql("UPDATE `guild_storage` SET `opt_idx0` = '199', `opt_val0` = '30'"+
        " WHERE `nameid`="+Backsword);

        $UPDATE=1558965422;
        debugmes "";
        debugmes "* picklog options Update";
        debugmes "";
    }
    // Current UPDATE value: Sex Jun 21 13:20:21 -03 2019
    // Tulimshar Sailors quest was split in two
    if ($UPDATE < 1561134021) {
        query_sql("UPDATE `quest` SET `count1` = '4' WHERE `quest`.`quest_id`="+TulimsharQuest_Sailors+" AND `count1` >= '2'");

        query_sql("UPDATE `char` SET `last_x` = '22' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `char` SET `last_y` = '22' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `char` SET `last_map` = '000-1' WHERE `char`.`last_map`='005-1'");
        $UPDATE=1561134021;
        debugmes "";
        debugmes "* Tulimshar Sailors split in two";
        debugmes "";
    }
    // Current UPDATE value: Seg Jul 15 22:10:00 -03 2019
    // Candor Redesign
    if ($UPDATE < 1563239400) {
        query_sql("UPDATE `char` SET `last_x` = '22' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `char` SET `last_y` = '22' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `char` SET `last_map` = '000-1' WHERE `char`.`last_map`='005-1'");
        query_sql("UPDATE `skill` SET `id` = '20010' WHERE `skill`.`id`='20011'");
        $UPDATE=1563239400;
        debugmes "";
        debugmes "* Candor Redesign";
        debugmes "* Magic Class Removed";
        debugmes "";
    }
    // Current UPDATE value: Sab Ago 24 17:42:24 -03 2019
    // Political System
    if ($UPDATE < 1566679344) {
        $UPDATE=1566679344;
        // $LOC_MAYOR$ - Stores the name of current Hurnscald Mayor
        // $LOC_MONEY - Total money reserves of Hurnscald
        // $LOC_TAX - How much in % is charged as taxes. (OnBuy income)
        // $LOC_EXPORT - Defines how much Hurnscald exports (weekly income)
        // $LOC_REPUTATION - Town reputation. Affects Max Tax and Weekly Income.

        // Tulim
        $TULIM_MAYOR$="Saulc GM";
        $TULIM_TAX=100;
        $TULIM_EXPORT=5000;
        $TULIM_REPUTATION=50;
        // Halin
        $HALIN_MAYOR$="Saulc GM";
        $HALIN_TAX=100;
        $HALIN_EXPORT=2000;
        $HALIN_REPUTATION=50;
        // Hurns
        $HURNS_MAYOR$="Saulc GM";
        $HURNS_TAX=100;
        $HURNS_EXPORT=3000;
        $HURNS_REPUTATION=50;
        // LoF
        $LOF_MAYOR$="Jesus Saves";
        $LOF_TAX=100;
        $LOF_EXPORT=4000;
        $LOF_REPUTATION=50;
        // Nival
        $NIVAL_MAYOR$="Jesus Saves";
        $NIVAL_TAX=100;
        $NIVAL_EXPORT=3000;
        $NIVAL_REPUTATION=50;
        // Frostia
        $FROSTIA_MAYOR$="Jesus Saves";
        $FROSTIA_TAX=100;
        $FROSTIA_EXPORT=3000;
        $FROSTIA_REPUTATION=50;

        debugmes "";
        debugmes "* Political System Base";
        debugmes "";
    }
    // Current UPDATE value: Sex Jan 17 14:42:37 BRT 2020
    // Remove Junk
    if ($UPDATE < 1579282957) {
        DelItemFromEveryPlayer(748);
        DelItemFromEveryPlayer(1166);
        $UPDATE=1579282957;
        debugmes "";
        debugmes "* Blanket fix";
        debugmes "";
    }
    // Current UPDATE value: Dom Fev 16 14:54:30 BRT 2020
    // Language over simplification
    if ($UPDATE < 1581875670) {
        $UPDATE=1581875670;
        query_sql("DELETE FROM `char_reg_num_db` WHERE `key`='INN_REGISTER'");
        DelAccRegFromEveryPlayer("#RARE_POINTS");
        debugmes "";
        debugmes "* Variable cleanup";
        debugmes "";
    }
    // Current UPDATE value: Sex Fev 24 12:15:12 BRT 2020
    // Release 11.1 final
    if ($UPDATE < 1582557312) {
        $UPDATE=1582557312;
        ReplaceItemFromEveryPlayer(RiceHat, BullHelmet);
        DelItemFromEveryPlayer(SilverEasteregg);
        query_sql("UPDATE `quest` SET `count1` = '2' WHERE `quest`.`quest_id`="+General_Narrator+" AND `count1` = '3'");
        query_sql("UPDATE `quest` SET `count2` = '0' WHERE `quest`.`quest_id`="+General_Narrator+" AND `count1` = '2'");
	    $BCONFN_SPAWN = 70;
	    $BCONFD_SPAWN = 100;
	    $BCONFN_MOBHP = 95;
	    $BCONFD_MOBHP = 85;
	    $BCONFB_EXPR = 100;
	    $BCONFN_EXPR = 5;
	    $BCONFD_EXPR = 0;
	    $BCONFB_DROP = 100;
	    $BCONFN_DROP = 7;
	    $BCONFD_DROP = 0;
        debugmes "";
        debugmes "* Bull Helmet Fix";
        debugmes "* Remove Silver Easter Eggs";
        debugmes "* Lua Quest Reset";
        debugmes "* Battle Configuration is now volatile";
        debugmes "";
    }
    // Current UPDATE value: Dom Abr 26 12:41:55 BRT 2020 [1587915715]
    // Current UPDATE value: Qua Mai 27 18:56:15 BRT 2020
    // Fix Tolchi mess
    if ($UPDATE < 1590616575) {
        if ($UPDATE < 1587915715) {
        query_sql("UPDATE `inventory` SET `opt_val0` = '20', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx0`='189' OR `opt_idx0`='200') AND (`opt_idx1`='189' OR `opt_idx1`='200') AND (`opt_val0` > '18' AND `opt_val1` > '18')");
        query_sql("UPDATE `cart_inventory` SET `opt_val0` = '20', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx0`='189' OR `opt_idx0`='200') AND (`opt_idx1`='189' OR `opt_idx1`='200') AND (`opt_val0` > '18' AND `opt_val1` > '18')");
        query_sql("UPDATE `storage` SET `opt_val0` = '20', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx0`='189' OR `opt_idx0`='200') AND (`opt_idx1`='189' OR `opt_idx1`='200') AND (`opt_val0` > '18' AND `opt_val1` > '18')");
        query_sql("UPDATE `guild_storage` SET `opt_val0` = '20', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx0`='189' OR `opt_idx0`='200') AND (`opt_idx1`='189' OR `opt_idx1`='200') AND (`opt_val0` > '18' AND `opt_val1` > '18')");
        query_sql("UPDATE `rodex_items` SET `opt_val0` = '20', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx0`='189' OR `opt_idx0`='200') AND (`opt_idx1`='189' OR `opt_idx1`='200') AND (`opt_val0` > '18' AND `opt_val1` > '18')");
        query_sql("UPDATE `auction` SET `opt_val0` = '20', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx0`='189' OR `opt_idx0`='200') AND (`opt_idx1`='189' OR `opt_idx1`='200') AND (`opt_val0` > '18' AND `opt_val1` > '18')");
        }

        // Destroy duplicates (should not exist)
        query_sql("UPDATE `inventory` SET `opt_val0` = '15', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx1`=`opt_idx0`) AND (`opt_val0` > '15' AND `opt_val1` > '15')");
        query_sql("UPDATE `storage` SET `opt_val0` = '15', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx1`=`opt_idx0`) AND (`opt_val0` > '15' AND `opt_val1` > '15')");
        query_sql("UPDATE `guild_storage` SET `opt_val0` = '15', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx1`=`opt_idx0`) AND (`opt_val0` > '15' AND `opt_val1` > '15')");
        query_sql("UPDATE `rodex_items` SET `opt_val0` = '15', `opt_val1` = '0', `opt_idx1` = '0' WHERE (`opt_idx1`=`opt_idx0`) AND (`opt_val0` > '15' AND `opt_val1` > '15')");

        // Magic v3
        $REBIRTH_WINNER$="Crazyfefe";
        DelChrRegFromEveryPlayer("MAGIC_SUBCLASS");
        ReplaceSkillFromEveryPlayer(MG_FIREBALL, TMW2_FIREARROW); // 1 MSP

        // These skills MSP cost was switched
        ReplaceSkillFromEveryPlayer(AL_HOLYLIGHT, TMW2_NAPALMBEAT);
        ReplaceSkillFromEveryPlayer(MG_NAPALMBEAT, TMW2_HOLYLIGHT);

        ReplaceSkillFromEveryPlayer(MG_SOULSTRIKE, TMW2_MAGICSTRIKE);
        ReplaceSkillFromEveryPlayer(WZ_EARTHSPIKE, TMW2_METEORSTRIKE);

        ReplaceSkillFromEveryPlayer(MG_COLDBOLT, TMW2_FROSTDIVER);
        ReplaceSkillFromEveryPlayer(MG_FROSTDIVER, TMW2_FROSTNOVA);
        ReplaceSkillFromEveryPlayer(WZ_FROSTNOVA, TMW2_NILFHEIM);

        ReplaceSkillFromEveryPlayer(AL_HEAL, TMW2_FIRSTAID);
        ReplaceSkillFromEveryPlayer(AB_HIGHNESSHEAL, TMW2_HEALING);

        debugmes "";
        debugmes "* Tolchi mess cleanup";
        debugmes "* Rebirth Hero";
        debugmes "";
        $UPDATE=1590616575;
    }
    // Current UPDATE value: Ter Jul 28 22:49:52 BRT 2020
    // Tulimshar Volcano
    if ($UPDATE < 1595987392) {
        $UPDATE=1595987392;
        ReplaceItemFromEveryPlayer(816, IcedBottle);
        debugmes "";
        debugmes "* Iced Water re-id";
        debugmes "";
    }
    */
    // Current UPDATE value: Ter Mar 9 18:45:00 BRT 2021
    // Easter Upgrade
    if ($UPDATE < 1615326300) {
        $UPDATE=1615326300;
        DelChrRegFromEveryPlayer("EASTER_EVENT");
        debugmes "";
        debugmes "* Easter changes";
        debugmes "";
    }
    // Current UPDATE value: Qua Abr 7 13:32:35 BRT 2021
    // Skill Update
    if ($UPDATE < 1617813155) {
        $UPDATE=1617813155;
        ReplaceSkillFromEveryPlayer(SM_BASH,            TMW2_FALKONSTRIKE);
        ReplaceSkillFromEveryPlayer(ASC_METEORASSAULT,  TMW2_GROUNDSTRIKE);
        ReplaceSkillFromEveryPlayer(MC_MAMMONITE,       TMW2_SUPREMEATTACK);
        ReplaceSkillFromEveryPlayer(AC_CHARGEARROW,     TMW2_CHARGEDARROW);
        ReplaceSkillFromEveryPlayer(AC_SHOWER,          TMW2_ARROWSHOWER);
        debugmes "";
        debugmes "* Skill changes";
        debugmes "";
    }


    // This mensures Contributors Credits, and changes only during updates.
    // All names in lower case. Only TMW2 direct work! Non-TMW2 contributors
    // should be disregarded if they're not involved with the project.
    // Standard patch (DIY); 10 points
    // Reports: 1 point if relevant enough

    // You may get more points for complexity, difficulty, usability, etc.
    // You may get less points if you don't do it yourself, eg. a bug report.
    // Relevance of the contribution is also to be taken in account.

    // See 003-2/lua.txt for rewards

    $@CONTRIBUTORS = htnew;
    htput($@CONTRIBUTORS, "jesusalva",      250);
    htput($@CONTRIBUTORS, "saulc",          250);
    htput($@CONTRIBUTORS, "lawncable",      180);
    htput($@CONTRIBUTORS, "crazyfefe",      70);
    htput($@CONTRIBUTORS, "mishana",        70);
    htput($@CONTRIBUTORS, "arthur",         50);
    htput($@CONTRIBUTORS, "rakinorf",       40);
    htput($@CONTRIBUTORS, "dangerduck",     40);
    htput($@CONTRIBUTORS, "pookie",         36);
    htput($@CONTRIBUTORS, "kolchak",        30);
    htput($@CONTRIBUTORS, "xanthem",        20);
    htput($@CONTRIBUTORS, "jak1",           20);
    htput($@CONTRIBUTORS, "dustman",        18);
    htput($@CONTRIBUTORS, "povo",           10);
    htput($@CONTRIBUTORS, "minegamerbr",    10);
    htput($@CONTRIBUTORS, "yuckfou",        10);
    htput($@CONTRIBUTORS, "kytty",          10);
    htput($@CONTRIBUTORS, "ledmitz",        10);
    htput($@CONTRIBUTORS, "esteria",        8);
    htput($@CONTRIBUTORS, "liangtai",       4);
    htput($@CONTRIBUTORS, "demure",         2);
    htput($@CONTRIBUTORS, "seeds",          2);
    htput($@CONTRIBUTORS, "lilanna",        1);

    htput($@CONTRIBUTORS, "manatauro reborn", 10);
    htput($@CONTRIBUTORS, "hocus pocus fidibus", 40);
    htput($@CONTRIBUTORS, "cadis etrama di raizel", 20);
    //htput($@CONTRIBUTORS, "", 1);
    end;

OnClock0500:
    if (gettime(GETTIME_DAYOFMONTH) >= 7)
        query_sql("DELETE FROM `chatlog` WHERE `time` < '"+sqldate(-7)+"'");
    query_sql("DELETE FROM `picklog` WHERE `time` < '"+sqldate(0, -3)+"'");
    end;

// This is for HUB
OnSkillInvoke:
    if (!playerattached())
        end;
    HUB_SkillInvoke();
    @skillId=0;
    end;

OnPCBonusEvent:
    if (!playerattached())
        end;
    HUB_PCBonus();
    end;

// Level up events
OnPCBaseLvUpEvent:
    switch (BaseLevel) {
    case 3:
    case 5:
    case 7:
    case 10:
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
        // Recalculate the bonus
        NewcomerEXPDROPUP();
        break;
    case 20:
        if (!REBIRTH) {
            sc_end SC_CASH_PLUSEXP;
            sc_end SC_CASH_RECEIVEITEM;
        }
        break;
	// Events
	case 40:
		if (countitem(MercCard_EH))
			delitem MercCard_EH, countitem(MercCard_EH);
		break;
	// Referral and Eisten
    case 25:
        if (#REFERRAL_PROG && BaseLevel == 25 && #REFERRAL_CTRL < 1) {
            #REFERRAL_CTRL=1;
        	rodex_sendmail(atoi(gf_charid(#REFERRAL_PROG)), "TMW2 Team", "Recruited Player got Lv 25!", strcharinfo(0)+" just got level 25!\nAs they get stronger, more rewards will be sent to you!", 0, SilverGift, 1);
        }
    case 50:
        if (#REFERRAL_PROG && BaseLevel == 50 && #REFERRAL_CTRL < 2) {
            #REFERRAL_CTRL=2;
        	rodex_sendmail(atoi(gf_charid(#REFERRAL_PROG)), "TMW2 Team", "Recruited Player got Lv 50!", strcharinfo(0)+" just got level 50!\nAs they get stronger, more rewards will be sent to you!", 0, ArcmageBoxset, 1);
        }
    case 75:
        if (#REFERRAL_PROG && BaseLevel == 75 && #REFERRAL_CTRL < 3) {
            #REFERRAL_CTRL=3;
        	rodex_sendmail(atoi(gf_charid(#REFERRAL_PROG)), "TMW2 Team", "Recruited Player got Lv 75!", strcharinfo(0)+" just got level 75!\nAs they get stronger, more rewards will be sent to you!", 0, PrismGift, 1);
        }
    case 100:
        if (#REFERRAL_PROG && BaseLevel == 100 && #REFERRAL_CTRL < 4) {
            #REFERRAL_CTRL=4;
        	rodex_sendmail(atoi(gf_charid(#REFERRAL_PROG)), "TMW2 Team", "Recruited Player got Lv 100!", strcharinfo(0)+" just got level 100!\nTime to reap what you've sow for so long!", 0, SupremeGift, 1);
        }
    case 125:
    case 150:
        dispbottom l("Milestone levelup: A reward can now be claimed in Tulimshar.");
        break;
    }
    end;

// Every time the server starts, clean up possibly broken data about treasures.
// NPC ID might have changed.
OnInterIfInitOnce:
    atcommand("@channel setopt #world MessageDelay 1");
    query_sql("DELETE FROM `char_reg_num_db` WHERE `key`='RNGTREASURE_DATE'");
    query_sql("DELETE FROM `char_reg_num_db` WHERE `key`='PVP_COOLDOWN'");
    query_sql("DELETE FROM `char_reg_num_db` WHERE `key`='CHAREG_CLEANUP'");
    end;

// This control all PC Login events
// Position matters!
OnPCLoginEvent:
    // Vault override
    if (#MerchantBank) {
        BankVault += max(0, #MerchantBank);
        #MerchantBank = 0;
    }

    // Cannot login with outdated client
    checkclientversion();

    // Message of the Day have priority
    MOTDHandler();

    // Scheduled Announces
    StoneBoardRead();

    // TODO: npc/commands/rate-management.txt

    // Main update handler
    clientupdater();

    // Position and gameplay fixes
    HUB_Login();

    // Alcohol system reset
    ALCReset();

    // Newbie bonuses recalc
    if (BaseLevel < 20 || REBIRTH) NewcomerEXPDROPUP();

    // Daily rewards (The first with dialog interactions)
    daily_login_bonus_handler();

    // Guild Weekly Login Bonus
    guild_login_bonus();

    // Thanksgiving Event
    sThanksgiving();
    end;
}

