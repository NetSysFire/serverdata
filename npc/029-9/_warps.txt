// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 029-9: Woodland mining camp warps
029-9,97,49,0	warp	#029-9_97_49	5,0,029-8,174,78
029-9,103,48,0	script	#029-9_103_48	NPC_HIDDEN,1,0,{
	end;
OnTouch:
	slide 136,40; end;
}
029-9,91,26,0	script	#029-9_91_26	NPC_HIDDEN,1,0,{
	end;
OnTouch:
	slide 34,24; end;
}
029-9,34,23,0	script	#029-9_34_23	NPC_HIDDEN,1,0,{
	end;
OnTouch:
	slide 90,25; end;
}
029-9,137,41,0	script	#029-9_137_41	NPC_HIDDEN,1,0,{
	end;
OnTouch:
	slide 102,47; end;
}
