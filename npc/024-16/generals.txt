// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    Generals

// FrostiaTaskMaster(lvl)
function	script	FrostiaTaskMaster	{
	.@q=General_MobHunting4;
	.@q1=getq(General_MobHunting4);
	.@q2=getq2(General_MobHunting4);
	.@q3=getq3(General_MobHunting4);
	.@d=gettimeparam(GETTIME_DAYOFMONTH);
	.@lv=getarg(0, 1);
    // It's a new day, so we must generate a new quest!
    if (.@q1 != .@d) {
        .@q1=.@d;
        .@q2=any(Moggun, IcedFluffy, Fluffy, Pollet, BlueSlime, WhiteSlime, SantaSlime, AzulSlime, RudolphSlime); // WaterFairy
        setq .@q, .@q1, .@q2, 0;
    }

    // Maybe there is no monster to kill
    if (!.@q2) {
        mesn;
        mesq l("You've already completed this quest today. Try again tomorrow.");
        return true;
    }

    // Maybe you finished the quest?
    if (.@q3 >= 50) {
        mesn;
        mesq l("Good job, you've killed the @@ @@ and reported back in time!", 50, getmonsterlink(.@q2));
        next;
        mesn;
        mesq l("It's not much of a reward, but doesn't it feels great to help others in need?! HAHAHA!");
        Zeny+=.@lv*8;
        // Raise LV according to monster level
        .@lv+=strmobinfo(3, .@q2);
        getexp BaseLevel*.@lv, .@lv;
        setq2 .@q, 0;
        setq3 .@q, 0;
        return false;
    }

    mesn;
    mesq l("Please kill @@/@@ @@ for us and make the world a safer place!", .@q3, 50, getmonsterlink(.@q2));
    return false;
}

// FrostiaGeneralQuest( Skillname )
function	script	FrostiaGeneralQuest	{
	.@at$=getarg(0);
	mesn;
	mesq l("Hey. You there. Time to do @@ and show what you're made of.", .@at$);
	next;
	mesn;
	mesq l("If you conclude the training, you'll be suitable for the difficult mission our King have to you.");
	next;
    mesc b(l(".:: Main Quest 6-2 ::.")), 3;
    msObjective(BaseLevel >= 52, l("* @@/@@ Base Level", BaseLevel, 52));
    msObjective(JobLevel >= 24, l("* @@/@@ Job Level", JobLevel, 24));
	//msObjective(JobLevel >= 24, l("* Win an Arena Match"));
	next;
	if (BaseLevel >= 52 && JobLevel >= 24) {
		mesn;
		mesq l("Congrats, you did it.");
		next;
		mesn;
		mesq l("So, if you hired a Mercenary to protect your back... Would you prefer it to attack from rearguard, or charge against certain death, buying you time?");
		next;
		select
			l("I honestely wouldn't hire a Mercenary."),
			l("Running is for noobs, we should hold our ground!"),
			l("I can protect myself, so it should attack from afar.");
		mes "";
		// Decide which mercenary you'll get
		if (@menu == 2)
			.@card=MercCard_Saulc;
		else if (@menu == 3)
			.@card=MercCard_Arthur;
		else
			.@card=any(MercCard_Saulc, MercCard_Arthur);

		inventoryplace ElixirOfLife, 1, .@card, 1, Grenade, 3;
		mesn;
		mesq l("Take this @@. It might save your life on the secret mission you're about to be assigned to.", getitemlink(.@card));
		next;
		mesn;
		mesq l("I'll also provide you a few @@s and an Elixir. Use them wisely.", getitemlink(Grenade));
		next;
		mesn;
		mesq l("Now go talk to the King.");
		setq General_Narrator, 15;
		getitem Grenade, 3;
		getitem .@card, 1;
		getitem ElixirOfLife, 1;
		getexp 0, 400; // Get 10% JEXP. Reference: 3988~5564
		close;
	}
	mesn;
	mesq l("Now, a simple @@ quest to make you stronger...", .@at$);
	next;
	if (FrostiaTaskMaster(20)) {
		next;
		mesn;
		mesq l("Or maybe... You want to try again right now? I'll do whatever needed to level you up to less-noobish levels.");
		next;
		if (askyesno() == ASK_YES) {
			setq General_MobHunting4, 0, 0, 0;
			FrostiaTaskMaster(20);
		}
	}
	close;
}

//////////////////////////////////////////////////////////////////////
024-16,29,21,0	script	Wizard General	NPC_BLUESABER,{
	.@q=getq(General_Narrator);
	if (.@q == 14)
		FrostiaGeneralQuest("Magic");
	else
		FrostiaTaskMaster(20);
	close;

OnInit:
    .distance=5;
    end;
}

024-16,31,21,0	script	Warrior General	NPC_REDSABER,{
	.@q=getq(General_Narrator);
	if (.@q == 14)
		FrostiaGeneralQuest("Fight");
	else
		FrostiaTaskMaster(20);
	close;

OnInit:
    .distance=5;
    end;
}

