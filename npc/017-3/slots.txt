// TMW2 Scripts
// Author:
//    Jesusalva
// Description:
//    Slot Machine for bets

017-3,84,67,0	script	Slot Machine#017-3a	NPC_SLOTMACHINE,{
    function symbol{
        switch (getarg(0)) {
        case 1:
            mesn "%%A";
            break;
        case 2:
            mesn "%%B";
            break;
        case 3:
            mesn "%%C";
            break;
        case 4:
            mesn "%%D";
            break;
        case 5:
            mesn "%%E";
            break;
        case 6:
            mesn "%%F";
            break;
        case 7:
            mesn "7";
            break;
        default:
            mesn "%%@";
            break;
        }
    }

L_Menu:
    mesn;
    mesc l("Spin three symbols, and jackpot great rewards!");
    mesc l("Just two coins for spin.");
    next;
    menu
        rif(countitem(CasinoCoins) >= 2, l("Spin!")), L_Spin,
        l("Prizes"), L_Info,
        l("Leave"), L_Quit;

L_Info:
    mes "";
    mesc l("Prizes:");
    mes l("##9 777: @@.", getitemlink(Bloodstone));
    mes l("##9 %%A%%A%%A: @@.", getitemlink(Sapphire));
    mes l("##9 %%B%%B%%B: @@.", getitemlink(Amethyst));
    mes l("##9 %%C%%C%%C: @@.", getitemlink(Diamond));
    mes l("##9 %%D%%D%%D: @@.", getitemlink(Emerald));
    mes l("##9 %%E%%E%%E: @@.", getitemlink(Topaz));
    mes l("##9 %%F%%F%%F: @@.", getitemlink(Ruby));
    mesc l("Two equal: 1 casino coin.");
    next;
    goto L_Menu;


L_Spin:
    mesc l("Spinning...");
    next;
    delitem CasinoCoins, 2;
    .@a=rand2(1,7);
    .@b=rand2(1,7);
    .@c=rand2(1,7);
    symbol(.@a);
    symbol(.@b);
    symbol(.@c);
    next;
    mesn;
    if (.@a == .@b && .@a == .@c) {
        .@it=Iten;
        switch (.@a) {
            case 1:
                .@it=Sapphire; break;
            case 2:
                .@it=Amethyst; break;
            case 3:
                .@it=Diamond; break;
            case 4:
                .@it=Emerald; break;
            case 5:
                .@it=Topaz; break;
            case 6:
                .@it=Ruby; break;
            case 7:
                .@it=Bloodstone; break;
        }
        getitem .@it, 1;
        mesc l("Jackpot! You got the @@!", .@it), 3;
    } else if (.@a == .@b || .@a == .@c || .@b == .@c) {
        getitem CasinoCoins, 1;
        mesc l("Lucky! You got the coin back!"), 3;
    } else {
        mesc l("It wasn't this time..."), 3;
    }
    next;
    goto L_Menu;

L_Quit:
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 4;
    end;


}

