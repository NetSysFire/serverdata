// TMW2/LoF Script.
// Author:
//    Jesusalva
// Notes:
//    Based on BenB idea.

017-3,80,39,0	script	Vault#0173	NPC_NO_SPRITE,{
    LootableVault(1, 3, "01738039");
    close;

OnInit:
    .distance=3;
    end;

OnClock0201:
OnClock1418:
    $VAULT_01738039+=rand2(5,25);
    end;
}

