// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 018-4: Secret Island mobs
018-4,125,78,46,21	monster	Golden Scorpion	1078,7,10000,5000
018-4,103,57,46,32	monster	Black Scorpion	1074,5,20000,10000
018-4,103,57,46,32	monster	Black Slime	1178,10,15000,10000
018-4,96,70,76,47	monster	Red Slime	1092,12,12000,4000
018-4,96,70,76,47	monster	Yellow Slime	1091,15,30000,30000
018-4,96,66,76,43	monster	Clover Patch	1028,2,75000,35000
018-4,96,70,76,47	monster	Mouboo	1023,3,12000,4000
018-4,103,76,40,12	monster	Wicked Mushroom	1176,2,85000,65000
