// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Forgetful

027-1,75,56,0	script	Eliza	NPC_DARK_SORCERER_F,{
    mesn l("%s, the Forgetful", .name$);
    mesq l("What was I doing before...? I totally forgot!");
    if (any(true, false))
        mesc l("Have you seen my sister Elza?");
    next;
    ForgetfulNPC(.name$, CLASS_DESTRUCTION);
    close;

OnInit:
    .sex = G_FEMALE;
    .distance = 5;
    end;

}

