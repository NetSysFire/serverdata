// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    NPC obssessed in sending gifts to Demure

001-11,37,25,0	script	Soren xd#Valentine	NPC_PLAYER,{
    mesn;
    mesq l("I will harass Demure with so many chocolate that she'll leave this event map rolling!");
    if (gettime(GETTIME_YEAR) == 2020)
        goto L_Purpose;
    next;

L_Main:
    mesn;
    mesq l("Do you want me to prepare more chocolate box for you? For that I'll need:");
    msObjective(countitem(ChocolateBar) >= 12, l("* @@/12 @@", countitem(ChocolateBar), getitemlink(ChocolateBar)));
    msObjective(Zeny >= 200, l("* @@/200 GP", format_number(Zeny)));
    if (countitem(ChocolateBar) < 12 || Zeny < 200)
        close;
    next;
    if (askyesno() == ASK_NO)
        close;
    if (Zeny < 200)
        warp "001-9", 0, 0;

    inventoryplace BoxOfChocolates, 1;
    delitem ChocolateBar, 12;
    Zeny=Zeny-200;
    getitem BoxOfChocolates, 1;
    mesn;
    mesq l("Here you go!");
    if (countitem(ChocolateBar) >= 12) {
        mesq l("Anything else?");
        next;
        goto L_Main;
    }
    close;

L_Purpose:
    next;
    select
        l("You're creepy."),
        l("Reset status"),
        l("Change weapons"),
        l("Open storage"),
        l("Open shop");
    mes "";
    switch (@menu) {
        case 2:
            ConfirmStatusReset(1, false);
            break;
        case 3:
            goto L_Swap;
            break;
        case 4:
            closeclientdialog;
            openstorage;
            break;
        case 5:
            closeclientdialog;
            shop .name$;
            break;
    }
    close;

L_Swap:
    // Obtain item ID
    if (countitem(Event1HSword))
        .@itemid=Event1HSword;
    else if (countitem(Event2HSword))
        .@itemid=Event2HSword;
    else if (countitem(EventBow))
        .@itemid=EventBow;
    else if (countitem(EventWand))
        .@itemid=EventWand;
    else
        close;

    // Item list
    delinventorylist();
    getinventorylist();
    .@idx=array_find(@inventorylist_id, .@itemid);
    if (.@idx < 0)
        Exception("Invalid index for ("+.@itemid+") "+getitemlink(.@itemid), RB_DEFAULT|RB_ISFATAL);

    select
        rif(.@itemid != Event1HSword, l("One hand sword (average)")),
        rif(.@itemid != Event2HSword, l("Two hands sword (strong, misses)")),
        rif(.@itemid != EventBow, l("Bow (strong, slow, no evasion)")),
        rif(.@itemid != EventWand, l("Wand (skill boost, pathetically weak)")),
        l("Cancel");
    mes "";
    switch (@menu) {
    case 1:
        delitem .@itemid, 1;
        getitembound2(Event1HSword, 1, 1, @inventorylist_refine[.@idx],
        @inventorylist_attribute[.@idx], @inventorylist_card1[.@idx],
        @inventorylist_card2[.@idx], @inventorylist_card3[.@idx],
        @inventorylist_card4[.@idx], 1);
        break;
    case 2:
        delitem .@itemid, 1;
        getitembound2(Event2HSword, 1, 1, @inventorylist_refine[.@idx],
        @inventorylist_attribute[.@idx], @inventorylist_card1[.@idx],
        @inventorylist_card2[.@idx], @inventorylist_card3[.@idx],
        @inventorylist_card4[.@idx], 1);
        break;
    case 3:
        delitem .@itemid, 1;
        getitembound2(EventBow, 1, 1, @inventorylist_refine[.@idx],
        @inventorylist_attribute[.@idx], @inventorylist_card1[.@idx],
        @inventorylist_card2[.@idx], @inventorylist_card3[.@idx],
        @inventorylist_card4[.@idx], 1);
        break;
    case 4:
        delitem .@itemid, 1;
        getitembound2(EventWand, 1, 1, @inventorylist_refine[.@idx],
        @inventorylist_attribute[.@idx], @inventorylist_card1[.@idx],
        @inventorylist_card2[.@idx], @inventorylist_card3[.@idx],
        @inventorylist_card4[.@idx], 1);
        break;
    }
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, CandorShorts);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CandorShirt);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    // Soren Shop
    /*
	sellitem AnimalBones, 90;
	sellitem Candy, 35;
	sellitem Piberries, 25;
    sellitem OolongTea;
    sellitem ChamomileTea;
    sellitem TolchiArrow;
    sellitem TolchiAmmoBox;
    sellitem TrainingAmmoBox;
    sellitem CrazyRum;
    sellitem Beer;
    sellitem GoldenApple, 7000;
    sellitem MercBoxA, 1000;
    sellitem Grenade;
    sellitem SmokeGrenade;
    sellitem LousyMoccasins;
    sellitem LeatherQuiver, 3500;
    sellitem AgiPotionA, 1000;
    sellitem VitPotionA, 1000;
    sellitem IntPotionA, 1000;
    sellitem DexPotionA, 1000;
    sellitem LukPotionA, 1000;
    sellitem SacredLifePotion, 2500;
    sellitem SacredManaPotion, 2500;
    sellitem CandorWarpCrystal, 100;
    //sellitem MagicApple;
    //sellitem FishingRod;
    //sellitem WurtziteOre;
    // TODO: LottoBox - Random hat to distinguish players
    sellitem DoggyDog;
    */
    sellitem LousyMoccasins; // Debug so things don't crash

    .sex = G_MALE;
    .distance = 5;
    end;

}

