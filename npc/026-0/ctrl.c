// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//   The Impregnable Fortress Control Files
// Quest: General_Fortress
//  (MaxFloor+1, internal, internal)

026-0,64,20,0	script	Impregnable#B0F	NPC_HIDDEN,4,0,{
    end;

OnTouch:
    if (getq(General_Fortress) > 1) goto L_Warp;
    mesc l(".:: Impregnable Fortress, %sF ::.", "B0"), 3;
    msObjective(getq(General_Fortress) == 2, l("* Solo \"The Yetifly\""));
    msObjective($MK_TEMPVAR < MKIF_LV_B0F, l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B1F));
    mes "";
    mesc l("Hint: Stomp! Stomp! Stomp! Walk around.");
    end;

L_Warp:
    // Not unlocked
    if ($GAME_STORYLINE >= 3 && $MK_TEMPVAR < MKIF_LV_B1F) {
        mesc l("The gate is sealed shut."), 1;
        mesc l("The monster army is still strong on this floor!"), 1;
        mesc l("Minimum wins: %d/%d", $MK_TEMPVAR, MKIF_LV_B1F), 1;
        close;
    }
    warp "026-1", 29, 94;
    end;

}

026-0,40,52,0	script	Impregnable#B0F_1	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    .@q=getq(General_Fortress);
    .@q2=getq2(General_Fortress);
    .@q3=getq3(General_Fortress);
    if (.@q != 1) end;

    .@n$=strnpcinfo(0, "_0");
    explode(.@ni$, .@n$, "_");
    .@id=atoi(.@ni$[1]);
    if (.@id <= 0) Exception("Unparseable switch: "+.@n$, RB_DEFAULT|RB_ISFATAL);

    if (!(.@q2 & .@id)) {
        setq2 General_Fortress, .@q2 | .@id;
    }
    end;
}

026-0,45,48,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_2	NPC_HIDDEN,0,0
026-0,90,42,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_4	NPC_HIDDEN,0,0
026-0,83,55,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_8	NPC_HIDDEN,0,0
026-0,82,64,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_16	NPC_HIDDEN,0,0
026-0,65,58,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_32	NPC_HIDDEN,0,0
026-0,64,60,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_64	NPC_HIDDEN,0,0
026-0,43,76,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_128	NPC_HIDDEN,0,0
026-0,46,77,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_256	NPC_HIDDEN,0,0
026-0,70,87,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_512	NPC_HIDDEN,0,0
026-0,84,84,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_1024	NPC_HIDDEN,0,0
026-0,84,86,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_2048	NPC_HIDDEN,0,0
026-0,85,86,0	duplicate(Impregnable#B0F_1)	Impregnable#B0F_4096	NPC_HIDDEN,0,0


026-0,99,41,0	script	Impregnable#B0F_X	NPC_HIDDEN,0,0,{
    end;
OnTouch:
    .@q=getq(General_Fortress);
    .@q2=getq2(General_Fortress);
    .@q3=getq3(General_Fortress);
    if (.@q != 1) end;
    if (.@q2 < 8191) end;

    if (getareausers("026-0", 54, 39, 77, 53) > 0) end;
    if (mobcount("026-0", "Impregnable#B0F_X::OnYetifly") > 0)
        killmonster("026-0", "Impregnable#B0F_X::OnYetifly", false);

    slide 72, 49;
    monster("026-0", 57, 44, l("The Yetifly"), Yetifly, 1, "Impregnable#B0F_X::OnYetifly");
    addtimer2(15000, "Impregnable#B0F_X::OnHeartbeat");
    end;

OnHeartbeat:
    if (getmap() != "026-0") end;
    if (ispcdead())
        warp "025-1", 100, 83;
    addtimer2(15000, "Impregnable#B0F_X::OnHeartbeat");
    end;

OnYetifly:
    .@q=getq(General_Fortress);
    .@q2=getq2(General_Fortress);
    .@q3=getq3(General_Fortress);
    if (.@q != 1) end;
    if (.@q2 < 8191) end;

    dispbottom b(l("You have defeated the Yetifly. Access to B1F granted."));
    setq General_Fortress, 2, 0;
    Zeny+=100000;
    slide 98, 41;
    end;
}

