// TMW2 scripts.
// Author:
//    Jesusalva
// Description:
//    Fires of Steam: The Death of Andrei Sakar

//////////////////////////////////////////
// Monster Control
-	script	SteamFire#Ctrl	32767,{
    function SFsetup1;
    function SFsetup2;
    function SFsetup3;
    function SFsetup4;
    function SFsetup5;
    function SFsetup6;
    function SFsetup7;
    function SFsetup8;
    function SFreset;
    end;

OnInit:
    .e1$="SteamFire#Ctrl::OnEvent1";
    .e2$="SteamFire#Ctrl::OnEvent2";
    .e3$="SteamFire#Ctrl::OnEvent3";
    .e4$="SteamFire#Ctrl::OnEvent4";
    .e5$="SteamFire#Ctrl::OnEvent5";
    .e6$="SteamFire#Ctrl::OnEvent6";
    .e7$="SteamFire#Ctrl::OnEvent7";
    .e8$="SteamFire#Ctrl::OnEvent8";
    SFsetup1();
    SFsetup2();
    SFsetup3();
    SFsetup4();
    SFsetup5();
    SFsetup6();
    SFsetup7();
    SFsetup8();
    end;

function SFspawn {
    .@am=max(1, $FIRESOFSTEAM > getarg(8) ? (getarg(6)*4/10) : getarg(6));
    //debugmes "Spawning %d/%d mobs on %s", .@am, getarg(6), getarg(0);
    areamonster(getarg(0), getarg(1), getarg(2), getarg(3), getarg(4),
                strmobinfo(1, getarg(5)), getarg(5), .@am,
                getarg(7));
    if ($FIRESOFSTEAM > getarg(8) && !$@FOS_RESPAWN[getarg(8)]) {
        $@FOS_RESPAWN[getarg(8)]=true;
        setmapflag(getarg(0), mf_nopenalty);
    }

    return;
}

function SFsetup1 {
    SFspawn("029-1", 15, 15, 77, 120, Scar, 60, .e1$, 1);
    SFspawn("029-1", 15, 15, 255, 255, Crafty, 320, .e1$, 1);
    SFspawn("029-1", 15, 15, 255, 255, GiantMutatedBat, 30, .e1$, 1);
    SFspawn("029-1", 15, 15, 255, 255, Scar, 100, .e1$, 1);
    SFspawn("029-1", 195, 15, 290, 100, Forain, 54, .e1$, 1);
    SFspawn("029-1", 77, 15, 195, 75, GreenDragon, 45, .e1$, 1);
    SFspawn("029-1", 75, 75, 205, 100, EliteDuck, 40, .e1$, 1);
    SFspawn("029-1", 15, 90, 75, 280, Terranite, 90, .e1$, 1);
    SFspawn("029-1", 177, 140, 280, 280, JackO, 80, .e1$, 1);
    SFspawn("029-1", 175, 75, 280, 200, RedSkullSlime, 60, .e1$, 1);
    SFspawn("029-1", 75, 175, 185, 280, Michel, 60, .e1$, 1);
    SFspawn("029-1", 80, 100, 180, 160, CopperSkullSlime, 60, .e1$, 1);
    // MAP BOSS
    monster("029-1", 147, 153, "Level Boss", MonsterGeneral, 1, .e1$);
    return;
}

function SFsetup2 {
    SFspawn("029-2", 20, 28, 51, 33, BlackMamba, 15, .e2$, 2);
    SFspawn("029-2", 20, 34, 51, 42, GreenSkullSlime, 20, .e2$, 2);
    SFspawn("029-2", 20, 42, 32, 70, Centaur, 15, .e2$, 2);
    SFspawn("029-2", 39, 42, 51, 70, TerraniteProtector, 15, .e2$, 2);
    SFspawn("029-2", 32, 42, 40, 70, GoboBear, 13, .e2$, 2);
    SFspawn("029-2", 20, 20, 35, 70, Moonshroom, 5, .e2$, 2);
    SFspawn("029-2", 20, 20, 35, 70, RobinBandit, 5, .e2$, 2);
    // MAP BOSS
    monster("029-2", 46, 68, "Level Boss", YetiKing, 1, .e2$);
    return;
}

function SFsetup3 {
    SFspawn("029-3", 20, 20, 130, 100, DustGatling, 40, .e3$, 3);
    SFspawn("029-3", 20, 20, 130, 100, Scar, 50, .e3$, 3);
    SFspawn("029-3", 20, 20, 130, 100, Skeleton, 80, .e3$, 3);
    SFspawn("029-3", 20, 20, 130, 100, GreenSkullSlime, 20, .e3$, 3);
    SFspawn("029-3", 20, 20, 48, 62, Troll, 25, .e3$, 3);
    SFspawn("029-3", 20, 62, 53, 100, Michel, 30, .e3$, 3);
    SFspawn("029-3", 47, 20, 72, 50, LavaSkullSlime, 25, .e3$, 3);
    SFspawn("029-3", 72, 20, 130, 50, ShadowPixie, 18, .e3$, 3);
    SFspawn("029-3", 67, 49, 130, 64, GoboBear, 25, .e3$, 3);
    SFspawn("029-3", 67, 62, 99, 100, BlackMamba, 30, .e3$, 3);
    SFspawn("029-3", 100, 62, 130, 100, JackO, 30, .e3$, 3);
    // MAP BOSS
    monster("029-3", 82, 89, "Level Boss", FallenKing2, 1, .e3$);
    return;
}

function SFsetup4 {
    SFspawn("029-4", 20, 20, 130, 100, DustRifle, 40, .e4$, 4);
    SFspawn("029-4", 20, 20, 130, 100, Skeleton, 60, .e4$, 4);
    SFspawn("029-4", 20, 20, 130, 100, ArmoredSkeleton, 20, .e4$, 4);

    SFspawn("029-4", 38, 22, 120, 28, FireSkull, 30, .e4$, 4);
    SFspawn("029-4", 38, 30, 128, 44, GreenSkullSlime, 30, .e4$, 4);
    SFspawn("029-4", 20, 22, 35, 68, BlackSkullSlime, 30, .e4$, 4);
    SFspawn("029-4", 37, 44, 128, 60, Reaper, 24, .e4$, 4);
    SFspawn("029-4", 88, 60, 128, 85, Jhon, 20, .e4$, 4);
    SFspawn("029-4", 20, 69, 60, 101, Mandragora, 20, .e4$, 4);
    SFspawn("029-4", 62, 61, 86, 92, JackO, 15, .e4$, 4); // BOSS
    SFspawn("029-4", 60, 94, 128, 101, NulityPixie, 15, .e4$, 4);
    // MAP BOSS
    monster("029-4", 73, 77, "Level Boss", PsiConscience, 1, .e4$);
    return;
}

function SFsetup5 {
    SFspawn("029-5", 20, 20, 130, 100, CursedArcher, 40, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, Skeleton, 20, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, ArmoredSkeleton, 60, .e5$, 5);

    SFspawn("029-5", 20, 20, 130, 100, Reaper, 32, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, JackO, 12, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, Mandragora, 24, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, Jhon, 12, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, ShadowTortuga, 1, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, RedSkullSlime, 32, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, EvilScythe, 2, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, Michel, 8, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, SiegeTower, 3, .e5$, 5);
    SFspawn("029-5", 20, 20, 130, 100, GreenSlimeMother, 15, .e5$, 5);
    // MAP BOSS
    SFspawn("029-5", 20, 20, 130, 100, SiegeTower, 3, .e5$, 5);
    monster("029-5", 70, 26, "Level Boss", TerraniteKing, 1, .e5$);
    return;
}

function SFsetup6 {
    SFspawn("029-6", 20, 20, 180, 100, CursedArcher, 40, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, ArmoredSkeleton, 60, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, Fluffy, 40, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, SilkWorm, 40, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, SiegeTower, 4, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, EliteDuck, 24, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, TerraniteProtector, 14, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, Snail, 7, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, PinkieSuseran, 4, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, Junglefowl, 8, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, Tengu, 12, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, Moubi, 2, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, SuperiorShroom, 2, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, Moonshroom, 12, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, GreenDragon, 14, .e6$, 6);
    SFspawn("029-6", 20, 20, 180, 100, GoboBear, 8, .e6$, 6);
    // MAP BOSS
    monster("029-6", 31+9, 91, "Level Boss", PinkieEmperor, 1, .e6$);
    return;
}

function SFsetup7 {
    SFspawn("029-7", 20, 20, 120, 130, Wolvern, 24, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, GoboBear, 20, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, GreenDragon, 8, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, SiegeTower, 8, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, Tengu, 12, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, Forain, 18, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, Golem, 12, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, ShadowTortuga, 8, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, WaterElement, 2, .e7$, 7);
    SFspawn("029-7", 20, 20, 120, 130, EvilWisp, 2, .e7$, 7);
    // MAP BOSS
    monster("029-7", 66, 35, "Level Boss", PanthomLord, 1, .e7$);
    return;
}

function SFsetup8 {
    SFspawn("029-8", 20, 20, 240, 220, Assassin, 30, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, DeathCat, 20, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, LogHead, 20, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, RobinBandit, 20, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, GrassSnake, 35, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, EliteDuck, 22, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, BlackMamba, 16, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Centaur, 24, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, GreenSkullSlime, 18, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Yetifly, 6, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Snail, 22, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, PinkieSuseran, 12, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Jhon, 12, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Mandragora, 12, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, PinkieMaximus, 12, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Junglefowl, 12, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Tengu, 11, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Moubi, 11, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, SuperiorShroom, 11, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, Nutcracker, 10, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, SiegeTower, 8, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, GreenhornAbomination, 32, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, ShadowTortuga, 10, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, FireElement, 8, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, WaterElement, 8, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, EarthElement, 8, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, WindElement, 8, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, SacredWisp, 4, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, EvilWisp, 4, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, PanthomWisp, 4, .e8$, 8);
    SFspawn("029-8", 20, 20, 240, 220, EpiphanyWisp, 4, .e8$, 8);
    // MAP BOSS
    monster("029-8", 206, 149, "Level Boss", Tortuga, 1, .e8$);
    return;
}


// SFreset(map, state)
function SFreset {
    .@m$=getarg(0);
    .@s=getarg(1);
    .@m=mobcount(.@m$, "all");
    if (!.@m) {
        if ($FIRESOFSTEAM == .@s) {
            $FIRESOFSTEAM+=1;
            // 3 hours cooldown (180 min)
            if (gettime(GETTIME_YEAR) != 2021)
                $FIRESOFSTEAM_CD=gettimetick(2)+10800;
            return 2;
        } else {
            mapannounce(.@m$, "Map cleared!", 0);
            sleep(rand2(15000, 30000));
            return 1;
        }
    } else if (.@m % 25 == 0) {
        mapannounce(.@m$, sprintf("%s monster(s) left", fnum(.@m)), 0);
    }
    return 0;
}

OnEvent1:
    .@f=SFreset("029-1", 1);
    if (.@f == 2)
        mapannounce("029-1", "Dracula's Castle magical seal has dissipated!", 0);
    if (.@f)
        SFsetup1();
    end;

OnEvent2:
    .@f=SFreset("029-2", 2);
    if (.@f == 2)
        mapannounce("029-2", "Map cleared!", 0);
    if (.@f)
        SFsetup2();
    end;

OnEvent3:
    .@f=SFreset("029-3", 3);
    if (.@f == 2)
        mapannounce("029-3", "Map cleared!", 0);
    if (.@f)
        SFsetup3();
    end;

OnEvent4:
    .@f=SFreset("029-4", 4);
    if (.@f == 2)
        mapannounce("029-4", "Map cleared!", 0);
    if (.@f)
        SFsetup4();
    end;

OnEvent5:
    .@f=SFreset("029-5", 5);
    if (.@f == 2)
        mapannounce("029-5", "Map cleared!", 0);
    if (.@f)
        SFsetup5();
    end;

OnEvent6:
    .@f=SFreset("029-6", 6);
    if (.@f == 2)
        mapannounce("029-6", "Map cleared! You'll need a Druid Tree Branch to use the tree warp to next area.", 0);
    if (.@f)
        SFsetup6();
    end;

OnEvent7:
    .@f=SFreset("029-7", 7);
    if (.@f == 2)
        mapannounce("029-7", "Swamps cleared! Meet Andrei Sakar on the other side!", 0);
    if (.@f)
        SFsetup7();
    end;

OnEvent8:
    .@f=SFreset("029-8", 8);
    if (.@f == 2)
        mapannounce("029-8", "Map cleared! Contact a GM in Migglemire Town!", 0);
    if (.@f)
        SFsetup8();
    end;
}


