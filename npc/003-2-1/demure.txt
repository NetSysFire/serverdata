// TMW2 Script
// Author:
//    Crazyfefe
//    Jesusalva

003-2-1,51,41,0	script	NotSoBot	NPC_FEMALE,{

    RegEasterEgg(EE_DEMURE, 5);

    // Let's try without freeloop
    mesn any("NotSoBot", "Demure");
    if (rand(1,5) <= 2)
        mes "Blame Saulc";

    .@mx=rand(6,12);
	for (.@i = 0; .@i < .@mx; ++.@i) {
        mes "leave Complaints";
        if (rand(1,5) == 3)
            mes "";
        if (.@i == 10) {
            next;
            mesn any("NotSoBot", "Demure");
            mes any("leave Complaints", "Blame Saulc");
        }
	}
    if (rand(1,5) >= 4)
        mes "Blame Saulc";
    close;

OnHydra:
    areamonster "003-2-1", 20, 20, 65, 60, strmobinfo(1, GreenDragon), GreenDragon, any(1,1,2), "NotSoBot::OnHydra2";
    setq Q_DragonFarm, getq(Q_DragonFarm)+1;
    if (getq(Q_DragonFarm) > 100) goto L_Kick;
    end;

OnHydra2:
    setq Q_DragonFarm, getq(Q_DragonFarm)+1;
    if (getq(Q_DragonFarm) > 100) goto L_Kick;
    end;

L_Kick:
    .@t=900+min(2700, getq3(Q_DragonFarm)*60);
    setq Q_DragonFarm, 1, gettimetick(2)+.@t, getq3(Q_DragonFarm)+1;
    warp "003-2", 28, 41;
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Cap);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RedStockings);
    setunitdata(.@npcId, UDT_HEADBOTTOM, BunnyEars);
    setunitdata(.@npcId, UDT_WEAPON, GMRobe);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
