// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Permanently repeatable quest, without any special limit

017-1,123,58,0	script	Boring NPC	NPC_RUMLY,{
    .@price=35;
    mesn;
    mesq l("Hwellow. I am a boring NPC which makes green dye."); // Typo on purpose: Uwhaaaaah (as in waking up) + hello
    next;
    mesn;
    mesq l("Hwaaaah... Man, I'm bored. Will you be getting @@?", getitemlink(GreenDye));
    if ($EVENT$ == "Patrick") {
        mesc l("It's St. Patrick Day, so be warned everyone wants a green dye... So, I raise the prices.");
        .@price*=2;
    }
    next;
    mesn;
    mes l("For you, boring person like me, It'll cost the small amount of:");
    mes l("* @@/@@ @@", countitem(CobaltHerb), .@price+10, getitemlink(CobaltHerb));
    mes l("* @@/@@ @@", countitem(GambogeHerb), .@price+10, getitemlink(GambogeHerb));
    mes l("* @@/@@ @@", countitem(BugLeg), .@price-10, getitemlink(BugLeg));
    mes l("@@/@@ GP", format_number(Zeny), format_number(.@price*2));
    next;
    select
        l("Do it!"),
        l("What a rip-off!");
    mes "";
    if (@menu == 2) {
        mesn;
        mesq l("Aha! I knew you were a boring person...");
        close;
    }
    if (
        countitem(CobaltHerb)   < .@price+10 ||
        countitem(GambogeHerb)  < .@price+10 ||
        countitem(BugLeg)       < .@price-10 ||
        Zeny                    < .@price*2
        )
            goto L_NotEnough;
    inventoryplace GreenDye, 1;
    delitem CobaltHerb, .@price+10;
    delitem GambogeHerb, .@price+10;
    delitem BugLeg, .@price-10;
    Zeny-=.@price*2;
    getitem GreenDye, 1;
    getexp 200, 100; // Job Experience >>> Base Experience
    mesn;
    mesq l("There you go, boring person... You will look like the trees here. Ugh, disgusting.");
    close;

L_NotEnough:
    mesn;
    mesq l("Please don't bore me even more... That's clearly not everything.");
    next;
    mesn;
    mesq l("I wonder if anyone will notice or care if I take a nap here...");
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

