// Map 025-1: Fortress Town - Holy Land
// This file is generated automatically. All manually added changes will be removed when running the Converter.
"npc/025-1/_config.txt",
"npc/025-1/_mobs.txt",
"npc/025-1/_warps.txt",
"npc/025-1/anin.txt",
"npc/025-1/commander.txt",
"npc/025-1/ctrl.c",
"npc/025-1/drahcir.txt",
"npc/025-1/ihclot.txt",
"npc/025-1/phoenix.txt",
"npc/025-1/rum.txt",
"npc/025-1/salohcin.txt",
"npc/025-1/selim.txt",
"npc/025-1/teleporter.txt",
"npc/025-1/xovilam.txt",
"npc/025-1/yuko.txt",
