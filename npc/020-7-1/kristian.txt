// TMW2 scripts.
// Authors:
//    Jesusalva
//    TMW Org.
// Description:
//    In charge to clear the mess
//    helperCleaning* + helperJanitor

// Quest: NivalisQuest_BlueSageSlimes
//  1: Acception state (0 - Not accepted, 1 - Accepted, 2 - Complete)
//  2: Killed Slimes Control
//  3: Killed Nests Control

020-7-1,62,83,0	script	Kristian	NPC_BLUESAGEWORKER_MA,{
    function askQuestion;
    mesn;
    mesq l("Welcome. Please don't go deeper into the library, there are still some slimes left. But in spite of that we have made quite a bit of progress. You should have seen the mess just after most of the slimes escaped!");
    askQuestion();
    close;

function askQuestion {
    do {
        .@q=getq(NivalisQuest_BlueSage);
        .@q2=getq2(NivalisQuest_BlueSage);
        .@q3=getq3(NivalisQuest_BlueSage);
        .@qs=BSQuestion(getq(NivalisQuest_BlueSage));
        .@qt=getq3(NivalisQuest_BlueSageSlimes);
        next;
        if (.@qt >= 255) {
            mesn;
            mesq l("I'm so glad that you took care of the remaining slimes. Thanks! Do you need anything else?");
        } else if (.@qt) {
            mesn;
            mesq l("Wow, it's really generous of you to fight those slimes for us. Do you need anything else?");
        } else {
            mesn;
            mesq l("Do you need anything else?");
        }
        mes "";
        select
            rif(.@qt < BS_SNESTALL, ""),
            rif(.@qs & BS_QVISITOR, l("Do you know anything about the strange visitor?")),
            rif(.@qs & BS_QHELPER, l("What's your opinion of Peetu and his work?")),
            any(l("I need to leave."), l("See you."), l("Bye."));
        mes "";
        switch (@menu) {
            case 2:
                mesn;
                mesq l("Oh, there was such a visitor, yes. Strange guy, and didn't really know how to behave. He sneaked into the workshop area all the time, I think he was rather curious about the things going on there.");
                next;
                mesn;
                mesq l("But still, he can't just walk in there, fiddle with the experiments and disturb the helpers doing their work.");
                if (!(.@q2 & .bsId))
                    setq2 NivalisQuest_BlueSage, .@q2 | .bsId;
                break;
            case 3:
                mesn;
                mesq l("Peetu? I think he's a good guy. Very focused on his work, I think. I don't have that much to do with him, so I can't really tell.");
                break;
        }
    } while (@menu != 4);
    close;
}

OnInit:
    .bsId=BS_NPC05;
    .sex=G_MALE;
    .distance=5;
    end;
}

