// TMW2 Script
// Author:
//    Jesusalva
// Personality Traits:
//    Bitter-sweet, resentful, lovely, cute
// Description:
//    Former Town's Mayor Daughter.
//    Have a grudge against current mayor.
//    Her father was banished from the town after the famine and the Monster Attack (Siege)
//    TODO: Remember to use disablenpc() and enablenpc() to keep consistency with Hurns Liberation Day
//    A new mayor was elected, which was her family political enemy. She misses her father,
//    which she doesn't knows where he is because the banishment. Main storyline unlocks
//    this side quest, but the main storyline keeps going forward regardless of that.
//    NOTE: Here, 'main storyline' is character story, do not mistake with world main storyline.

012-7,52,58,0	script	Airlia	NPC_ELF_F,{
    .@q1=getq(HurnscaldQuest_Farmers);
    .@n = getq(General_Narrator);
    if (strcharinfo(0) == $MOST_HEROIC$) npctalk3 l("Ah, if it isn't @@? You're the talk of the town!", $MOST_HEROIC$);
    if (.@q1 == 4) goto L_Letter;
    if (.@n >= 3 && .@n < 6) goto L_Mayor;
    if (.@n == 9) goto L_Nivalis;
    mesn;
    mesq l("Oh, a visitor. Welcome to the city hall. Please, enjoy your stay.");
    close;

// Hurnscald Farmers Quest
L_Letter:
    mesn;
    mesq l("You're the savior of Hurnscald crops. Half from the world would die from famine, weren't for you.");
    next;
    mesn;
    mesq l("We're currently the biggest exporter of food. We may be a small town, but Tulimshar and Halinarzo can barely hold themselves.");
    next;
    mesn;
    mesq l("Here, take this @@. This is the proof that you've helped Hurnscald greatly. This shall open new opportunities to you.", getitemlink(CottonBoots));
    inventoryplace CottonBoots, 1;
    setq HurnscaldQuest_Farmers, 5;
    getitem CottonBoots, 1;
    getexp 0, 1000;
    close;

// Main Storyline & Airlia Quest
L_Mayor:
    .@q=getq2(General_Narrator);

    // 3 - Quest Not Assigned
    if (.@n == 3)
        goto L_In1;

    // 4 - Airlia doesn't wants to talk about </3
    if (.@n == 4) {
        if (.@q == 2) goto L_In3;
        if (.@q == 1) goto L_In2;
    }

    // 5 - Access to Rakinorf was granted
    if (.@n == 5)
        goto L_In4;

    mesc l("Script Error"), 1;
    close;

L_In1:
    mesn strcharinfo(0);
    mesq l("Hello! Could I see the Mayor?");
    next;
    mesn;
    mesq l("Ah... The mayor. The current mayor. Why do you even want to talk to him, anyway?");
    next;
    mesn;
    mesq l("Anyway, he is not available at the moment. Try your luck again later.");
    setq General_Narrator, 4, 1;
    close;

L_In2:
    mesn strcharinfo(0);
    mesq l("Hello! Could I see the Mayor?");
    next;
    mesn;
    mesq l("I already told you he is out! Why do you even want to talk to him?! Haven't I told you to... to... leave me alone!?");
    setq2 General_Narrator, 2;
    close;

L_In3:
    mesn strcharinfo(0);
    mesq l("Hello! Could I see the Mayor?");
    next;
    mesn;
    mes "...";
    next;
    mes "... ...";
    next;
    mes "... ... ...";
    next;
    mesn;
    mesq l("Fine.");
    next;
    mesn;
    mesq l("The mayor, Rakinorf, is probably on the Inn upstairs, drinking himself to death.");
    next;
    mesn;
    mesq l("Usually only sponsors are allowed up there, but I'll make an exception today. Go meet the mayor.");
    setq General_Narrator, 5, 0;
    close;

L_In4:
    if (.@q == 5)
        goto L_In5;
    mesn;
    mesq l("The mayor, Rakinorf, is probably on the Inn upstairs, drinking himself to death.");
    next;
    mesn;
    mesq l("Usually only sponsors are allowed up there, but I'll make an exception today. Go meet the mayor.");
    close;

L_In5:
    mesn;
    mesq l("Go away.");
    next;
    mesn strcharinfo(0);
    mesq l("I can't. Rakinorf told me I should bring this matter to you instead.");
    next;
    mesn;
    mesq l("That lazy mayor... He can't do anything on his own!! Sure, what do you need?");
    next;
    mesn strcharinfo(0);
    mesc l("You explain to Airlia about you wanting to travel to Halinarzo to find clues.");
    next;
    mesn;
    mesq l("What, only that? Well, this is actually simpler than it looks.");
    next;
    mesn;
    mesq l("Basically, there's no sea route to Halinarzo. You must head southeast of Tulimshar.");
    next;
    mesn;
    mesq l("Halinarzo lies past the Canyon, but the @@ there are level @@.", getmonsterlink(Snake), strmobinfo(3, Snake));
    next;
    mesn;
    mesq l("So, you would die a few thousands of times before ever reaching Halinarzo.");
    next;
    mesn;
    mesq l("But with a @@, when you are about to die, you can warp back to Soul Menhir and bypass death penalty.", getitemlink(ReturnPotion));
    next;
    mesn;
    mesq l("That's a super rare potion, and it expires after some time. Take care!");
    setq2 General_Narrator, 6;
    close;

L_Nivalis:
    // Supposed to send player to talk with another NPC on Nivalis about World's Edge
    mesn;
    mesq l("Going to World Edge? Never heard of.");
    next;
    mesn;
    mesq l("My father probably knew... Well, you better ask Blue Sage then.");
    next;
    mesn;
    mesq l("The Blue Sage is Nivalis Mayor and very knowledgeable on stuff. Lemme write a letter of recommendation for you and sign it in Rakinorf's stead...");
    next;
    mesc b(l(".:: Main Quest 4-1 ::.")), 3;
    mesc l("* Meet the Blue Sage"), 9;
    setq General_Narrator, 10, 0;
    next;
    mesn;
    mesq l("There you go. I wish you good luck. @@", col(l("*grumpf*"), 9));
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, Earmuffs);
    setunitdata(.@npcId, UDT_HEADMIDDLE, RedknightArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, Boots);
    setunitdata(.@npcId, UDT_WEAPON, CottonSkirt);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 20);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 5);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}

