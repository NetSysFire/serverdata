// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    This script controls access to Ships, fixing variables.

003-1,82,68,0	script	TulimShip	NPC_HIDDEN,0,0,{

OnTouch:
    EnterTown("Tulim");
    goto L_Warp;

L_Warp:
    warp "002-3@"+LOCATION$, 31, 28;
    closedialog;
    close;
}

003-1,120,25,0	script	TulimShip#M	NPC_HIDDEN,0,0,{

OnTouch:
    EnterTown("Tulim");
    goto L_Warp;

L_Warp:
    /* Is Hurnscald already liberated? Precendence. */
    if (!$HURNS_LIBDATE) {
        .@online=$@BG1_SIZE;
        if (is_gm())
            dispbottom l("GMs are NOT allowed on Hurnscald Liberation day.");
        else if (.@online)
            dispbottom l("Right click on the NPC to join the Liberation Force on Hurnscald.");
        else
            dispbottom l("The ship is locked, probably unable to leave port.");

        if (!.@online)
            npctalk3 col(l("A Game Master is required to begin the Liberation Day."), 1);
    end;
    }

    warp "016-1@"+LOCATION$, 21, 26;
    closedialog;
    close;
}
