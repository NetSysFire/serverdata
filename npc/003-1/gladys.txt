// Author:
//    Jesusalva

003-1,63,105,0	script	Gladys	NPC_FEMALE_TONORI,{

    mesn;
    mesq l("Hello darling. The Alliance sucks, but don't tell anyone I told you that.");
    mes "";
    menu
        l("What is the Alliance?"), L_What,
        l("Why it sucks?"), L_Why,
        l("Well, too bad!"), L_Close;

L_What:
    mes "";
    mesn;
    mesq l("What Alliance? The Alliance which rules over the World Of Mana!");
    next;

L_AllianceMenu:
    mes "";
    menu
        l("Who composes the Alliance?"), L_Members,
        l("What are the Alliance tasks?"), L_Tasks,
        l("Do you think I could join the Alliance?"), L_Join,
        l("I have to go. See you later."), L_Close;

L_Why:
    mes "";
    mesn;
    mesq l("Because they control everything! They even determine taxes!");
    next;
    mesq l("They also hold control over the Mana Stones, but this is not the problem.");
    next;
    mesq l("In the end, they decide the defensive measures, and they even set curfews!");
    next;
    mesq l("They also determine rules, and enforces them. We live under a disguised dictatorship.");
    next;
    mesq l("But what I really hate is that they forbid gossiping.");
    next;
    goto L_AllianceMenu;

L_Members:
    mes "";
    mesn;
    mesq l("Oh, every settlement is part of it. Candor, Tulimshar, Hurnscald, Frostia, Halinarzo, Artis, Nivalis...");
    next;
    mesq l("Each settlement names a representative, which forms the Alliance Council. There is also the High Council, and the Magic Council.");
    next;
    mesq l("The Magic Council can be found at this city. They are the most powerful mages. If you have an impressive magical affinity, they may entitle you to have access to a Mana Stone.");
    next;
    mesq l("The Alliance Council rarely meets. It's thanks to them that the cities are well protected, if you want my opinion.");
    next;
    mesq l("The High Council acts in the Alliance stead, as it is not possible to reunite every city leader for all minor matters.");
    next;
    mesq l("Saulc, Crazyfefe and Jesus Saves are the three High Councillors. Don't do anything illegal while they're watching!");
    next;
    goto L_AllianceMenu;

L_Tasks:
    mes "";
    mesn;
    mesq l("Their main tasks are ensuring trade, protecting the cities from monster invasions, and guarding the Mana Stones.");
    next;
    mesq l("They also recently took over some city affairs. It's good they're protecting us, but I'm afraid of they creeping in our lives!");
    next;
    goto L_AllianceMenu;

L_Join:
    mes "";
    mesn;
    mesq l("Unless you are an exceptionally talented mage or an influentical politician, it is unlikely.");
    next;
    mesq l("The High Council's have some subordinates, but those are hand-picked by them.");
    next;
    mesq l("You could try getting into a city administration. If you can get a citizenship, you can try to elect for an office.");
    next;
    mesq l("Otherwise, you could ask the Constable. They act in the Alliance stead to enforce law and order, and may have... special arrangements for you.");
    next;
    goto L_AllianceMenu;

L_Close:
    mes "";
    mesq l("Good bye, darling!");
    close;

OnInit:
    .@npcId = getnpcid("Gladys");
    setunitdata(.@npcId, UDT_HEADTOP, TrapperHat); // Whaaaaaaaaat
    setunitdata(.@npcId, UDT_HEADMIDDLE, ValentineDress);
    setunitdata(.@npcId, UDT_HEADBOTTOM, CottonTrousers);
    setunitdata(.@npcId, UDT_WEAPON, DeepBlackBoots); // Boots
    setunitdata(.@npcId, UDT_HAIRSTYLE, 20);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 20);

    .sex = G_FEMALE;
    .distance = 5;
    end;
}
