// TMW-2 Script
// Author:
//    Saulc
//    Jesusalva
// Description:
//    Another shopkeeper which babbles about Jean Shorts, how Cave Snake drops them
//    and encourage players to find it. He pays 6.000 GP and gives 1620 xp for one.
//    In future, this could grant you discounts on his shop, or allow a quest with
//    a family member from his.

003-1,103,106,0	script	Neko	NPC_M_SHOPKEEPER,{
    .@q=getq(TulimsharQuest_Neko);
    mesn;
    mesq l("Only finest wares!");
    mes "";
    select
        l("Trade"),
        rif(BaseLevel > 20 && .@q == 0, l("Why do you only use the silk robe? Don't you have a shorts or something?")),
        rif(.@q == 1 && countitem(JeansShorts) >= 1, l("Hey... I found a Jeans Shorts. Is it yours?")),
        l("Leave");

    if (@menu == 2) {
        mesn;
        mesq l("Oh, that's a long story.");
        next;
        mesn;
        mesq l("You see, I was walking in the secret caves near Ched, and... well...");
        next;
        mesn;
        mesq l("The @@ stole my @@. Hahah. I can't find anywhere else to buy it.", getmonsterlink(CaveSnake), getitemlink(JeansShorts));
        next;
        setq TulimsharQuest_Neko, 1;
        mesn;
        mesq l("If you bring friends, I'm sure they'll be no match. Ah... How many people already lost their shorts to Cave Snakes?");
        next;
    }
    if (@menu == 3) {
        getexp rand(120,200), 0;
        setq TulimsharQuest_Neko, 2;
        mesn;
        mesq l("Oh... Wow! I'm surprised.");
        next;
        mesn;
        mesq l("Please, keep it. I don't need it at all!");
        next;
        mesn;
        mesq l("This silk robe is more than enough, and you are fighting, you need the def bonus more than me. %%2");
        next;
    }

    closedialog;
    if (@menu == 1) {
        npcshopattach(.name$);
        shop .name$;
    }
    goodbye;
    close;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, NPCEyes);
    setunitdata(.@npcId, UDT_HEADMIDDLE, SilkRobe);
    //setunitdata(.@npcId, UDT_HEADBOTTOM, LeatherTrousers); // FIXME: LeatherTrousers are BROKEN!
    setunitdata(.@npcId, UDT_WEAPON, AssassinBoots);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 26);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 2);

    tradertype(NST_MARKET);
    sellitem LeatherShirt, 12000, 1;
    sellitem ShortTankTop, 8000, 1;
    sellitem TrainingBow, 990, 2;
    sellitem CottonCloth, -1, 3;
    sellitem RoastedMaggot, -1, 2;
	sellitem ArrowAmmoBox,-1,rand(5,10);
    sellitem Arrow, -1, rand(10000,30000);
    sellitem CroconutBox, rand(2650,2950), 4;
    sellitem EmptyBox, -1, 2;
    sellitem ChamomileTea, getiteminfo(ChamomileTea, ITEMINFO_BUYPRICE)*15/10, 10;

    .sex = G_MALE;
    .distance = 5;
    end;

OnClock1149:
OnClock2359:
    restoreshopitem LeatherShirt, 12000, 1;
    restoreshopitem ShortTankTop, 8000, 1;
    restoreshopitem TrainingBow, 990, 2;
    restoreshopitem CottonCloth, 3;
    restoreshopitem RoastedMaggot, 2;
	restoreshopitem ArrowAmmoBox,rand(5,10);
    restoreshopitem Arrow, rand(10000,30000);
    restoreshopitem CroconutBox, rand(2650,3000), 4;
    restoreshopitem EmptyBox, 2;
    restoreshopitem ChamomileTea, getiteminfo(ChamomileTea, ITEMINFO_BUYPRICE)*15/10, 10;
    end;

// Pay your taxes!
OnBuyItem:
    debugmes("Purchase confirmed");
    PurchaseTaxes();
    end;

OnSellItem:
    debugmes("Sale confirmed");
    SaleTaxes();
    end;
}
