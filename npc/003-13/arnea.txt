// TMW2 Scripts
// Author:
//    4144
//    Jesusalva
// Description:
//    Arena for Duels and PVP (temporary map)

003-13,29,29,0	script	Arnea#003-13	NPC_ELF_F,{
    /*
    // FIXME
    warp "003-10", 22, 62;
    close;
    */
    if (is_staff()) {
        mes "npc name: " + .name$;
        mes "npc ext name: " + .extname$;
        mes "npc id: " + .id;
        mes "npc parent id: " + .parent;
        mes "npc src id: " + .srcId;
        mes "char id 3: " + getcharid(3);
        mes "instance id: " + instance_id();
        mes "Map ID: " + instance_mapname("003-13");
    }
    if (instance_id() >= 0) {
        goto L_Manage;
    } else {
        // Non staff and on 003-13? That's a bug!
        if (!is_staff()) {
            atcommand "@request Hey hey hey, player found in 003-13 - regular map! Report this to Jesusalva at once!";
            warp "Save", 0, 0;
            close;
        }
        mes "npc not in instance";
    }

    close;

L_Manage:
    mesn;
    select
        l("warp back"),
        rif(!'UDTf, l("begin Doppelganger Challenge")),
        l("Doppelganger Challenge Ranking"),
        l("cancel");

    switch (@menu)
    {
        case 1:
            warp "003-10", 22, 62;
            break;
        case 2:
            mesc l("spawn challenge monster! How far can you go?"), 2;
            mesc l("The Doppelganger Challenge will create a Gladiator Monster which should somehow emulate a PvP experience.");
            mesc l("In other words, it'll make a copy of youself.");
            mesc l("Defeating it will advance the round. How far can you survive?");
            next;
            mesc l("Really begin the Doppelganger Challenge?");
            mesc l("Switching from strong to weak equipments WON'T make it go any easier on you!"), 1;
            if (askyesno() == ASK_NO)
                break;

            npctalk l("Doppelganger Challenge, @@ began the fight!", strcharinfo(0));
            // Save permanent data
            'udt_blv=BaseLevel;
            'udt_Str=readparam2(bStr);
            'udt_Agi=readparam2(bAgi);
            'udt_Vit=readparam2(bVit);
            'udt_Int=readparam2(bInt);
            'udt_Dex=readparam2(bDex);
            'udt_Luk=readparam2(bLuk);
            'udt_Dly=battleparam(UDT_ADELAY);
            'udt_Rng=battleparam(UDT_ATKRANGE);

            // Save (b)ase data
            'udt_bhp=MaxHp;
            'udt_bAtk1=battleparam(UDT_ATKMIN);
            'udt_bAtk2=battleparam(UDT_ATKMAX);
            'udt_bMatk=battleparam(UDT_MATKMAX);
            'udt_bDef=battleparam(UDT_DEF);
            'udt_bMdef=battleparam(UDT_MDEF);
            'udt_bHit=battleparam(UDT_HIT);
            'udt_bFlee=battleparam(UDT_FLEE);
            'udt_bCrit=battleparam(UDT_CRIT);

            // Save (p)rogression data
            'udt_php='udt_bhp/5;
            'udt_pAtk1='udt_bAtk1/10;
            'udt_pAtk2='udt_bAtk2/10;
            'udt_pMatk='udt_bMatk/5;
            'udt_pDef='udt_bDef/5;
            'udt_pMdef='udt_bMdef/5;
            'udt_pHit='udt_bHit/10;
            'udt_pFlee='udt_bFlee/15;
            'udt_pCrit='udt_bCrit/10;

            /*
            .@a1=battleparam(UDT_ATKRANGE);
            .@a2=battleparam(UDT_ATKMIN);
            .@a3=battleparam(UDT_ATKMAX);
            .@d1=battleparam(UDT_DEF);
            .@d2=battleparam(UDT_MDEF);
            .@c1=battleparam(UDT_HIT);
            .@c2=battleparam(UDT_FLEE);
            .@m1=battleparam(UDT_MATKMIN);
            .@m2=battleparam(UDT_MATKMAX);
            .@m3=battleparam(UDT_ELETYPE);
            .@s1=battleparam(UDT_STR);
            .@s2=battleparam(UDT_ADELAY);
            .@s3=battleparam(UDT_DMOTION);

            debugmes "Unit Data %d (Str %d)", getcharid(3), .@s1;
            debugmes "Atk (%d~%d) Range %d", .@a2, .@a3, .@a1;
            debugmes "Def %d MDef %d", .@d1, .@d2;
            debugmes "Hit %d Flee %d", .@c1, .@c2;
            debugmes "MAtk (%d~%d) Element %d", .@m1, .@m2, .@m3;
            debugmes "Attack delay %d Stun %d", .@s2, .@s3;
            */

            // Begin the fight
            doevent instance_npcname(.name$)+"::OnGladius";
            addtimer 5000, instance_npcname(.name$)+"::OnVerify";
            closeclientdialog;
            break;
        case 3:
            mesc l("All leaderboards are refreshed hourly."), 1;
            mesc l("Your current score: @@", UDTRANK), 3;
            HallOfUDT();
            break;
        case 4:
            break;
    }
    close;

OnGladius:
    sleep(800);
    .@mg=monster(instance_mapname("003-13"), 38, 32, "Gladiator", Gladiator, 1, instance_npcname(.name$)+"::OnGladius");
    // Set "permanent" data
    setunitdata(.@mg, UDT_ADELAY, 'udt_Dly-'UDTf);
    setunitdata(.@mg, UDT_ATKRANGE, 'udt_Rng+limit(0, 'UDTf/10, 3));

    // Set base data
    setunitdata(.@mg, UDT_LEVEL,    'udt_blv+'UDTf);
    setunitdata(.@mg, UDT_STR, 'udt_Str+'UDTf);
    setunitdata(.@mg, UDT_AGI, 'udt_Agi+'UDTf);
    setunitdata(.@mg, UDT_VIT, 'udt_Vit+'UDTf);
    setunitdata(.@mg, UDT_INT, 'udt_Int+'UDTf);
    setunitdata(.@mg, UDT_DEX, 'udt_Dex+'UDTf);
    setunitdata(.@mg, UDT_LUK, 'udt_Luk+'UDTf);

    // Set variable data
    setunitdata(.@mg, UDT_MAXHP,    'udt_bhp+'udt_php*('UDTf-1));
    setunitdata(.@mg, UDT_HP,       'udt_bhp+'udt_php*('UDTf-1));

    setunitdata(.@mg, UDT_ATKMIN,   'udt_bAtk1+'udt_pAtk1*('UDTf-1));
    setunitdata(.@mg, UDT_ATKMAX,   'udt_bAtk2+'udt_pAtk2*('UDTf-1));
    setunitdata(.@mg, UDT_MATKMIN,  'udt_bMatk+'udt_pMatk*('UDTf-1));
    setunitdata(.@mg, UDT_MATKMAX,  'udt_bMatk+'udt_pMatk*('UDTf-1));
    setunitdata(.@mg, UDT_DEF,      'udt_bDef+'udt_pDef*('UDTf-1));
    setunitdata(.@mg, UDT_MDEF,     'udt_Mdef+'udt_pMdef*('UDTf-1));
    setunitdata(.@mg, UDT_HIT,      'udt_bHit+'udt_pHit*('UDTf-1));
    setunitdata(.@mg, UDT_FLEE,     'udt_bFlee+'udt_pFlee*('UDTf-1));
    setunitdata(.@mg, UDT_CRIT,     'udt_bCrit+'udt_pCrit*('UDTf-1));

    setunitdata(.@mg, UDT_PDODGE,   min(30, 'udt_Luk/10+('UDTf/3)));

    'UDTf+=1;
    npctalk ("Doppelganger Challenge, wave " + 'UDTf + "!");
    maptimer(instance_mapname("003-13"), 10, instance_npcname(.name$)+"::OnUDTUpdate");
    end;

OnUDTUpdate:
    if ('UDTf > UDTRANK)
        UDTRANK='UDTf;
    getexp 'UDTf*7, 'UDTf*7;
    Mobpt+='UDTf*7;
    end;

// Check for possible cheats, and update default values
OnVerify:
    if (battleparam(UDT_ATKRANGE) > 'udt_Rng)
        'udt_Rng=battleparam(UDT_ATKRANGE);

    if (battleparam(UDT_ATKMAX) > 'udt_bAtk1) {
        'udt_bAtk1=battleparam(UDT_ATKMIN);
        'udt_bAtk2=battleparam(UDT_ATKMAX);
        'udt_pAtk1='udt_bAtk1/10;
        'udt_pAtk2='udt_bAtk2/10;
    }

    if (battleparam(UDT_DEF) > 'udt_bDef) {
        'udt_bDef=battleparam(UDT_DEF);
        'udt_pDef='udt_bDef/5;
    }

    if (battleparam(UDT_MDEF) > 'udt_bMdef) {
        'udt_bMdef=battleparam(UDT_MDEF);
        'udt_pMdef='udt_bMdef/5;
    }

    if (battleparam(UDT_MATKMAX) > 'udt_bMatk) {
        'udt_bMatk=battleparam(UDT_MATKMAX);
        'udt_pMatk='udt_bMatk/5;
    }

    if (battleparam(UDT_ADELAY) < 'udt_bDly)
        'udt_bDly=battleparam(UDT_DELAY);

    addtimer 5000, instance_npcname(.name$)+"::OnVerify";
    end;

OnInit:
    .@npcId = getnpcid(.name$);
    setunitdata(.@npcId, UDT_HEADTOP, DarkHelm);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansChaps);
    setunitdata(.@npcId, UDT_WEAPON, RockKnife);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);

    .sex = G_FEMALE;
    .distance = 9;
    end;

OnInstanceInit:
    .@npcId = getnpcid(instance_npcname(.name$));
    setunitdata(.@npcId, UDT_HEADTOP, DarkHelm);
    setunitdata(.@npcId, UDT_HEADMIDDLE, CopperArmor);
    setunitdata(.@npcId, UDT_HEADBOTTOM, JeansChaps);
    setunitdata(.@npcId, UDT_WEAPON, RockKnife);
    setunitdata(.@npcId, UDT_HAIRSTYLE, 14);
    setunitdata(.@npcId, UDT_HAIRCOLOR, 18);
    end;

}

