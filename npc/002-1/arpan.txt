// TMW-2 Script.
// Editor: Jesusalva
//
// Evol scripts.
// Authors:
//    Ablu
//    Qwerty Dragon
// Description:
//    Introduction NPC

002-1,49,36,0	script	LeftDoorCheck	NPC_HIDDEN,0,0,{
    .@q = getq(ShipQuests_Arpan);
    if (.@q == 5) doevent instance_npcname("Magic Arpan")+"::OnTalk";

    close;
}

002-1,54,36,0	script	RightDoorCheck	NPC_HIDDEN,0,0,{
    .@q = getq(ShipQuests_Arpan);
    if (.@q == 5) doevent instance_npcname("Magic Arpan")+"::OnTalk";

    close;
}


002-1,49,33,0	script	Magic Arpan	NPC_MAGIC_ARPAN,{
    showavatar NPC_MAGIC_ARPAN;

    .@q = getq(ShipQuests_Arpan);
    .@s = getq2(ShipQuests_Arpan);
    .@n = getq(General_Narrator);
    .@q_julia = getq(ShipQuests_Julia);

OnTalk:
    showavatar NPC_MAGIC_ARPAN;
    mesn;

    if (.@q > 5) goto L_Menu;
    setq ShipQuests_Arpan, 6;
    deltimer("Magic Arpan::OnSlow");

    mesq lg("Yeye, are you finally ready to go?");
    if (!TUTORIAL)
        mesc l("Protip: You skipped tutorial. A lot of tutorial-ish dialogs and quests will be skipped. You can change this anytime on %s > Game Settings.", b("@ucp"));
    next;

    select
        l("Yes, I want to find out who I am."),
        l("No, but what option do I have? I'm railroaded!");
    mes "";

    if (@menu == 2) {
        mesn;
        mesq l("Yeye is not paying for your food and is not a sailor like us! You should start standing on your own feet.");
        next;
        mesn strcharinfo(0);
        mesq l("Thanks for the honestity, I guess.");
        next;
    }

    mes "";
    mesn;
    mesq l("Anyway, our shipkeeper, Juliet, helped to heal your injuries way back.");
    next;
    mesn;
    if (.@q_julia == 0)
        setq ShipQuests_Julia, 1;
    mesq lg("Yaya, you should go see her! She'll be happy to help you again.");
    next;
    mesc b(l(".:: Main Quest 1-1 ::.")), 3;
    msObjective(false, l("Talk to @@", l("Juliet")));
    tutmes l("Juliet is east (right) of %s.", .name$), l("Protip"), false;
    next;
    goto L_Menu;


L_Menu:
    mesq l("What yeye could I do for you today?");
    next;

    menu
        rif(.@q_julia < 2, lg("Where can I find Juliet?")), L_Julia,
        rif(!.@n, lg("Could you tell me where I am?")), L_Where,
        rif(!.@n, l("Who are you?")), L_Who,
        rif(!.@n, l("I need a tutorial, where can I find help?")), L_Trainer,
        rif(!getq(ShipQuests_ArpanMoney), lg("Do you know what happened to the gold I had when you guys saved me?")), L_WhereMoney,
        l("Nothing, sorry."), -;

    closedialog;
    close;

L_Trainer:
    mes "";
    mesn;
    mesq l("There is a NPC called Trainer, just outside this ship.");
    next;
    mesn;
    mesq l("Just use the arrow key--, err, I mean, just walk to the door on the right. The one which is not guarded by Peter.");
    next;
    mesn;
    mesq lg("You must be dressed, and talk to our captain first. He'll give you a mission and unlock the ship main door.");
    next;
    mesn;
    mesq l("All you need to do then is walk outside, enter on the biggest house, and talk to the Trainer. He'll teach you everything.");
    next;
    goto L_Menu;

L_Where:
    mes "";
    mesn;
    mesq lg("You're on our ship, we made port to a little island and we're actually yeyending our long merchant travelling adventure at the city of Tulimshar.");
    next;
    mesq l("We will be yaying there in a few days, so we will drop you off there.");
    next;
    mesq l("You will see, citizens are polite and you can still ask around for help. They can help find a job for you or maybe help you find out what happened to you out at sea!");
    next;

    goto L_Menu;

L_Julia:
    mes "";
    mesn;
    mesq lg("Just go right, yeye can't miss her. She's the only girl in this crew, oh well, except for you now yeyeye!", "Just go right, yeye  can't miss her. She's the only girl in this crew.");
    next;

    mesn "Narrator";
    mesc l("Use the arrow keys to walk right and meet Juliet.");
    next;

    goto L_Menu;

L_Who:
    mes "";
    mesn;
    mesq l("Sorry! I forgot to introduce myself. My name is Arpan, but other sailors call me Magic Arpan because I know one or two yaing magic tricks.");
    next;
    mesn;
    mesq l("They're not magic, but yayaya, people like to say it is! Yeyeye.");
    next;

    goto L_Menu;

L_WhereMoney:
    mes "";
    mesn;
    mesq l("Oh right, I totally forgot about that, here you go.");
    next;

    if (BaseLevel < 4) goto L_Apana;
    setq ShipQuests_ArpanMoney, 1;
    Zeny = Zeny + 35;
    message strcharinfo(0), l("You receive @@ GP!", 35);

    goto L_BeforeMenu;

L_Apana:
    mesn;
    mesq l("On hindsight, I'll wait you get a few levels. Can't have cheaters, ya know!");
    next;
    goto L_Menu;

L_BeforeMenu:
    mesn;
    goto L_Menu;

OnSlow:
    npctalk3 l("@@, do you need help? Are you lost? Click me!", strcharinfo(0));
    /*
    setcamnpc;
    showavatar NPC_MAGIC_ARPAN;
    mesn;
    mesq l("Yayaya, @@ is surely slow. Do you remember how to walk? You can use arrow keys for that!", strcharinfo(0));
    next;
    mesn;
    mesc l("Here, come talk to me, the Magic Arpan! I'll help you get dressed."), 1;
    */
    addtimer(90000,"Magic Arpan::OnSlow");
    close;

OnInit:
    .sex = G_MALE;
    .distance = 6;
    end;
}
