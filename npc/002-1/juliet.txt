// TMW2 scripts.
// Authors:
//    4144
//    Qwerty Dragon
//    Vasily_Makarov
//    Jesusalva
// Description:
//    Allows to change language and talks about what happened to him.
//    Modified by Jesusalva for TMW2. She is the nurse and also does other minor tasks.
// Variables:
//    0 ShipQuests_Julia
// Values:
//    Julia:
//    0   Default, haven't started the game yet.
//    1   Need to see Julia.
//    2   Has been registered by Julia.
//    3   Has talked with Nard

002-1,67,26,0	script	Juliet	NPC_JULIA,2,10,{


    function sellFood {
        closeclientdialog;
        openshop;
        close;
        return;
    }



    function basicSkill {
        mes "";
        mesn;
        mesq l("Let me check into it...");
        next;
        adddefaultskills;
        mesq l("Here you go, everything is fixed.");
        emotion E_HAPPY;
        next;
        return;
    }

    function chooseLang {
        mes "";
        mesn;
        mesq l("Of course! But beware that %s are always in demand!", "[@@https://www.transifex.com/arctic-games/moubootaur-legends/|"+l("Translators")+"@@]");
        next;
        mesq l("Tell me which language you speak and I will change the note on the ship passenger list.");
        next;

        asklanguage(LANG_IN_SHIP);

        mes "";
        mesn;
        mesq l("Ok, done.");
        next;
        return;
    }

    function whereAmI {
        mes "";
        mesn;
        mesq l("You're on a ship, we're on our way to the oldest human city, Tulishmar.");
        next;
        mesq l("We should be there in a few days. For now, you can relax on the ship, or visit the island we're docked at! Its a small island, but a good place to get some exercise and stretch your legs.");
        next;
        return;
    }

    function whatHappened {
        mes "";
        mesn;
        mesq l("We thought that you could help us understand this, all we know is that we found you cast in the sea, in a sand bank.");
        next;
        mesq lg("You were in bad shape, you should be happy we found you before the sea killed you.");
        next;
        return;
    }

    function readRules {
        mes "";
        mesn;
        mesq l("Of course, they are on the left wall, go have a look at them.");
        next;
        return;
    }

    function mainMenu {
        do
        {
            .@q4 = getq(General_Narrator);

            select
                l("I am hungry. Can I buy some food here?"),
                rif(getskilllv(NV_BASIC) < 6, l("Something is wrong with me, I can't smile nor sit.")),
                lg("I made a mistake, I would like to change my language."),
                rif(!.@q4, l("Could you explain to me where I am?")),
                rif(!.@q4, l("What happened to me?")),
                l("Can I read these rules again?"),
                l("Nothing, sorry.");

            switch (@menu) {
                case 1: sellFood; break;
                case 2: basicSkill; break;
                case 3: chooseLang .@s$; break;
                case 4: whereAmI; break;
                case 5: whatHappened; break;
                case 6: readRules; break;
                case 7: closedialog; end;
            }
        } while (1);
    }

    mesn;
    mesq lg("Hello dear!");
    next;
    if (getq(ShipQuests_Julia) < 3)
        mesq l("Have you already talked to our captain? He should be downstairs waiting for you!");
    mesq l("What do you want today?");
    next;

    mainMenu;

OnTouch:
    .@q = getq(ShipQuests_Julia);
    if (.@q > 1) end;

    checkclientversion;

    mesn;
    mesq l("Hi, nice to see you!");
    next;
    mesq l("My name is Juliet, it is me who took care of you after we found you in the sea. I'm glad to see you're okay.");
    next;
    if (getq(ShipQuests_Julia) < 2) {
        mesq l("I'm sure that you've got some questions for me, feel free to ask them, but first I need to tell you the rules all adventurers must respect on this world.");
        next;

        GameRules 8 | 4;

        mesn;
        mesq l("Oh, and I almost forgot! Do not share passwords or pincodes, not even with staff! And do not use the same password somewhere else, they can be stolen!");
        next;
        mesn;
        mesq l("If you want to read this page again, there is a copy up on the wall.");
        next;
        mesn;
        mesq l("Also, take this book so you don't forget the rules. You can always read it, or type ##B@rules##b on the chat.");
        // No inventoryplace here.
        getitem BookOfLaws, 1;
        setq ShipQuests_Julia, 2;
        next;
        mesq l("I think I'm done with that now. You should now look for captain Nard downstairs. He'll be waiting for you.");
        mesq l("Do you have any questions?");
        next;
        mesc b(l(".:: Main Quest 1-2 ::.")), 3;
        msObjective(false, l("Talk to @@", l("Captain Nard")));
        next;
    }
    mainMenu;
    end;

OnInit:
    .sex = G_FEMALE;
    .distance = 10;
	sellitem Cheese;
	sellitem Aquada;
	sellitem Piberries;
	sellitem Bread;
    end;
}
