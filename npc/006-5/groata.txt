// TMW2 Script
// Author:
//    Jesusalva
// Note:
//    Groata Grotto is NOT instanced, NOT timed, and meant to be weird.

function	script	GroataGrotto	{
    .@lv = getarg(0, $@GROATA);

    .@mb[0] = MagicGoblin;
    .@mb[1] = IceMaggot;

    // Artillery
    if (.@lv > 40)
        array_push(.@mb, RobinBandit);
    if (.@lv > 50)
        array_push(.@mb, DustGatling);
    if (.@lv > 60)
        array_push(.@mb, DustRifle);
    if (.@lv > 70)
        array_push(.@mb, DustRevolver);
    if (.@lv > 80)
        array_push(.@mb, DustBoss);
    if (.@lv > 90)
        array_push(.@mb, GreatMoubooSlime);
    if (.@lv > 100)
        array_push(.@mb, Jhon);

    // Monsters
    if (is_between(0, .@lv, 30)) {
        array_push(.@mb, Piou);
        array_push(.@mb, Piousse);
        array_push(.@mb, Squirrel);
        array_push(.@mb, ManaPiou);
        array_push(.@mb, ForestPiou);
        array_push(.@mb, RedButterfly);
        array_push(.@mb, Maggot);
        array_push(.@mb, CandorScorpion);
        array_push(.@mb, HouseMaggot);
        array_push(.@mb, LittleYellowSlime);
        array_push(.@mb, Ratto);
        array_push(.@mb, RudolphSlime);
        array_push(.@mb, MoubooSlime);
        array_push(.@mb, Croc);
        array_push(.@mb, Scorpion);
        array_push(.@mb, SmallFrog);
    }
    if (is_between(10, .@lv, 40)) {
        array_push(.@mb, BigFrog);
        array_push(.@mb, Lavern);
        array_push(.@mb, LittleRedSlime);
        array_push(.@mb, ChocolateSlime);
        array_push(.@mb, Blub);
        array_push(.@mb, Duck);
        array_push(.@mb, Bat);
        array_push(.@mb, CaveMaggot);
        array_push(.@mb, ManaGhost);
        array_push(.@mb, ManaBug);
        array_push(.@mb, Fluffy);
        array_push(.@mb, FireGoblin);
        array_push(.@mb, ViciousSquirrel);
        array_push(.@mb, RedScorpion);
        array_push(.@mb, WhiteSlime);
        array_push(.@mb, AzulSlime);
        array_push(.@mb, DesertLogHead);
    }
    if (is_between(20, .@lv, 50)) {
        array_push(.@mb, RedSlime);
        array_push(.@mb, PoisonSpikyMushroom);
        array_push(.@mb, DesertBandit);
        array_push(.@mb, OceanCroc);
        array_push(.@mb, ToppyBlub);
        array_push(.@mb, Sarracenus);
        array_push(.@mb, IceMaggot);
        array_push(.@mb, VampireBat);
        array_push(.@mb, Bandit);
        array_push(.@mb, Pinkie);
        array_push(.@mb, LivingPotato);
        array_push(.@mb, Assassin);
        array_push(.@mb, Skeleton);
    }
    if (is_between(30, .@lv, 60)) {
        array_push(.@mb, CaveSnake);
        array_push(.@mb, GreenSlime);
        array_push(.@mb, CopperSlime);
        array_push(.@mb, YellowSlime);
        array_push(.@mb, SantaSlime);
        array_push(.@mb, LavaSlime);
        array_push(.@mb, Bluepar);
        array_push(.@mb, DeathCat);
        array_push(.@mb, Moggun);
        array_push(.@mb, SeaSlime);
        array_push(.@mb, RedMushroom);
        array_push(.@mb, Mouboo);
        array_push(.@mb, LogHead);
        array_push(.@mb, CandiedSlime);
        array_push(.@mb, OldSnake);
        array_push(.@mb, GrassSnake);
    }
    if (is_between(40, .@lv, 70)) {
        array_push(.@mb, GiantMaggot);
        array_push(.@mb, IcedFluffy);
        array_push(.@mb, Snake);
        array_push(.@mb, BlackSlime);
        array_push(.@mb, Tipiou);
        array_push(.@mb, AlphaMouboo);
        array_push(.@mb, Pollet);
        array_push(.@mb, PiouKnight);
        array_push(.@mb, Shrewboo);
    }
    if (is_between(40, .@lv, 80)) {
        array_push(.@mb, Wolvern);
        array_push(.@mb, FireSkull);
        array_push(.@mb, DarkLizard);
    }
    if (is_between(50, .@lv, 90)) {
        array_push(.@mb, ArmoredSkeleton);
        array_push(.@mb, BlackScorpion);
        array_push(.@mb, ElectroWorm);
        array_push(.@mb, EarthFairy);
        array_push(.@mb, FireFairy);
        array_push(.@mb, WaterFairy);
        array_push(.@mb, WindFairy);
        array_push(.@mb, PoisonFairy);
        array_push(.@mb, MountainSnake);
        array_push(.@mb, HoodedNinja);
        array_push(.@mb, ForestMushroom);
        array_push(.@mb, GoldenScorpion);
    }
    if (is_between(60, .@lv, 100)) {
        array_push(.@mb, Yeti);
        array_push(.@mb, FallenGuard1);
        array_push(.@mb, GreenSlimeMother);
        array_push(.@mb, SnowFlower);
        array_push(.@mb, BlueSlimeMother);
        array_push(.@mb, WickedMushroom);
        array_push(.@mb, CopperSlimeMother);
        array_push(.@mb, YellowSlimeMother);
        array_push(.@mb, RedSlimeMother);
        array_push(.@mb, ChocolateSlimeMother);
        array_push(.@mb, WhiteSlimeMother);
        array_push(.@mb, Archant);
        array_push(.@mb, Scar);
    }
    if (is_between(70, .@lv, 110)) {
        array_push(.@mb, AzulSlimeMother);
        array_push(.@mb, SeaSlimeMother);
        array_push(.@mb, LavaSlimeMother);
        array_push(.@mb, BlackSlimeMother);
        array_push(.@mb, Crafty);
        array_push(.@mb, Forain);
        array_push(.@mb, GreenDragon);
        array_push(.@mb, Michel);
        array_push(.@mb, Troll);
    }
    if (is_between(80, .@lv, 120)) {
        array_push(.@mb, EliteDuck);
        array_push(.@mb, AzulSkullSlime);
        array_push(.@mb, Moonshroom);
        array_push(.@mb, RedSkullSlime);
        array_push(.@mb, Terranite);
        array_push(.@mb, JackO);
        array_push(.@mb, BlackMamba);
        array_push(.@mb, GreenSkullSlime);
        array_push(.@mb, BloodyMouboo);
        array_push(.@mb, Centaur);
        array_push(.@mb, GoboBear);
    }
    if (is_between(90, .@lv, 130)) {
        array_push(.@mb, CopperSkullSlime);
        array_push(.@mb, LavaSkullSlime);
        array_push(.@mb, BlackSkullSlime);
        array_push(.@mb, GiantCaveMaggot);
        array_push(.@mb, TerraniteProtector);
        array_push(.@mb, VanityPixie);
        array_push(.@mb, HolyPixie);
    }
    if (is_between(100, .@lv, 140)) {
        array_push(.@mb, ShadowPixie);
        array_push(.@mb, NulityPixie);
        array_push(.@mb, Reaper);
        array_push(.@mb, NightmareDragon);
        array_push(.@mb, Snail);
        array_push(.@mb, WhirlyBird);
    }
    if (is_between(110, .@lv, 150)) {
        array_push(.@mb, PinkieSuseran);
        array_push(.@mb, Mandragora);
        array_push(.@mb, PinkieMaximus);
    }
    if (.@lv > 120) {
        array_push(.@mb, Junglefowl);
        array_push(.@mb, Tengu);
        array_push(.@mb, Moubi);
    }
    if (.@lv > 130) {
        array_push(.@mb, SuperiorShroom);
        array_push(.@mb, Nutcracker);
        array_push(.@mb, Golem);
    }
    if (.@lv > 140) {
        array_push(.@mb, SiegeTower);
        array_push(.@mb, GreenhornAbomination);
        array_push(.@mb, ShadowTortuga);
        array_push(.@mb, FireElement);
        array_push(.@mb, WaterElement);
        array_push(.@mb, EarthElement);
        array_push(.@mb, WindElement);
    }
    if (.@lv > 150) {
        array_push(.@mb, SacredWisp);
        array_push(.@mb, EvilWisp);
        array_push(.@mb, PanthomWisp);
        array_push(.@mb, EpiphanyWisp);
    }
    if (.@lv > 175)
        array_push(.@mb, Tortuga);

    /* Spawn them and make hostile */
    freeloop(true);
    for (.@i = 0; .@i < 1+($@GROTTO*2/3); .@i++) {
        .@mid = any_of(.@mb);
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), strmobinfo(1, .@mid), .@mid, 1);
        set_aggro(.@m);
    }
    freeloop(false);

    // TODO: Boss Logic if needed
    // One boss each 10 grottos or something?
    if (is_between(10, $@GROTTO, 15)) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", MonsterSergeant, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 15) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", SaxsoGhost, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 20) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", BlueSlime, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 25) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", MurdererScorpion, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 30) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", NightScorpion, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 35) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", SpiderQueen, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 40) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", HoodedAssassin, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 45) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", BanditLord, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 50) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", Tipiu, 1);
        set_aggro(.@m);
        maptimer2("006-5", 10, "Torch#0065::OnLv50");
    }
    if ($@GROTTO > 55) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", FafiDragon, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 60) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", NightDragon, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 65) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", GiantMutatedBat, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 70) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", EvilScythe, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 75) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", AndreiSakar, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 80) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", Yetifly, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 85) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", DemureFirstForm, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 90) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", YetiKing, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 95) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", FallenKing1, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 100) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", PsiConscience, 1);
        set_aggro(.@m);
        maptimer2("006-5", 10, "Torch#0065::OnLv100");
    }
    if ($@GROTTO > 105) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", Isbamuth, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 110) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", TerraniteKing, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 120) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", PinkieEmperor, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 130) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", PanthomLord, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 140) {
        .@m=areamonster("006-5", 20, 20, getmapinfo(MAPINFO_SIZE_X,"006-5"), getmapinfo(MAPINFO_SIZE_Y,"006-5"), "Groata?", MonsterKing, 1);
        set_aggro(.@m);
    }
    if ($@GROTTO > 150)
        maptimer2("006-5", 10, "Torch#0065::OnLv150");
    return;
}

006-5,49,36,0	script	Torch#0065	NPC_NO_SPRITE,{
    if (!mobcount("006-5", "all")) {
        if ($@GROTTO) {
            mesc l("The torch is currently lit.");
            mesc l("Do you want to put it out, or to make it brighter?");
            next;
            select
                l("I have to think."),
                l("Put it out"),
                l("Make it brighter");
            mes "";
            // Put it out
            if (@menu == 2) {
                $@GROTTO = 0;
                $@GROATA = 0;
                enablenpc "#006-5_49_54";
            }
            // I do not want to continue
            if (@menu != 3)
                close;
        } else {
            mesc l("Lit the torch?");
            if (askyesno() == ASK_NO) close;
            disablenpc "#006-5_49_54";
        }
        if (!getq(CandorQuest_Groata)) {
            getitem any(Coal, SulfurPowder, FluoPowder, PileOfAsh, Pearl), 2;
            setq CandorQuest_Groata, 1;
        }
        $@GROTTO += 1;
        // Create monsters based on GROTTO
        $@GROATA = max(BaseLevel, $@GROATA+1);
        GroataGrotto();
        close;
    }
    mesc l("The torch is lit.");
    close;

// Rewards
OnLv50:
    if (getq(CandorQuest_Groata) == 1) {
        dispbottom l("This is ridiculous. Groata is not here, I am wasting my time!");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Uh? Hey, look! A platinum card! This should be worth something with the Guild Merchant Guild, right?");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("...I'll take it.");
        Zeny+=7500;
        Mobpt+=2500;
        getexp 0, 10000;
        setq CandorQuest_Groata, 2;
    }
    end;

OnLv100:
    if (getq(CandorQuest_Groata) == 2) {
        dispbottom l("Groata : Uh. What's happening here?");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : Hey. I live here. Why are you spawning monsters in my home??");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : This is rude. Seriously, stop doing this.");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : Let's make a deal. Take this, clear my house, and get out.");
        getitem Mustache, 1;
        setq CandorQuest_Groata, 3;
    }
    end;

OnLv150:
    if (getq(CandorQuest_Groata) == 3) {
        dispbottom l("Groata : You must be idiotic in keeping spawning.");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : I wonder for how long you can survive here...");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : ...Will you seriously just keep running around? Like a headless chicken?");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : C'mon, that is boring. What I need to do to get you out of my house?");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : ...Meh. I'll give you this... stuff, and for the last time, don't bother me again...");
        sleep2(5000);
        if (getmap() != "006-5") end;
        dispbottom l("Groata : ...Because I am out of freebies and need to sleep. So shoo.");
        getitembound MylarinDust, 1, 1;
        setq CandorQuest_Groata, 4;
    }
    end;

OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

006-5,52,36,0	script	Crystal#0065	NPC_NO_SPRITE,{
    if (!mobcount("006-5", "all")) {
        if ($@GROTTO) {
            mesc l("The crystal looks damaged.");
            mesc l("Do you want to restore it, or to keep breaking it?");
            next;
            select
                l("I have to think."),
                l("Restore it"),
                l("Keep breaking");
            mes "";
            // Restore it
            if (@menu == 2) {
                $@GROTTO = 0;
                $@GROATA = 0;
                enablenpc "#006-5_49_54";
            }
            // I do not want to continue
            if (@menu != 3)
                close;
        } else {
            mesc l("Break the crystal?");
            if (askyesno() == ASK_NO) close;
            disablenpc "#006-5_49_54";
        }
        if (!getq(CandorQuest_Groata)) {
            getitem any(WurtziteOre, DarkCrystal, CandorWarpCrystal), 1;
            setq CandorQuest_Groata, 1;
        }
        $@GROTTO += 5;
        // Create monsters based on GROTTO
        $@GROATA = max(BaseLevel, $@GROATA+1);
        GroataGrotto();
        close;
    }
    mesc l("The crystal has a shard missing.");
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 1;
    end;
}

