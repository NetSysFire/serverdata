// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 014-2: Woodlands Southeast mobs
014-2,150,63,86,43	monster	Log Head	1066,25,30000,20000
014-2,66,45,58,26	monster	Mouboo	1023,6,30000,20000
014-2,108,60,52,58	monster	Forest Mushroom	1060,12,40000,25000
014-2,29,74,19,27	monster	Fluffy	1022,5,30000,20000
014-2,207,65,40,50	monster	Sea Slime	1093,6,42000,20000
014-2,125,84,157,54	monster	Alpha Mouboo	1056,2,120000,120000
014-2,132,59,90,35	monster	Squirrel	1032,28,30000,42000
014-2,143,74,137,117	monster	Cobalt Plant	1136,3,45000,50000
014-2,148,70,137,117	monster	Mauve Plant	1135,3,45000,50000
014-2,154,66,137,117	monster	Gamboge Plant	1134,3,45000,50000
014-2,125,23,125,37	monster	Clover Field	1028,6,45000,50000
