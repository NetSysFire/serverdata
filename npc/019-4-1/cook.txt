// TMW2/LoF scripts.
// Authors:
//    TMW-LoF Team
//    Jesusalva
// Description:
//    Christmas Quest.
//    Originally, it was planned to collect XMAS CAKE, XMAS CANDY CANE and GINGERBREAD BISCUIT
//    and then make a player ranking for sport (like Ched). The ranking function,
//    however, was moved to Chief. Maybe Cook will exchange these for open xmas boxes.

019-4-1,30,29,0	script	Christmas Cook	NPC_GNOME_B,{
    if ($EVENT$ != "Christmas")
        goto L_OutOfSeason;
    goto L_Main;

L_OutOfSeason:
    mesn;
    mesq l("Hmm... I can handle cooking during normal days. Christmas is the problem. I never have enough sweeties by then...");
    close;

L_Main:
    mesn;
    mesq l("Ah... Santa's helpers sure eat a lot. I'm sure gift delivery is hard for them.");
    next;
    mesn;
    mesq l("Well, you know... Maybe we can strike a deal. I have good relations with the stock manager. I'll give you an event item.");
    next;

L_Loop:
    mesc l("@@/10 @@ for 1 @@", countitem(GingerBreadMan),  getitemlink(GingerBreadMan),getitemlink(OpenedChristmasBox));
    mesc l("@@/9  @@ for 1 @@", countitem(XmasCandyCane),   getitemlink(XmasCandyCane), getitemlink(OpenedChristmasBox));
    mesc l("@@/8  @@ for 1 @@", countitem(XmasCake),        getitemlink(XmasCake),      getitemlink(OpenedChristmasBox));
    mes "";
    select
        l("Uhm, I don't need that."),
        rif(countitem(GingerBreadMan)>=10,  l("Trade the Gingerbread")),
        rif(countitem(XmasCandyCane)>=9,    l("Trade the Xmas Candy Cane")),
        rif(countitem(XmasCake)>=8,         l("Trade the Xmas Cake")),
        l("Maybe later.");
    mes "";
    switch (@menu) {
        case 2:
            delitem GingerBreadMan, 10;
            getexp getiteminfo(GingerBreadMan, ITEMINFO_SELLPRICE)*10, 10;
            getitem OpenedChristmasBox, 1;
            mesn;
            mesq l("Deal. Here you go.");
            next;
            goto L_Loop; break;
        case 3:
            delitem XmasCandyCane, 9;
            getexp getiteminfo(XmasCandyCane, ITEMINFO_SELLPRICE)*10, 9;
            getitem OpenedChristmasBox, 1;
            mesn;
            mesq l("Deal. Here you go.");
            next;
            goto L_Loop; break;
        case 4:
            delitem XmasCake, 8;
            getexp getiteminfo(XmasCake, ITEMINFO_SELLPRICE)*10, 8;
            getitem OpenedChristmasBox, 1;
            mesn;
            mesq l("Deal. Here you go.");
            next;
            goto L_Loop; break;
    }
    close;

OnInit:
    .sex=G_MALE;
    .distance=5;
    end;
}

