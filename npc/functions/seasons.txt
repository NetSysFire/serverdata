// TMW2 Script.
// Authors:
//    Jesusalva
// Description:
//    Season functions

// Function authored by Reid and edited by Jesusalva
// season({day, month})
// SQuest_Summer
//    returns the current season (approximation)
//      WINTER:      Winter,    21/12
//      SPRING:      Spring,    20/03
//      SUMMER:      Summer,    21/06
//      AUTUMN:      Autumn,    22/09

function	script	season	{
    .@current_month = getarg(1, gettime(GETTIME_MONTH));

    if (.@current_month % 3 == 0) {
        .@current_day = getarg(0, gettime(GETTIME_DAYOFMONTH));

        switch (.@current_month) {
            case MARCH:     .@season_day = 20; break;
            case JUNE:      .@season_day = 21; break;
            case SEPTEMBER: .@season_day = 22; break;
            case DECEMBER:  .@season_day = 21; break;
            default: break;
        }

        .@is_after_season_day = .@current_day >= .@season_day ? 0 : -1;
    }

    return (.@current_month / 3 + .@is_after_season_day) % 4;
}


// Event seasons
// Christmas cannot be on GlobalEventMenu because it affects seasons system
function	script	sChristmas	{
    // Determine the drop rates based on month, and Christmas proximity
    if (gettime(GETTIME_MONTH) == DECEMBER) {
        if (gettime(GETTIME_DAYOFMONTH) <= 26)
            .@m=10;
        else
            .@m=8;
    } else {
        .@m=5;
    }

    // Add Christmas drops
    addmonsterdrop(Moggun,              XmasCake,            80*.@m);
    addmonsterdrop(AlphaMouboo,         XmasCake,            92*.@m);
    addmonsterdrop(BlueSlime,           XmasCake,           100*.@m);
    addmonsterdrop(SantaSlime,          XmasCake,           120*.@m);
    addmonsterdrop(Pollet,              XmasCake,           130*.@m);
    addmonsterdrop(IcedFluffy,          XmasCake,           150*.@m);
    addmonsterdrop(Yeti,                XmasCake,           500*.@m);

    addmonsterdrop(Bandit,              XmasCandyCane,       30*.@m);
    addmonsterdrop(Mouboo,              XmasCandyCane,       48*.@m);
    addmonsterdrop(WhiteSlime,          XmasCandyCane,       50*.@m);
    addmonsterdrop(RudolphSlime,        XmasCandyCane,      100*.@m);
    addmonsterdrop(Fluffy,              XmasCandyCane,      200*.@m);
    addmonsterdrop(AzulSlime,           XmasCandyCane,      200*.@m);

    addmonsterdrop(Duck,                GingerBreadMan,      36*.@m);
    addmonsterdrop(WaterFairy,          GingerBreadMan,     100*.@m);

    // Event drop rates, multiplied by 10 during Christmas (see .@m)
    addmonsterdrop(Yeti,                ClosedChristmasBox, 350*.@m);
    addmonsterdrop(WaterFairy,          ClosedChristmasBox, 108*.@m);
    addmonsterdrop(Pollet,              ClosedChristmasBox,  97*.@m);
    addmonsterdrop(AlphaMouboo,         ClosedChristmasBox,  83*.@m);
    addmonsterdrop(IcedFluffy,          ClosedChristmasBox,  67*.@m);
    addmonsterdrop(BlueSlime,           ClosedChristmasBox,  42*.@m);
    addmonsterdrop(Moggun,              ClosedChristmasBox,  40*.@m);
    addmonsterdrop(SantaSlime,          ClosedChristmasBox,  36*.@m);
    addmonsterdrop(AzulSlime,           ClosedChristmasBox,  20*.@m);
    addmonsterdrop(Fluffy,              ClosedChristmasBox,  20*.@m);
    addmonsterdrop(RudolphSlime,        ClosedChristmasBox,   8*.@m);
    addmonsterdrop(WhiteSlime,          ClosedChristmasBox,   3*.@m);
    addmonsterdrop(GiantMaggot,         ClosedChristmasBox,   2*.@m);

    // This is not dropped outside December
    if (gettime(GETTIME_MONTH) == DECEMBER) {
        // Bugfix
        if (gettime(GETTIME_YEAR) == 2018)
            .@m+=10;
    addmonsterdrop(WaterFairy,          XmasGift,            6*.@m);
    addmonsterdrop(Pollet,              XmasGift,            5*.@m);
    addmonsterdrop(AlphaMouboo,         XmasGift,            5*.@m);
    addmonsterdrop(IcedFluffy,          XmasGift,            4*.@m);
    addmonsterdrop(SantaSlime,          XmasGift,            3*.@m);
    addmonsterdrop(Fluffy,              XmasGift,            2*.@m);
    addmonsterdrop(AzulSlime,           XmasGift,            2*.@m);
    }

    // Change maps for Christmas Season (Specially LoF maps)
    addmapmask "003-1", MASK_CHRISTMAS;
    addmapmask "005-1", MASK_CHRISTMAS;
    addmapmask "009-1", MASK_CHRISTMAS;
    addmapmask "012-1", MASK_CHRISTMAS;
    addmapmask "017-2", MASK_CHRISTMAS;
    addmapmask "017-2-1", MASK_CHRISTMAS;
    addmapmask "017-3", MASK_CHRISTMAS;
    addmapmask "020-2", MASK_CHRISTMAS;

    // Enable event
    set $EVENT$, "Christmas";
    //logmes "Enabled CHRISTMAS event.", LOGMES_ATCOMMAND;
    return;
}
// Valentine Day is handled by @event, this is only for @reloadmobdb
function	script	sValentine	{
    // Add Valentine drops
    addmonsterdrop(WhirlyBird,          LoveLetter, 10000);
    addmonsterdrop(RedMushroom,         LoveLetter,     8);
    addmonsterdrop(ChocolateSlime,      LoveLetter,     4);
    return;
}

function	script	sEaster	{
        // Enable event
        set $EVENT$, "Easter";
        addmonsterdrop(Forain, DarkEggshellHat, 2);
        if (playerattached())
            logmes "Enabled EASTER event.", LOGMES_ATCOMMAND;
        return;
}

// This allows GMs to change seasons if needed
function	script	SeasonControl	{
    do
    {
        select
            "Summer Start",
            "Summer End",
            "Autumn Start",
            "Autumn End",
            "Winter Start",
            "Winter End",
            "Spring Start",
            "Spring End",
            "SPECIAL - Christmas",
            "SPECIAL - Valentine",
            "SPECIAL - Easter",
            "FORCED DAYLIGHT",
            "FORCED NIGHTTIME",
            "Abort";

        switch (@menu) {
            case 1: donpcevent("#SeasonCore::OnSummerStart"); break;
            case 2: donpcevent("#SeasonCore::OnSummerEnd"); break;
            case 3: donpcevent("#SeasonCore::OnAutumnStart"); break;
            case 4: donpcevent("#SeasonCore::OnAutumnEnd"); break;
            case 5: donpcevent("#SeasonCore::OnWinterStart"); break;
            case 6: donpcevent("#SeasonCore::OnWinterEnd"); break;
            case 7: donpcevent("#SeasonCore::OnSpringStart"); break;
            case 8: donpcevent("#SeasonCore::OnSpringEnd"); break;
            case 9: sChristmas(); break;
            case 10: sValentine(); break;
            case 11: sEaster(); break;
            case 12: $@WEATHER_NIGHT=false; doevent("#WeatherCore::OnMinute45"); break;
            case 13: $@WEATHER_NIGHT=true; doevent("#WeatherCore::OnMinute45"); break;
        }
    } while (@menu != 13);
    return;
}

// If skip_checks is set, it'll ignore $@SEASON control.
// SeasonReload( {skip_checks} )
function	script	SeasonReload	{
    // Proccess skip_checks
    if (getarg(0,0))
        $@SEASON=99;

    // Summer extra drops
    if (season() == SUMMER && $@SEASON != SUMMER) {
        donpcevent("#SeasonCore::OnSummerStart");
    }
    // Summer end delete drops
    if (season() == AUTUMN && $@SEASON == SUMMER) {
        donpcevent("#SeasonCore::OnSummerEnd");
    }
    // Autumn extra drops
    if (season() == AUTUMN && $@SEASON != AUTUMN) {
        donpcevent("#SeasonCore::OnAutumnStart");
    }
    // Autumn end delete drops
    if (season() == WINTER && $@SEASON == AUTUMN) {
        donpcevent("#SeasonCore::OnAutumnEnd");
    }
    // Winter extra drops
    if (season() == WINTER && $@SEASON != WINTER) {
        donpcevent("#SeasonCore::OnWinterStart");
    }
    // Winter end delete drops
    if (season() == SPRING && $@SEASON == WINTER) {
        donpcevent("#SeasonCore::OnWinterEnd");
    }
    // Spring extra drops
    if (season() == SPRING && $@SEASON != SPRING) {
        donpcevent("#SeasonCore::OnSpringStart");
    }
    // Spring end delete drops
    if (season() == SUMMER && $@SEASON == SPRING) {
        donpcevent("#SeasonCore::OnSpringEnd");
    }

    // Non-season, but season-related
    // Christmas have a special feature
    if ($EVENT$ == "Christmas")
        sChristmas();
    if ($EVENT$ == "Valentine")
        sValentine();

    $@SEASON=season();
    initnpctimer("#SeasonCore");
    return;
}

000-0,0,0,0	script	#SeasonCore	NPC_HIDDEN,{
    end;

OnSummerStart:
    addmonsterdrop(SaxsoGhost,          CherryCocktail,      450);
    addmonsterdrop(DesertBandit,        CherryCocktail,      410); // INVALID
    addmonsterdrop(Duck,                CherryCocktail,      360);
    addmonsterdrop(Croc,                CherryCocktail,      180);
    addmonsterdrop(RedButterfly,        CherryCocktail,      100);

    addmonsterdrop(Centaur,             CactusCocktail,     1000);
    addmonsterdrop(GiantMaggot,         CactusCocktail,      290); // INVALID
    addmonsterdrop(FireGoblin,          CactusCocktail,      220); // INVALID
    addmonsterdrop(DesertMaggot,        CactusCocktail,      190);
    addmonsterdrop(Scorpion,            CactusCocktail,      165);
    addmonsterdrop(Maggot,              CactusCocktail,      140);

    addmonsterdrop(AlphaMouboo,         AppleCocktail,       850);
    addmonsterdrop(OceanCroc,           AppleCocktail,       480);
    addmonsterdrop(Mouboo,              AppleCocktail,       280);
    addmonsterdrop(RedScorpion,         AppleCocktail,       120);
    addmonsterdrop(Pinkie,              AppleCocktail,        70);


    addmonsterdrop(Duck,                Sunglasses,            1);
    addmonsterdrop(Croc,                Sunglasses,            1);
    addmonsterdrop(SaxsoGhost,          Sunglasses,            1);
    addmonsterdrop(DesertMaggot,        Sunglasses,            1); // INVALID
    addmonsterdrop(Scorpion,            Sunglasses,            1);
    addmonsterdrop(GiantMaggot,         Sunglasses,            1); // INVALID
    addmonsterdrop(Centaur,             Sunglasses,            3);
    addmonsterdrop(AlphaMouboo,         Sunglasses,            1);
    addmonsterdrop(OceanCroc,           Sunglasses,            1);
    addmonsterdrop(Mouboo,              Sunglasses,            1);
    addmonsterdrop(Pinkie,              Sunglasses,            1);
    addmonsterdrop(Moonshroom,          Sunglasses,            2);
    addmonsterdrop(RedButterfly,        Sunglasses,            1);
    end;

OnSummerEnd:
    delmonsterdrop(Duck,                CherryCocktail);
    delmonsterdrop(Croc,                CherryCocktail);
    delmonsterdrop(SaxsoGhost,          CherryCocktail);
    delmonsterdrop(RedButterfly,        CherryCocktail);
    delmonsterdrop(DesertBandit,        CherryCocktail);
    delmonsterdrop(Maggot,              CactusCocktail);
    delmonsterdrop(DesertMaggot,        CactusCocktail);
    delmonsterdrop(Scorpion,            CactusCocktail);
    delmonsterdrop(GiantMaggot,         CactusCocktail);
    delmonsterdrop(Centaur,             CactusCocktail);
    delmonsterdrop(FireGoblin,          CactusCocktail);
    delmonsterdrop(AlphaMouboo,         AppleCocktail);
    delmonsterdrop(OceanCroc,           AppleCocktail);
    delmonsterdrop(Mouboo,              AppleCocktail);
    delmonsterdrop(Pinkie,              AppleCocktail);
    delmonsterdrop(RedScorpion,         AppleCocktail);
    delmonsterdrop(Duck,                Sunglasses);
    delmonsterdrop(Croc,                Sunglasses);
    delmonsterdrop(SaxsoGhost,          Sunglasses);
    delmonsterdrop(DesertMaggot,        Sunglasses);
    delmonsterdrop(Scorpion,            Sunglasses);
    delmonsterdrop(GiantMaggot,         Sunglasses);
    delmonsterdrop(Centaur,             Sunglasses);
    delmonsterdrop(AlphaMouboo,         Sunglasses);
    delmonsterdrop(OceanCroc,           Sunglasses);
    delmonsterdrop(Mouboo,              Sunglasses);
    delmonsterdrop(Pinkie,              Sunglasses);
    delmonsterdrop(Moonshroom,          Sunglasses);
    delmonsterdrop(RedButterfly,        Sunglasses);
    end;

OnAutumnStart:
    // Fancy trees
    addmapmask "012-1", MASK_AUTUMN;

    // Autumn's Drop
    addmonsterdrop(FafiDragon,          PumpkandySeed,     10000);
    addmonsterdrop(BlackMamba,          PumpkandySeed,      8000);
    addmonsterdrop(AlphaMouboo,         PumpkandySeed,      5000);
    addmonsterdrop(PoisonSpikyMushroom, PumpkandySeed,      3000);
    addmonsterdrop(Mouboo,              PumpkandySeed,      2000);
    addmonsterdrop(Bandit,              PumpkandySeed,      1600);
    addmonsterdrop(Fluffy,              PumpkandySeed,      1500);
    addmonsterdrop(LogHead,             PumpkandySeed,      1400);
    addmonsterdrop(CaveSnake,           PumpkandySeed,      1380);
    addmonsterdrop(CaveMaggot,          PumpkandySeed,       850);
    addmonsterdrop(GreenSlime,          PumpkandySeed,       750);
    addmonsterdrop(Piou,                PumpkandySeed,       600);
    addmonsterdrop(Squirrel,            PumpkandySeed,       500);
    end;

OnAutumnEnd:
    // Ched's rewards can't be claimed anymore. Delete that from all players.
    DelQuestFromEveryPlayer(SQuest_Ched);

    removemapmask "012-1", MASK_AUTUMN;
    delmonsterdrop(FafiDragon,          PumpkandySeed);
    delmonsterdrop(Mouboo,              PumpkandySeed);
    delmonsterdrop(AlphaMouboo,         PumpkandySeed);
    delmonsterdrop(Fluffy,              PumpkandySeed);
    delmonsterdrop(Piou,                PumpkandySeed);
    delmonsterdrop(CaveSnake,           PumpkandySeed);
    delmonsterdrop(CaveMaggot,          PumpkandySeed);
    delmonsterdrop(Bandit,              PumpkandySeed);
    delmonsterdrop(Squirrel,            PumpkandySeed);
    delmonsterdrop(PoisonSpikyMushroom, PumpkandySeed);
    delmonsterdrop(LogHead,             PumpkandySeed);
    delmonsterdrop(GreenSlime,          PumpkandySeed);
    delmonsterdrop(BlackMamba,          PumpkandySeed);
    end;

OnWinterStart:
    // Nearly all Winterlands + Woodlands + ducks drop winter items. (Summer is Desert shining, Winter is the opposite)
    // Winterland Area Mobs
    // Moggun AlphaMouboo BlueSlime SantaSlime IcedFluffy Yeti WaterFairy AzulSlime Fluffy
    // RudolphSlime WhiteSlime AngryBat Wolvern WindFairy

    // Woodlands Area Mobs
    // Tipiu Piousse Silkworm Squirrel Blub CobaltPlant MauvePlant GambogePlant AlizarinPlant
    // Loghead Mouboo ForestMushroom SeaSlime Centaur Pinkie CloverField PoisonSpikyMushroom
    // ChagashroomField PlushroomField ManaPiou Bluepar LivingPotato RedMushroom RedButterfly
    // ManaBug TrainingDummy

    // LoF Area Mobs
    // ChocolateSlime Lavern ShadowPlant CyanButterfly

    // Items: Snowflake CaramelCandy GingerBreadMan ChocolateBiscuit
    // Quest Requires All Of The Above

    addmonsterdrop(SantaSlime,          ChocolateBiscuit,    100);

    addmonsterdrop(WaterFairy,          GingerBreadMan,     1000);
    addmonsterdrop(RedMushroom,         GingerBreadMan,      300);
    addmonsterdrop(Duck,                GingerBreadMan,      280);
    addmonsterdrop(ManaPiou,            GingerBreadMan,      240);
    addmonsterdrop(AngryBat,            GingerBreadMan,      140);
    addmonsterdrop(AzulSlime,           GingerBreadMan,      110);
    addmonsterdrop(Lavern,              GingerBreadMan,       90);

    addmonsterdrop(Tipiu,               CaramelCandy,       8000);
    addmonsterdrop(WindFairy,           CaramelCandy,       2000);
    addmonsterdrop(IcedFluffy,          CaramelCandy,       1200);
    addmonsterdrop(Wolvern,             CaramelCandy,       1000);
    addmonsterdrop(LivingPotato,        CaramelCandy,        800);
    addmonsterdrop(ChocolateSlime,      CaramelCandy,        400);
    addmonsterdrop(SeaSlime,            CaramelCandy,        400);
    addmonsterdrop(Pinkie,              CaramelCandy,        200);
    addmonsterdrop(Fluffy,              CaramelCandy,        150);
    addmonsterdrop(SlimeBlast,          CaramelCandy,        100);
    addmonsterdrop(SilkWorm,            CaramelCandy,         30);

    addmonsterdrop(ForestMushroom,      Snowflake,          3000);
    addmonsterdrop(Blub,                Snowflake,          3000);
    addmonsterdrop(LogHead,             Snowflake,           600);
    addmonsterdrop(RedButterfly,        Snowflake,           400);
    addmonsterdrop(CyanButterfly,       Snowflake,           400);
    addmonsterdrop(Dummy,               Snowflake,           300);
    addmonsterdrop(ManaBug,             Snowflake,           200);
    addmonsterdrop(Piousse,             Snowflake,           100);
    addmonsterdrop(Squirrel,            Snowflake,           100);
    addmonsterdrop(CloverPatch,         Snowflake,           100);
    addmonsterdrop(CobaltPlant,         Snowflake,            90);
    addmonsterdrop(GambogePlant,        Snowflake,            90);
    addmonsterdrop(MauvePlant,          Snowflake,            90);
    addmonsterdrop(AlizarinPlant,       Snowflake,            90);


    addmonsterdrop(AlphaMouboo,         ReinbooWand,           3);
    addmonsterdrop(BloodyMouboo,        ReinbooWand,           2);
    addmonsterdrop(Centaur,             ReinbooWand,           2);
    addmonsterdrop(Mouboo,              ReinbooWand,           1);
    addmonsterdrop(EasterMouboo,        ReinbooWand,           1);
    addmonsterdrop(MoubooSlime,         ReinbooWand,           1);
    end;

OnWinterEnd:
    // Hasan's rewards can't be claimed anymore. Delete that from all players.
    DelQuestFromEveryPlayer(SQuest_Autumn);

    delmonsterdrop(WaterFairy,          GingerBreadMan);
    delmonsterdrop(RedMushroom,         GingerBreadMan);
    delmonsterdrop(Duck,                GingerBreadMan);
    delmonsterdrop(ManaPiou,            GingerBreadMan);
    delmonsterdrop(AngryBat,            GingerBreadMan);
    delmonsterdrop(AzulSlime,           GingerBreadMan);
    delmonsterdrop(Lavern,              GingerBreadMan);
    delmonsterdrop(Tipiu,               CaramelCandy);
    delmonsterdrop(WindFairy,           CaramelCandy);
    delmonsterdrop(IcedFluffy,          CaramelCandy);
    delmonsterdrop(Wolvern,             CaramelCandy);
    delmonsterdrop(LivingPotato,        CaramelCandy);
    delmonsterdrop(ChocolateSlime,      CaramelCandy);
    delmonsterdrop(SeaSlime,            CaramelCandy);
    delmonsterdrop(Pinkie,              CaramelCandy);
    delmonsterdrop(SlimeBlast,          CaramelCandy);
    delmonsterdrop(SilkWorm,            CaramelCandy);
    delmonsterdrop(ForestMushroom,      Snowflake);
    delmonsterdrop(Blub,                Snowflake);
    delmonsterdrop(LogHead,             Snowflake);
    delmonsterdrop(RedButterfly,        Snowflake);
    delmonsterdrop(CyanButterfly,       Snowflake);
    delmonsterdrop(Dummy,               Snowflake);
    delmonsterdrop(ManaBug,             Snowflake);
    delmonsterdrop(Piousse,             Snowflake);
    delmonsterdrop(Squirrel,            Snowflake);
    delmonsterdrop(CloverPatch,         Snowflake);
    delmonsterdrop(CobaltPlant,         Snowflake);
    delmonsterdrop(GambogePlant,        Snowflake);
    delmonsterdrop(MauvePlant,          Snowflake);
    delmonsterdrop(AlizarinPlant,       Snowflake);

    delmonsterdrop(AlphaMouboo,         ReinbooWand);
    delmonsterdrop(BloodyMouboo,        ReinbooWand);
    delmonsterdrop(Centaur,             ReinbooWand);
    delmonsterdrop(Mouboo,              ReinbooWand);
    delmonsterdrop(EasterMouboo,        ReinbooWand);
    delmonsterdrop(MoubooSlime,         ReinbooWand);
    end;

OnSpringStart:
    // All Woodlands drop spring items.

    // Items: Tulip Rose Blueberries (+ GrassSeeds and AlizarinHerb )
    // Quest Requires All Of The Above (+herbs which are common drop)
    // All mobs in same group drops the same thing

    // Boos (GrassSeeds)
    addmonsterdrop(Centaur,             GrassSeeds,         2000);
    addmonsterdrop(AlphaMouboo,         GrassSeeds,          800);
    addmonsterdrop(Mouboo,              GrassSeeds,          300);
    addmonsterdrop(MoubooSlime,         GrassSeeds,          100);

    // Shrooms (AlizarinHerb)
    addmonsterdrop(WickedMushroom,      AlizarinHerb,       9000);
    addmonsterdrop(ForestMushroom,      AlizarinHerb,       6000);
    addmonsterdrop(RedMushroom,         AlizarinHerb,       2000);
    addmonsterdrop(PoisonSpikyMushroom, AlizarinHerb,       1000);

    // Pious (AlizarinHerb)
    addmonsterdrop(Tipiu,               AlizarinHerb,       8000);
    addmonsterdrop(ManaPiou,            AlizarinHerb,        740);
    addmonsterdrop(Piousse,             AlizarinHerb,        400);

    // Mystical & Fairies (Tulip)
    addmonsterdrop(GreenDragon,         Tulip,              3000);
    addmonsterdrop(WindFairy,           Tulip,              1000);
    addmonsterdrop(PoisonFairy,         Tulip,              1000);
    addmonsterdrop(WaterFairy,          Tulip,              1000);
    addmonsterdrop(EarthFairy,          Tulip,              1000);

    // Slimes (AlizarinHerb)
    addmonsterdrop(BlackSlime,          AlizarinHerb,        220);
    addmonsterdrop(SeaSlime,            AlizarinHerb,        210);
    addmonsterdrop(RedSlime,            AlizarinHerb,        200);
    addmonsterdrop(ChocolateSlime,      AlizarinHerb,         50);

    // Snakes (Rose)
    addmonsterdrop(BlackMamba,          Rose,               1800);
    addmonsterdrop(MountainSnake,       Rose,               1500);
    addmonsterdrop(GrassSnake,          Rose,                700);

    // Butterflies (Blueberries)
    addmonsterdrop(CyanButterfly,       Blueberries,         400);
    addmonsterdrop(RedButterfly,        Blueberries,         400);
    addmonsterdrop(ManaBug,             Blueberries,         200);

    // Scorpions (Blueberries)
    addmonsterdrop(GoldenScorpion,      Blueberries,        4000);
    addmonsterdrop(NightScorpion,       Blueberries,        3000);

    // Underground mobs (Rose)
    addmonsterdrop(Troll,               Rose,               3000);
    addmonsterdrop(Lavern,              Rose,                900);
    addmonsterdrop(DarkLizard,          Rose,                400);
    addmonsterdrop(AngryBat,            Rose,                100);
    addmonsterdrop(SilkWorm,            Rose,                 30);

    // Standard Greenary (Tulip)
    addmonsterdrop(LogHead,             Tulip,               600);
    addmonsterdrop(LivingPotato,        Tulip,               400);
    addmonsterdrop(Pinkie,              Tulip,               200);
    addmonsterdrop(Squirrel,            Tulip,               100);

    // Special mobs and drop
    // 1- No mob dropping spring-exclusive (Tulip/Rose/Blueberries)
    // 2- Preference to mobs you wouldn't otherwise bother
    // 3- Keep a certain level threshold for rarity
    addmonsterdrop(Centaur,             BrimmedFlowerHat,      4);
    addmonsterdrop(Tipiu,               BrimmedFlowerHat,      4);
    addmonsterdrop(AlphaMouboo,         BrimmedFlowerHat,      2);
    addmonsterdrop(BlackSlime,          BrimmedFlowerHat,      1);
    addmonsterdrop(SeaSlime,            BrimmedFlowerHat,      1);
    addmonsterdrop(RedSlime,            BrimmedFlowerHat,      1);
    addmonsterdrop(MoubooSlime,         BrimmedFlowerHat,      1);
    addmonsterdrop(ManaPiou,            BrimmedFlowerHat,      1);

    end;

OnSpringEnd:
    delmonsterdrop(Centaur,             GrassSeeds);
    delmonsterdrop(AlphaMouboo,         GrassSeeds);
    delmonsterdrop(Mouboo,              GrassSeeds);
    delmonsterdrop(MoubooSlime,         GrassSeeds);
    delmonsterdrop(WickedMushroom,      AlizarinHerb);
    delmonsterdrop(ForestMushroom,      AlizarinHerb);
    delmonsterdrop(RedMushroom,         AlizarinHerb);
    delmonsterdrop(PoisonSpikyMushroom, AlizarinHerb);
    delmonsterdrop(Tipiu,               AlizarinHerb);
    delmonsterdrop(ManaPiou,            AlizarinHerb);
    delmonsterdrop(Piousse,             AlizarinHerb);
    delmonsterdrop(BlackSlime,          AlizarinHerb);
    delmonsterdrop(SeaSlime,            AlizarinHerb);
    delmonsterdrop(RedSlime,            AlizarinHerb);
    delmonsterdrop(ChocolateSlime,      AlizarinHerb);
    delmonsterdrop(GreenDragon,         Tulip);
    delmonsterdrop(WindFairy,           Tulip);
    delmonsterdrop(PoisonFairy,         Tulip);
    delmonsterdrop(WaterFairy,          Tulip);
    delmonsterdrop(EarthFairy,          Tulip);
    delmonsterdrop(LogHead,             Tulip);
    delmonsterdrop(LivingPotato,        Tulip);
    delmonsterdrop(Pinkie,              Tulip);
    delmonsterdrop(Squirrel,            Tulip);
    delmonsterdrop(CyanButterfly,       Blueberries);
    delmonsterdrop(RedButterfly,        Blueberries);
    delmonsterdrop(ManaBug,             Blueberries);
    delmonsterdrop(GoldenScorpion,      Blueberries);
    delmonsterdrop(NightScorpion,       Blueberries);
    delmonsterdrop(BlackMamba,          Rose);
    delmonsterdrop(MountainSnake,       Rose);
    delmonsterdrop(GrassSnake,          Rose);
    delmonsterdrop(Troll,               Rose);
    delmonsterdrop(Lavern,              Rose);
    delmonsterdrop(DarkLizard,          Rose);
    delmonsterdrop(AngryBat,            Rose);
    delmonsterdrop(SilkWorm,            Rose);
    delmonsterdrop(Centaur,             BrimmedFlowerHat);
    delmonsterdrop(Tipiu,               BrimmedFlowerHat);
    delmonsterdrop(AlphaMouboo,         BrimmedFlowerHat);
    delmonsterdrop(BlackSlime,          BrimmedFlowerHat);
    delmonsterdrop(SeaSlime,            BrimmedFlowerHat);
    delmonsterdrop(RedSlime,            BrimmedFlowerHat);
    delmonsterdrop(MoubooSlime,         BrimmedFlowerHat);
    delmonsterdrop(ManaPiou,            BrimmedFlowerHat);
    end;

OnInit:
    SeasonReload(1);
    end;

OnHour00:
    if ($@SEASON != season()) {
        SeasonReload();
    }
    end;

OnTimer700:
    charcommand("@refreshall");
    stopnpctimer;
    end;
}
