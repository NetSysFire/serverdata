// TMW2 scripts.
// Authors:
//    Jesusalva
// Description:
//    The most powerful sword ever. It's alive.

boss,45,42,0	script	Lightbringer	NPC_LIGHTBRINGER,{
    function movelb;
    function matrixlb;
    function notAvailable;

    if ($GAME_STORYLINE < 3) notAvailable();
    if (islegendary()) notAvailable();
    if ($LIGHT_HOLDER$ != "") notAvailable();

    .@q=getq(General_Narrator);
    // Prologue missed - straight refusal
    if (.@q < 19) {
        if (strcharinfo(0) != $MOST_HEROIC$)
            npctalk3 l("The sword glows too much. Perhaps @@ could take it.", $MOST_HEROIC$);
        else
            npctalk3 l("I'm not going to touch a FLOATING sword made of unobtainum without at least knowing a bit more about it.");
        end;
    }
    // Determine if you are worthy (from 0 to 101)
    if (!@lbscore)
        @lbscore=matrixlb();

    if (is_staff())
        dispbottom "I only did "+@lbscore+" points...";

    if (@lbscore <= 30) {
        npctalk3 l("...");
        end;
    } else if (@lbscore <= 60) {
        npctalk3 l("You are not worthy...");
        end;
    }

    // Else: score > 60, 2 minutes per additional score
    // If you are above 100: +2 minutes per score
    // If you are above 110: +4 minutes per score
    if (@lbscore > 100)
        @lbscore*=3;
    if (@lbscore > 110*3)
        @lbscore*=2;

    // Basic wield time: 2 minutes per point
    .@btime=120;
    if ($GAME_STORYLINE == 4)
        .@btime+=30; // +30 seconds for stage 4
    else if ($GAME_STORYLINE >= 5)
        .@btime+=60; // +60 seconds for stage 5 (but it was not yet claimed? o.o)

    // Each siege gives +1 second per point
    if ($GAME_STORYLINE < 5)
        .@btime+=$MK_TEMPVAR;

    if (@lbscore > 100 && $GAME_STORYLINE > 3 && $LIGHT_HOLDER$ == "" && strcharinfo(0) == $MOST_HEROIC$ && !islegendary())
        goto L_Perma;

    rentitem Lightbringer, (.@btime*(@lbscore-60));
    dispbottom l("This live sword drafts itself to your hand. You can wield it for a while.");
    disablenpc .name$;
    .busy=gettimetick(2)+(.@btime*(@lbscore-60))+300;
    end;

function notAvailable {
    npctalkonce l("It still isn't time to awake the King Of Holy Swords, Light Bringer.");
    end;
    return;
}

L_Perma:
    mesn l("Lightbringer"), 2;
    mesc l("%s, you are pathetically weak.", strcharinfo(0)), 2;
    next;
    mesn l("Lightbringer"), 2;
    mesc l("However, the fate of this world is at your hands. I shall not allow the Moubootaur to cause havoc."), 2;
    next;
    mesn l("Lightbringer"), 2;
    mesc l("Therefore, just for a short while, I offer to be your blade."), 2;
    next;
    mesn l("Lightbringer"), 2;
    mesc l("What do you say?"), 2;
    select
        l("It'll be my pleasure."),
        l("I am a noob and don't need you.");

    mes "";
    if (islegendary()) close;
    if ($LIGHT_HOLDER$ != "") close;
    if (@menu != 1) close;
    inventoryplace Lightbringer, 1;
    mesn l("The Mana Source"), 2;
    mesc l("%s, you did your best to protect this world inhabitants. It is my wish that you continue protecting this world. Therefore, I bestow upon you, the legendary %s. Please use its powers to protect your friend and the world peace!", strcharinfo(0), getitemlink(Lightbringer)), 2;
    mesc l("WARNING: The %s is a %s. Besides being insanely powerful, no duplicate of them exist in the world. They can be tweaked freely and can hold multiple cards as well, and scale according to your level. Use its powers wisely. However, beware: This weapon cannot be traded except with \"@grantpower\" command, and if you abandon the world, the weapon will abandon you as well!", getitemlink(Lightbringer), b(l("legendary weapon"))), 1;

    // Destroy the previous Lightbringer
    DelItemFromEveryPlayer(Lightbringer);
    getitembound Lightbringer, 1, 1; // Account bound or char bound? (1 or 4)
    dispbottom l("You received the @@ from @@.", getitemlink(Lightbringer), l("The Mana Source"));
    $LIGHT_HOLDER$=strcharinfo(0);
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 2;

    if ($GAME_STORYLINE < 3)
        disablenpc .name$;

    .npcId=getnpcid();
    .users=getusers(1);
    .busy=false;
    // Constants

    // We should jump straight to loop (it runs every 5 minutes)
OnTimer300000:
    .users=getusers(1);
    // If $LIGHT_HOLDER$ is set, we die here
    if ($LIGHT_HOLDER$ != "")
        end;

    // Proccess busy time
    if (.busy) {
        if (.busy < gettimetick(2)) {
            .busy=false;
            enablenpc .name$;
        } else {
            initnpctimer;
            end;
        }
    }

    // Min. 2 PCs for the Lightbringer to show up
    if (!.busy) {
        if (.users <= 2 && !$@GM_OVERRIDE) {
            disablenpc .name$;
            .busy=true;
        }
    }

    // You cannot interact with it during events
    if ($@MK_SCENE || $@GM_EVENT) {
        disablenpc .name$;
        .busy=true;
        initnpctimer;
        end;
    }

    // Move Lightbringer to a random map
    movelb(.npcId);

    // Debug markers
    if ($@GM_OVERRIDE)
        debugmes "Light Bringer (bot): "+.map$+" ("+.x+", "+.y+")";

    // We're done, restart loop timer
    initnpctimer;
    end;

// Functions
// movelb(npcid)
function movelb {
    .@id=getarg(0);
    setarray .@m$,  "001-1", "001-3", "001-7",
                    "004-2", "004-2-1",
                    "010-1", "010-1-1", "011-2", "011-3",
                    "013-1", "014-5", "015-1", "015-2", "015-3", "015-4", "015-5", "015-6", "015-7",
                    "017-0", "018-1", "018-1-1", "018-2", "018-3", "018-4", "018-4-1", "018-5-1", "018-7",
                    "019-4", "021-1", "021-2", "021-3", "022-1", "023-1",
                    "029-1", "029-7", "029-8";
    .mp$=any_of(.@m$);

    // Try to warp randomly: 30 attempts for a walkable spot
    .@e=0; .@x=0; .@y=0;
    .@mx=getmapinfo(MAPINFO_SIZE_X, .mp$)-20;
    .@my=getmapinfo(MAPINFO_SIZE_Y, .mp$)-20;
    do {
        if (.@e >= 30) {
            .mp$="boss";
            .@x=45;
            .@y=42;
            break;
        }
        .@x = rand2(20, .@mx);
        .@y = rand2(20, .@my);
        .@e+=1;
    } while (!checknpccell(.mp$, .@x, .@y, cell_chkpass));

    if (!checknpccell(.mp$, .@x, .@y, cell_chkpass)) {
        Exception("gm.bot runtime error: GM_ERR_128 highlight @Jesusalva", RB_DEBUGMES|RB_IRCBROADCAST); .mp$="boss"; .@x=45; .@y=42;
    }

    // Time to warp the NPC
    npctalk("...");
    sleep(20);
    unitwarp(.@id, .mp$, .@x, .@y);
    sleep(50); // For some reason or other, adding sleep(norid) and sleep2(rid).
    return;
}

// matrixlb()
function matrixlb {
    .@pts=0;
    // Heroic Matrice
    .@pts+=($MOST_HEROIC$ == strcharinfo(0) ? 10 : 0);
    // Magic Matrice
    .@pts+=min(7, MAGIC_LVL);
    // Main levels matrix
    .@pts+=min(15, BaseLevel/10);
    .@pts+=min(15, JobLevel/10);
    // Scoreboards matrix
    .@br=array_find($@hoblvl_name$, strcharinfo(0));
    .@jr=array_find($@hojlvl_name$, strcharinfo(0));
    .@mr=array_find($@hofortune_name$, strcharinfo(0));
    .@br=limit(0, (.@br >= 0 ? 10-.@br : 0), 10);
    .@jr=limit(0, (.@jr >= 0 ? 10-.@jr : 0), 10);
    .@mr=limit(0, (.@mr >= 0 ? 10-.@mr : 0), 10);
    .@pts+=.@br;
    .@pts+=.@jr;
    .@pts+=.@mr;
    // Reputation matrix
    .@pts+=min(3, reputation("Tulim")/33);
    .@pts+=min(3, reputation("Hurns")/33);
    .@pts+=min(3, reputation("LoF")/33);
    .@pts+=min(3, reputation("Nival")/33);
    .@pts+=min(3, reputation("Halin")/33);
    .@pts+=min(3, reputation("Frostia")/33);
    .@pts+=min(3, reputation("Candor")/33);
    .@pts+=min(3, reputation("Fortress")/33);
    // What about Lilit?

    // Temporaly I'll use Candor Battle Top 3
    .@cr=array_find($@cfefe_name$, strcharinfo(0));
    .@cr=limit(0, (.@cr >= 0 ? 3-.@cr : 0), 3);
    .@pts+=.@cr;

    // What about Doppelganger TOP 3?
    .@dr=array_find($@udt_name$, strcharinfo(0));
    .@dr=limit(0, (.@dr >= 0 ? 3-.@dr : 0), 3);
    .@pts+=.@dr;

    // Rebirth (2pts per level)
    .@pts+=limit(0, REBIRTH*2, 10);

    // World Story Bonus
    if ($GAME_STORYLINE > 3 && $MK_TEMPVAR >= 12)
        .@pts+=1;
    if ($FIRESOFSTEAM >= 10)
        .@pts+=1;
    if ($GAME_STORYLINE >=5)
        .@pts+=1; // Should never, ever, happen

    // Max points: 101→104→114→117→120
    return .@pts;
}

}

