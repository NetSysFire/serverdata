// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  Guild System Utils

// Usage: getguildrole (guildid, accid, {type})
// Returns a string if TYPE is set. Otherwise, Returns the GPOS_*
// On failure, returns "null" or -1
function	script	getguildrole	{
    .@gid=getarg(0);
    .@aid=getarg(1);
    .@type=getarg(2,0);
    .@pos=-1;
    getguildmember(.@gid, 2);
    for (.@i=0; .@i < $@guildmembercount; .@i++) {
        //debugmes "Found AID %d - Position %d", $@guildmemberaid[.@i], $@guildmemberpos[.@i];
        if (.@aid == $@guildmemberaid[.@i]) {
            .@pos=$@guildmemberpos[.@i];
            break;
        }
    }
    if (!.@type)
        return .@pos;

    // Wait, we now have a command for this
    return getguildpostitle(.@gid, .@pos);

    // Legacy behavior
    switch (.@pos) {
    case GPOS_GUILDMASTER:
        return "Guild Master";
    case GPOS_VICELEADER:
        return "Vice Leader";
    case GPOS_RECRUITER:
        return "Guild Recruiter";
    case GPOS_TREASURER:
        return "Guild Treasurer";
    case GPOS_MEMBER:
        return "Member";
    case GPOS_NEWBIE:
        return "Newbie";
    default:
        return "null";
    }
}
