// TMW2 Script
// Authors:
//    Jesusalva
// Description:
//    Link portals to soul menhirs like the teleporters from old
//    The price is temporary. This feature got in because no ship in Nivalis Port
//    PS. Anise => “Aisen” Anagram

// TeleporterGate(TP_CURRENT)
function	script	TeleporterGate	{
    .@TP=getarg(0);

    // Validate
    .@x=array_find($@LOCMASTER_TP, .@TP);
    if (.@x < 0)
        return Exception("Invalid Teleport Point: "+.@TP);

    // Obtain current Teleport Point (.@IF)
    .@x=array_find($@LOCMASTER_MAP$, getmap());
    .@IF=$@LOCMASTER_TP[.@x];

    if (!(TELEPORTERS & .@TP)) {
        TELEPORTERS=TELEPORTERS|.@TP;
        mesn "Anise Inc.";
        mesc l("Location Registered. You are now capable to use this warp gate.");
        next;
    }
    mesc l("Where should I warp to?");
    mesc l("Cost: 1 @@", getitemlink(PileOfAsh)), 1;
    if (!countitem(PileOfAsh))
        close;
    next;
    menuint
        rif(TELEPORTERS & TP_FROST && .@IF != TP_FROST, l("Frostia")), TP_FROST,
        rif(TELEPORTERS & TP_HALIN && .@IF != TP_HALIN, l("Halinarzo")), TP_HALIN,
        rif(TELEPORTERS & TP_LILIT && .@IF != TP_LILIT, l("Lilit")), TP_LILIT,
        rif(TELEPORTERS & TP_TULIM && .@IF != TP_TULIM, l("Tulimshar")), TP_TULIM,
        rif(TELEPORTERS & TP_HURNS && .@IF != TP_HURNS, l("Hurnscald")), TP_HURNS,
        rif(TELEPORTERS & TP_NIVAL && .@IF != TP_NIVAL, l("Nivalis")), TP_NIVAL,
        rif(TELEPORTERS & TP_LOF   && .@IF != TP_LOF,   l("Land Of Fire")), TP_LOF,
        rif(TELEPORTERS & TP_FORT  && .@IF != TP_FORT,  l("Fortress Island")), TP_FORT,
        rif(TELEPORTERS & TP_BOSSR && .@IF != TP_BOSSR, l("Moubootaur Maze")), TP_BOOSR,
        l("None"), -1;
    mes "";

    // Proccess menu
    if (@menuret != -1)
        delitem PileOfAsh, 1;
    else
        close;

    // Retrieve location specifics and warp
    .@x=array_find($@LOCMASTER_TP, @menuret);
    closeclientdialog;
    warp $@LOCMASTER_MAP$[.@x], $@LOCMASTER_X[.@x], $@LOCMASTER_Y[.@x];
    return;
}

