// Evol functions.
// Author:
//    4144
//    Jesusalva
// Description:
//    Shops utility functions
// Variables:
//    Item ID, Item Price, Item Amount

// restoreshopitem(amount) or restoreshopitem(price, amount)
function	script	restoreshopitem	{
    if (getarg(2, -1337) == -1337) {
        .@amount=getarg(1);
        .@price=-1;
    } else {
        .@amount=getarg(2);
        .@price=getarg(1);
    }

    if (shopcount(getarg(0)) < .@amount)
        sellitem getarg(0), .@price, .@amount;
    return;
}


// shop2({NPC Name, {showmenu=true}})
function	script	shop2	{
    npctalk3 any(
        l("The best wares!"),
        l("Buy cheap, sell expensive!")
    );
    .@n$=getarg(0, strnpcinfo(0));

    npcshopattach(.@n$);

    if (getarg(1,true))
        shop .@n$;
    else
        openshop .@n$;

    closeclientdialog;
    return;
}
