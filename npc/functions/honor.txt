// TMW2 Script
// Author:
//  Jesusalva
// Description:
//  PvP Honor Rank system

// Returns if a map is on PVP Mode or Not
// ispvpmap( {mapid} )
function	script	ispvpmap	{
    .@mapa$=getarg(0, getmapname());
    return (getmapflag(.@mapa$, mf_pvp) || getmapflag(.@mapa$, mf_pvp_noparty) || getmapflag(.@mapa$, mf_pvpnoguild));
}

// Numerical representation of player strength
// get_BR( getcharid(3) )
function	script	get_BR	{
    .@oid=getcharid(3);
    .@rid=getarg(0, .@oid);
    .@br=0;

    // attachrid() and detachrid()
    // readbattleparam(.@rid)
    // Or rather: battleparam()
    if (attachrid(.@rid)) {
        // 1 BR per allocated status point
        .@br+=battleparam(UDT_STR);
        .@br+=battleparam(UDT_AGI);
        .@br+=battleparam(UDT_VIT);
        .@br+=battleparam(UDT_DEX);
        .@br+=battleparam(UDT_INT);
        .@br+=battleparam(UDT_LUK);

        // 6 BR per level
        .@br+=BaseLevel*6;

        // 1 BR for 5 DMG points (average)
        .@br+=(battleparam(UDT_ATKMIN)+battleparam(UDT_ATKMAX))/5;
        .@br+=battleparam(UDT_MATKMAX)/5;

        // 8 BR for each attack range
        .@br+=battleparam(UDT_ATKRANGE)*8;

        // 1 BR for 10 DEF points
        .@br+=battleparam(UDT_DEF)/10;
        .@br+=battleparam(UDT_MDEF)/10;
    } else {
        Exception("GET_BR INVALID RID "+.@rid, RB_DEBUGMES|RB_IRCBROADCAST);
    }

    // Restore
    detachrid();
    if (!attachrid(.@oid))
        Exception("::FATAL :: GET_BR INVALID OID "+.@oid, RB_DEBUGMES|RB_IRCBROADCAST|RB_ISFATAL);

    return .@br;
}

// Calculate the Honor Points which are due
// calc_HR( get_BR(getcharid(3)), get_BR(killedrid), log=True )
function	script	calc_HR	{
    //.@atk_br=get_BR(getarg(0));
    //.@def_br=get_BR(getarg(1));
    .@atk=readparam(BaseLevel, getarg(0));
    .@def=readparam(BaseLevel, getarg(1));
    .@log=getarg(2, true);

    // Calculate how much levels you've used above needed
    .@overpower=.@atk-.@def;

    // Dishonorable: You used 15 levels above target, or target < level 30
    if (.@overpower > 15 || .@def < 30) {
        .@honor=-(limit(1, .@overpower-15, 12));
    } else {
        .@honor=limit(3, -(.@overpower)+15, 20);
    }

    // Log cooldown (30 minutes)
    // If cooldown already in effect, do not gain honor.
    if (.@log) {
        if (.@honor > 0 && PVP_COOLDOWN[.@def] >= gettimetick(2))
            .@honor=0;
        PVP_COOLDOWN[.@def]=gettimetick(2)+PVP_WAITTIME;
    }
    return .@honor;
}

// getvariableofpc(HONOR, .@rid, 0) < 0 → determine if other player is bandit
// is_bandit( account id )
function	script	is_bandit	{
    .@oid=getcharid(3);
    .@rid=getarg(0, .@oid);

    return getvariableofpc(HONOR, .@rid, 0) < 0;
}

