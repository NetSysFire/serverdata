// TMW2/LOF Script.
// Author:
//    Jesusalva
// Description:
//  Well connected to Terranite Cave. This is the water Nivalis townsfolk uses.

019-1,110,101,0	script	Well#Nivalis	NPC_NO_SPRITE,{
    // Begin here
    .@q=getq(NivalisQuest_Well);
    mesn l("The Self-Serving Ice Well!");
    mesc l("Hello, my name is Mahid, and this well belongs to me!");
    mesc l("You are allowed to fill your bottles, but BE SURE TO PAY!");
    mesc l("Otherwise, ##BYOU'LL DIE.##b Have a nice day!");
    next;
    if (.@q == 1) {
        mesn l("???");
        mesq l("Hey, is somebody over there?");
        mes "";
    }

    menu
        l("Fill Water Bottles"), L_Bottle,
        rif(.@q == 2, l("Jump inside!")), L_Reckless,
        rif(.@q == 1, l("Steal bucket!")), L_Bucket,
        rif(.@q != 1, l("Throw something inside!")), L_Throw,
        l("Leave."), -;
    close;

// Jump to Terranite Cave (requires 55 vitality minimum)
L_Reckless:
    closedialog;
    warp "015-6", 363, 109;
    dispbottom l("Ouch! That was kinda reckless!");
    percentheal -150+readparam2(bVit), 0;
    close;

// Easter Egg
L_Bucket:
    mes "";
    percentheal -rand(10,20), 0;
    mesn strcharinfo(0);
    mesq l("Ouch, the bucket BITE me!");
    close;

// Main Quest
L_Throw:
        mes "##B" + l("Drag and drop an item from your inventory.") + "##b";

        .@id = requestitem();

        // If ID is invalid, there's not enough items, it is bound = Cannot bury
        if (.@id < 1) {
            mesc l("You give up.");
            close;
        }
        if (countitem(.@id) < 1 || checkbound(.@id) || !getiteminfo(.@id, ITEMINFO_MAXCHANCE)) {
            if (checkbound(.@id))
                mesc l("You cannot drop this item!");
            else if (!getiteminfo(.@id, ITEMINFO_MAXCHANCE))
                mesc l("This item is too precious, you cannot part with it!");
            else
                mesc l("You give up.");
            close;
        }
        // Delete item and spawn it at Terranite Cave (an Angry Yellow Slime might steal it)
        delitem .@id, 1;
        makeitem .@id, 1, "015-6", any(362,363), any(110, 112, 114);
        // Now we check if quest must start
        if (!.@q)
            goto L_Quest;
        // If not, report if it is safe to jump
        if (readparam2(bVit) < 55)
            mesc l("The item impact suggests you don't have enough vitality to jump inside."), 1;
        else if (readparam2(bVit) < 75)
            mesc l("The item impact suggests jumping inside will leave you badly wounded.");
        else
            mesc l("The item impact suggests jumping inside should be safe if you have enough life.");
    close;

// Quest Node
L_Quest:
    mesn l("???");
    if (getiteminfo(.@id, ITEMINFO_WEIGHT) > 1000)
        mesq l("Ouch! That's heavy!");
    else if (getiteminfo(.@id, ITEMINFO_SELLPRICE) < 10)
        mesq l("What cheap crap is this? It's not worth even 10 GP.");
    else if (getiteminfo(.@id, ITEMINFO_TYPE) == IT_HEALING)
        mesq l("Mhm, this looks healthy.");
    else if (getiteminfo(.@id, ITEMINFO_TYPE) == IT_AMMO)
        mesq l("Ammo? I prefer power gloves! That is useless for me!");
    else
        mesq l("Who is throwing stuff at me?!");
    next;
    select
        l("Who are you?"),
        l("How did you get down there?"),
        l("Do you need help?"),
        menuaction(l("Leave."));
    mes "";
    mesn;
    switch (@menu) {
        case 1: mesq l("I'll talk about who I am after leaving the well."); break;
        case 2: mesq l("I don't remember. I guess somebody threw me here!"); break;
        case 3: mesq l("I certainly can't get out on my own."); break;
        default: mesq l("..Anyone there?"); close; break;
    }
    next;
    do {
        select
            l("I'll call someone to aid you."),
            l("Is it too deep?"),
            l("Couldn't you climb the rope?"),
            menuaction(l("Leave."));
        mes "";
        mesn;
        switch (@menu) {
            case 1: mesq l("Please do, my friend."); setq NivalisQuest_Well, 1; break;
            case 2: mesq l("It's over a hundred meters in depth. There is some land here, but I'm afraid of Terranite."); break;
            case 3: mesq l("I'm not crazy, the bucket is vicious and the rope won't withstand my weight."); break;
            default: mesq l("..Anyone there?"); close; break;
        }
    } while (@menu != 1);
    close;

// Fill a water bottle
L_Bottle:
    mes "";
    mesc l("Cost: @@ gp per bottle.", .price);
    input .@count;

    if (!.@count)
        close;

    .@gp = .@count * .price;

    if (Zeny < .@gp) {
        mesc l("Not enough money.");
        close;
    }

    if (countitem(EmptyBottle) < .@count) {
        mesc l("Not enough bottles.");
        close;
    }

    inventoryplace IcedBottle, .@count, BottleOfWoodlandWater, .@count;
    delitem EmptyBottle, .@count;

    // Calculate how many iced bottles you'll get
    .@iced=0;
    for (.@i=0; .@i < .@count; .@i++) {
        if (rand2(500) < 33)
            .@iced++;
    }

    // Apply the results and have a happy day!
    Zeny-=.@gp;
    if (.@iced)
        getitem IcedBottle, .@iced;
    getitem BottleOfWoodlandWater, .@count-.@iced;
    close;

OnInit:
    .sex = G_OTHER;
    .distance = 3;

    .price=60;
    end;
}
