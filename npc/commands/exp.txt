// TMW2 Script
// Author: Jesusalva

// @getexp atcommand
// Gets experience
//
// group lv: 5
// group char lv: 99
// log: True

-	script	@getexp	32767,{
    end;

OnCall:
    .@delta$ = .@atcmd_parameters$[0];
    .@d = atoi(.@delta$);

    getexp .@d, 0;
    end;

OnHomun:
    if (!gethominfo(0)) {
        dispbottom l("No homunculus found!");
        end;
    }
    .@delta$ = .@atcmd_parameters$[0];
    .@d = atoi(.@delta$);

    gethomunexp .@d;
    end;

OnInit:
    bindatcmd "getexp", "@getexp::OnCall", 5, 99, 1;
    bindatcmd "gethexp", "@getexp::OnHomun", 5, 99, 1;
}
