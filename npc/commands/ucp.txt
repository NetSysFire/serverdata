// TMW2 Script
// Author:
//    Jesusalva

function	script	UserCtrlPanel	{
    do
    {
        @unsaved=false;
        clear;
        setnpcdialogtitle l("User Control Panel");
        mes l("This menu gives you some options which affect your account.");
        mes l("In some cases, your pincode will be required.");
        mes "";
        mes l("What do you want to access?");
        next;
        if (@unsaved) {
            mesc l("Careful: You have unsaved changes!"), 1;
            mes "";
        }
        select
            l("Rules"),
            l("Game News"),
            rif(#FIRST_TIME == 1, l("Create PIN Number")),
            l("Account Information"),
            rif(getcharid(2) > 0, l("Guild Information")),
            l("Change Language"),
            rif(getskilllv(TMW2_CRAFT), l("Change Crafting Options")),
            l("Game Settings"),
            l("Save & Exit");

        switch (@menu)
        {
            case 1: GameRules(); break;
            case 2: GameNews(); break;
            case 3:
                // Account age check
                if (gettimetick(2) < #REG_DATE+86400*15) {
                    mesc l("Your account is too young."), 1;
                    mesc l("The accounts need 15 days to set Pin Codes."), 1;
                    next;
                    break;
                }
                mes l(".:: Create PIN Code ::.");
                mes l("If you decide to continue, a random PINCODE will be");
                mes l("sent to the email you used to register on Moubootaur Legends.");
                mes "";
                mes l("With a PinCode, you'll have access to restricted features,");
                mes l("Like Discord integration and sensitive options.");
                mes "";
                mes l("You can change the PIN from ManaPlus char selection screen.");
                mes l("You can also modify your email with %s.", b("@email"));
                mes l("This will do nothing if the account already have a PIN.");
                next;
                mesc l("Are you sure you want to create a PIN now?"), 1;
                if (askyesno() == ASK_NO)
                    break;
                mes "";
                .@msg$=json_encode("date", gettimetick(2),
                                   "accid", getcharid(3),
                                   "pin", rand2(10000));
                debugmes .@msg$;
                api_send(API_PINCODE, .@msg$);
                #FIRST_TIME=2;
                mesc l("PinCode created, an email should arrive within 15 minutes."), 3;
                next;
                break;
            case 4:
                if (!validatepin())
                    break;
                if (!@lgc || @query) {
                    query_sql("SELECT email,logincount,last_ip FROM `login` WHERE account_id="+getcharid(3)+" LIMIT 1", .@email$, .@lgc, .@ip$);
                    @email$=.@email$;
                    @lgc=.@lgc;
                    @ip$=.@ip$;
                } else {
                    .@email$=@email$;
                    .@lgc=@lgc;
                    .@ip$=@ip$;
                }
                mes l("Char Name: @@", strcharinfo(0));
                mes l("Party Name: @@", strcharinfo(1));
                mes l("Guild Name: @@", strcharinfo(2));
                mes l("Clan Name: @@", strcharinfo(4));
                mes "";
                mes l("Email: @@", .@email$[0]);
                if (Sex)
                    mes l("Male");
                else
                    mes l("Female");
                mes l("Last IP: @@", .@ip$[0]);
                mes l("Total Logins: @@", .@lgc[0]);
                mes l("Registed %s ago", FuzzyTime(#REG_DATE));
                next;
                if (@query)
                    break;
                @query=1;
                query_sql("SELECT name,last_login,last_map,partner_id FROM `char` WHERE account_id="+getcharid(3)+" LIMIT 9", .@name$, .@lastlogin$, .@map$, .@married);
                for (.@i = 1; .@i <= getarraysize(.@name$); .@i++) {
                mesn .@name$[.@i-1];
                mes l("Last Seen: @@", FuzzyTime(.@lastlogin$[.@i-1]));
                mes l("Last map: @@", .@map$[.@i-1]);
                if (.@married[.@i-1])
                    mes l("Married with @@", gf_charname(.@married[.@i-1]));
                mes "";
                }
                next;
                break;
            case 5:
                .@gid=getcharid(2);
                mesc (".:: "+getguildname(.@gid)+" ::."), 1;
                mesc l("Guild Master: @@", getguildmaster(.@gid)), 3;
                if (getguildnxp(.@gid) > 0)
                    mesc l("Guild Lv @@, @@/@@ EXP to level up", getguildlvl(.@gid), fnum(getguildexp(.@gid)), fnum(getguildnxp(.@gid)));
                else
                    mesc l("Guild Lv @@, @@/@@ EXP to level up", fnum(getguildlvl(.@gid)), getguildexp(.@gid), "???");

                mes "";
                mesc l("Average player level: @@", getguildavg(.@gid));
                mesc l("Your position on the guild: @@", getguildrole(.@gid, getcharid(3), true));
                next;
                break;
            case 6: asklanguage(LANG_IN_SHIP); break;
            case 7:
              // Draw the GUI and any info on it
              csysGUI_Report();
              mesc l("NOTE: The effective bonus level applied is the average level of enabled options!");
              //mesc l("Mobpt: @@", Mobpt);
              do {
                .@opt$="Do nothing";
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_BASE);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_ATK);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_DEF);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_ACC);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_EVD);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_REGEN);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_SPEED);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_DOUBLE);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_MAXPC);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_SCRESIST);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_SCINFLICT);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_MANAUSE);
                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_BOSSATK);

                .@opt$+=":"+csysGUI_OptToogleMenu(CRGROUP_FINAL);

                select (.@opt$);
                mes "";
                switch (@menu) {
                case 2: csysGUI_ChangeOpt(CRGROUP_BASE); break;

                case 3: csysGUI_ChangeOpt(CRGROUP_ATK); break;
                case 4: csysGUI_ChangeOpt(CRGROUP_DEF); break;
                case 5: csysGUI_ChangeOpt(CRGROUP_ACC); break;
                case 6: csysGUI_ChangeOpt(CRGROUP_EVD); break;

                case 7: csysGUI_ChangeOpt(CRGROUP_REGEN); break;
                case 8: csysGUI_ChangeOpt(CRGROUP_SPEED); break;
                case 9: csysGUI_ChangeOpt(CRGROUP_DOUBLE); break;
                case 10: csysGUI_ChangeOpt(CRGROUP_MAXPC); break;

                case 11: csysGUI_ChangeOpt(CRGROUP_SCRESIST); break;
                case 12: csysGUI_ChangeOpt(CRGROUP_SCINFLICT); break;
                case 13: csysGUI_ChangeOpt(CRGROUP_MANAUSE); break;
                case 14: csysGUI_ChangeOpt(CRGROUP_BOSSATK); break;

                case 15: csysGUI_ChangeOpt(CRGROUP_FINAL); break;
                }
              } while (@menu > 1);
                break;
            case 8:
              do
              {
                mesc ".:: " + l("GAME SETTINGS") + " ::.", 3;

                // GSET_SOULMENHIR_MANUAL
                // Enables/Disable manual position saving on Soul Menhir
                if (GSET_SOULMENHIR_MANUAL)
                    mes l("Soul Menhir automatic saving: ") + col(l("Disabled"), 1);
                else
                    mes l("Soul Menhir automatic saving: ") + col(l("Enabled"), 2);


                // GSET_DAILYREWARD_SILENT
                // Enables/Disable silent dialog for daily rewards
                // (otherwise a image will be shown)
                if (GSET_DAILYREWARD_SILENT)
                    mes l("Display daily reward screen: ") + col(l("Disabled"), 1);
                else
                    mes l("Display daily reward screen: ") + col(l("Enabled"), 2);


                // GSET_LONGMENU_DENSITY
                // How many nexts should be queued in a density list
                // (use higher values if you have few recipes or can scroll)
                if (!GSET_LONGMENU_DENSITY)
                    mes l("Long Text Wall Density: ") + col(l("Normal"), 1);
                else
                    mes l("Long Text Wall Density: ") + col(l("Compact")+": "+GSET_LONGMENU_DENSITY, 2);


                // GSET_FIXED_ALCHEMY
                // Alchemy Table Behavior
                if (GSET_FIXED_ALCHEMY)
                    mes l("Alchemy Table: ") + col(l("Never ask: Brew %d", GSET_FIXED_ALCHEMY), 1);
                else
                    mes l("Alchemy Table: ") + col(l("Ask everytime"), 2);


                // GSET_ALCOHOL_NOOVERDRINK
                // Should players be allowed to drink themselves to death?
                if (GSET_ALCOHOL_NOOVERDRINK)
                    mes l("Lethal overdrinking: ") + col(l("Not allowed"), 1);
                else
                    mes l("Lethal overdrinking: ") + col(l("Allowed"), 2);


                // GSET_CRAFT_BOUND
                // Should players make bound or named items?
                if (GSET_CRAFT_BOUND)
                    mes l("Crafting method: ") + col(l("Account Bound"), 1);
                else
                    mes l("Crafting method: ") + col(l("Named Items"), 2);


                // TUTORIAL
                // Should we show players tutorial info?
                if (!TUTORIAL)
                    mes l("Tutorial Protips: ") + col(l("Disabled"), 1);
                else
                    mes l("Tutorial Protips: ") + col(l("Enabled"), 2);


                if ($EVENT$ == "Valentine") {
                    // GSET_VALENTINE_EATONE
                    // Eat all Chocolate Boxes from Valentine Day event
                    if (!GSET_VALENTINE_EATALL)
                        mes l("[Valentine] Eat all chocolate: ") + col(l("Not allowed"), 2);
                    else
                        mes l("[Valentine] Eat all chocolate: ") + col(l("Allowed"), 1);
                }


                if (strcharinfo(2) == "Monster King") {
                    // GSET_AUTORECEIVE_COINS
                    // Enables/Disable autoreceive strange coins
                    if (!GSET_AUTORECEIVE_COINS)
                        mes l("Autoreceive Strange Coins: ") + col(l("Disabled"), 1);
                    else
                        mes l("Autoreceive Strange Coins: ") + col(l("Enabled"), 2);
                }

                if (@unsaved) {
                    mes "";
                    mesc l("Careful: You have unsaved changes!"), 1;
                }

                mes "";
                select
                    l("Return to User Control Panel"),
                    l("Toggle Soul Menhir automatic saving"),
                    l("Toggle Daily Reward screen"),
                    l("Text Wall Density"),
                    l("Alchemy Table brewing"),
                    l("Lethal alcohol overdrinking"),
                    l("Change crafting method"),
                    l("Show Tutorial Protips"),
                    rif($EVENT$ == "Valentine", ("Valentine Eating")),
                    rif(strcharinfo(2) == "Monster King", ("Toggle Autoreceive Event Coins"));
                mes "";

                switch (@menu) {
                    case 1:
                        // Update savepoint if needed
                        if (@unsaved) {
                            if (!GSET_SOULMENHIR_MANUAL) savepoint "000-1", 22, 22;
                            else ResaveRespawn();
                        }
                        break;
                    case 2:
                        GSET_SOULMENHIR_MANUAL=!GSET_SOULMENHIR_MANUAL;
                        @unsaved=true;
                        break;
                    case 3:
                        GSET_DAILYREWARD_SILENT=!GSET_DAILYREWARD_SILENT; break;
                    case 4:
                        if (GSET_LONGMENU_DENSITY >= 5)
							GSET_LONGMENU_DENSITY=0;
						else
							GSET_LONGMENU_DENSITY+=1;
						break;
                    case 5:
                        if (GSET_FIXED_ALCHEMY) {
							GSET_FIXED_ALCHEMY=0;
						} else {
                            .@max=(is_sponsor() ? 25 : 10);
                            mesc l("How many to brew? (%d-%d)", 0, .@max);
                            input(GSET_FIXED_ALCHEMY, 0, .@max);
							GSET_FIXED_ALCHEMY=limit(0, GSET_FIXED_ALCHEMY, 10);
                        }
						break;
                    case 6:
                        GSET_ALCOHOL_NOOVERDRINK=!GSET_ALCOHOL_NOOVERDRINK; break;
                    case 7:
                        GSET_CRAFT_BOUND=!GSET_CRAFT_BOUND; break;
                    case 8:
                        TUTORIAL=!TUTORIAL; break;
                    case 9:
                        GSET_VALENTINE_EATALL=!GSET_VALENTINE_EATALL; break;
                    case 10:
                        GSET_AUTORECEIVE_COINS=!GSET_AUTORECEIVE_COINS; break;
                }
                clear;
              } while (@menu != 1);
            break;
            default: close; break;
        }
    } while (1);
}



-	script	@ucp	32767,{
    end;

OnCall:

    UserCtrlPanel;
    closedialog;
    end;

OnInit:
    bindatcmd "ucp", "@ucp::OnCall", 0, 99, 0;
}
