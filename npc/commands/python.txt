// The Mana World script
// Author: Gumi <gumi@themanaworld.org>
// Author: Jesusalva <jesusalva@themanaworld.org>
//
// Stomp stomp stomp (use with caution)

-	script	@python	32767,{
    end;

OnCall:
    specialeffect(34, AREA, playerattached());
    .@zone$=getmapinfo(MAPINFO_ZONE, .@mapa$);
    if (.@zone$ == "MMO")
        end;
    sc_start SC_CASH_DEATHPENALTY, 1000, 1;
    addtimer 380, .name$+"::OnKill";
    end;

OnKill:
    percentheal -100, -100;
    //dispbottom l("Oh look, it is Cupid!");
    end;

OnInit:
    bindatcmd "python", "@python::OnCall", 60, 60, 1;
    end;
}
