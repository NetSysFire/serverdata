// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 001-12: Southeast Enchanted Forest mobs
001-12,99,98,23,23	monster	Clover Field	1028,2,90000,120000
001-12,90,88,70,68	monster	Lovely Fluffy	1050,80,22000,1000
001-12,22,89,6,69	monster	Duck	1029,4,60000,120000
001-12,87,148,71,10	monster	Duck	1029,4,60000,120000
001-12,96,79,64,60	monster	Red Mushroom	1042,20,175000,50000
001-12,102,108,4,3	monster	Chocolate Slime	1180,2,220000,50000
001-12,80,139,85,28	monster	Whirly Bird (BOSS)	1232,1,36000000,50000
001-12,115,40,36,13	monster	Angry Bat	1194,6,260000,50000
001-12,94,93,4,3	monster	Mana Piou	1155,1,220000,50000
001-12,37,149,1,1	monster	Mana Piou	1155,3,220000,50000
