// TMW2 Script

// (Random) Treasure Chest
//  Authored by Jesusalva
// Regenerates every 6 hours

010-1-1,0,0,0	script	#chest_010110	NPC_CHEST,{

   if (!.busy && !.empty) {
        TreasureBox();

        specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
        .dir = .dir == 0 ? 2 : 6; // closed ? opening : closing
        .busy = true; // lock until available again
        initnpctimer;
    } else if (!.busy) {
        mesc l("Someone looted this treasure box already...");
    } else {
        end;
    }
    close;

OnTimer160:
    .dir = .dir == 6 ? 0 : 4; // closing ? closed : open
    end;

OnTimer500:
    .busy = false; // unlock
    if (.dir == 0 || .dir == 4)
        stopnpctimer; // stop here if the chest is closed
    end;

OnInit:
    .busy = false;
    .distance = 2;
    .empty = false;

OnClock0156:
OnClock0756:
OnClock1356:
OnClock1956:
    // Try to warp randomly to a walkable spot, up to 20 attempts
    // Otherwise, it'll stay where it already is (but will close and refill).
    .@e=0; .@x=0; .@y=0;
   while (!checkcell(.map$, .@x, .@y, cell_chkpass))
    {
        if (.@e == 20) {
            .@x=.x;
            .@y=.y;
            break;
        }
        // Remember the +20 -20 margin adjustment
        .@x = rand(20, 180);
        .@y = rand(20, 120);
        ++.@e;
    }
    .busy=false;
    .empty=false;
    movenpc .name$, .@x, .@y, 0;
    end;
}
