// TMW2 scripts.
// Author:
//    Saulc
//    Jesusalva
// Variables:
//    CandorQuest_Tolchi
// Description:
//   Tolchi crafts weapons at Rosen & Tolchi shop
// Values:
//    0    BlackSmith quest ask for 1 iron oore
//    1    BlackSmith quest ask for 3 coal
//    2    BlackSmith quest ask for 1 iron ingot
//    3    Quest is Complete


005-4,42,37,0	script	Tolchi	NPC_RAIJIN_FEMALE_LEGION_ARTIS,{
    function quest_close;
    function quest_giveitem;
    function quest_first;
    function quest_second;
    function quest_third;
    function tolchi_arrows;
    function tolchi_tweaks;

    /////////////////////////////////////////////////////////////////////////////
    @q = getq(CandorQuest_Tolchi);

    if (@q == 3) {
        mesn;
        mesq l("Hey! How are you today? Thanks again for your help.");
    } else if (BaseLevel < 5 || BaseLevel < 10 && @q == 1 || BaseLevel < 15 && @q == 2) {
        mesn;
        mesq l("I need help, but you aren't strong enough. Please come back later.");
        next;
    } else {
        mesn;
        mesq l("Hello! You seem strong enough, could take a request from me? Of course, not for free.");
        next;
    }

    do
    {
        select
            l("I'm interested in your arrows, they're too expensive with Rosen."),
            rif(@q == 0 && BaseLevel >= 5, l("Yes. What do you need help with?")),
            rif(@q == 1 && BaseLevel >= 10, l("Yes. What do you need help with?")),
            rif(@q == 2 && BaseLevel >= 15, l("Yes. What do you need help with?")),
            l("Do you accept special requests?"),
            l("I have other things to do at the moment.");
        mes "";
        switch (@menu)
        {
            case 1:
                tolchi_arrows();
                break;
            case 2:
                quest_first();
                break;
            case 3:
                quest_second();
                break;
            case 4:
                quest_third();
                break;
            case 5:
                tolchi_tweaks();
                break;
            default:
                quest_close();
                break;
        }
    } while (@menu != 6);

    closedialog;
    goodbye;
    close;

// Close and talk about quest
function quest_close {
    if (@q < 3) {
        mesn;
        mesq l("Maybe next time, then.");
        next;
    }
    return;
}

// Item delivery core
function quest_giveitem {
@q = getq(CandorQuest_Tolchi);
    if (@q == 2)
    {
        if (countitem(IronIngot) == 0)
        {
            mesn;
            mesq l("You don't have the Iron Ingot.");
            next;
            return;
        }
        delitem IronIngot, 1;
        Zeny = Zeny + 8000; // Real worth: 2820 GP + 2400 (ship fee) = 5220 gp poll (the plus is net profit)
        getexp 1575,0;
        setq CandorQuest_Tolchi, 3;
        speech S_FIRST_BLANK_LINE,
        l("Thanks mate, that is everything I need! Have a good day!");
    }
    if (@q == 1)
    {
        if (countitem(Coal) < 3)
        {
            mesn;
            mesq l("You don't have the three Coal lumps.");
            next;
            return;
        }
        delitem Coal, 3;
        Zeny = Zeny + 825;
        getexp 105,0;
        setq CandorQuest_Tolchi, 2;
    }
    if (@q == 0)
    {
        if (countitem("Iron Ore") == 0)
        {
            mesn;
            mesq l("You don't have the Iron Ore.");
            next;
            return;
        }
        delitem IronOre, 1;
        Zeny = Zeny + 225;
        getexp 52,0;
        setq CandorQuest_Tolchi, 1;
    }
    close;
}

// First quest description
function quest_first {
    speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("Well, it is terrible! How can I make armours if I ran out of Iron? Shall the citizens of Candor Island perish in a monster attack?!"), // NOTE: I really prefer EN_US (eg. armor vs armour)
        l("They shouldn't. Could you perhaps, kindly bring me 1 @@?", getitemlink(IronOre));
    do
    {
        //l("Do not worry, I'll seek and bring it to you."),
        select
            l("Do not worry, I have them right here."),
            l("I am not a citizen of Candor.");

        switch (@menu)
        {
            case 1:
                quest_giveitem();
                break;
            case 2:
                quest_close();
                break;
        }
    } while (@menu != 2);
    return;
}

// Second quest description
function quest_second {
    speech  S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("Thanks for helping my shop earlier. I forge weapons, but unfortunately, I need more than just iron to forge them."),
        l("Can you bring me 3 @@? Of course, you'll be rewarded.", getitemlink(Coal));
    do
    {
        // l("Hey, I like rewards. Wait me, I'll be back!"),
        select
            l("You better have it ready, because I have the Coal with me!"),
            l("Eh, that seems too problematic. Sorry.");

        switch (@menu)
        {
            case 1:
                quest_giveitem();
                break;
            case 2:
                quest_close();
                break;
        }
    } while (@menu != 2);
    return;
}

// Third quest description
function quest_third {
    speech S_FIRST_BLANK_LINE | S_LAST_NEXT,
        l("Ok, this will be my last request. The Trainer asked me for a fine weapon, to protect our village."),
        l("Problem is, I do not have the knowledge to make it without @@. If you bring me one, I'll reward you with one quarter of my commission.", getitemlink(IronIngot));
    do
    {
        // l("Do not worry, I'll be back in a jiffy."),
        select
            l("No problem is too big for me. I have them right here!"),
            l("Sorry, I am busy...");

        switch (@menu)
        {
            case 1:
                quest_giveitem();
                break;
            case 2:
                quest_close();
                break;
        }
    } while (@menu != 2);
    return;
}

// Make Tolchi Arrows (not cost-effective)
function tolchi_arrows {
    // Price: 120~200, with 25 GP discount per task done (max. 3 tasks atm)
    .@price=max(120, 200-(@q*25));
    mesn;
    mesq l("Well, I can sell you a random box with almost 100 @@ for just @@ GP.", getitemlink(TolchiArrow), .@price);
    next;
    if (Zeny < .@price || askyesno() == ASK_NO) {
        mesc l("Come back when you are willing to spend money.");
        next;
    } else {
        mes "";
        inventoryplace TolchiArrow, 110;
        Zeny=Zeny-.@price;
        getitem TolchiArrow, rand2(102,106);
        mesn;
        mesq l("Here you go. Uhm, I really prefer if you buy with Rosen, though."); // I just don't want to add loops or inputs
        next;
    }
    return;
}

// Supreme Tweaking
function tolchi_tweaks {
    // Price: the sky is the limit
    .@price=max(120, 200-(@q*25));
    mesn;
    mes l("Hmm, no, not really. However, I can try my hand at tweaking an item options :3");
    next;
    mesn;
    mesq l("Options are %s. Any craftsman can make them. Adventurers tend to be the best craftsman in the world.", b(l("item dependant bonuses")));
    next;
    mesn;
    mesq l("Usually you can only tweak if the item already have another option, but I am Tolchi. So, which item you want to tweak?");
    next;

    // Request and confirm
    .@id=requestitemindex();
    if (!csys_Confirm(.@id))
        return false;

    // Find numeric ID
    delinventorylist();
    getinventorylist();
    .@handle=@inventorylist_id[.@id];

    // Invalid (absolutely should never happen)
    if (.@handle < 1) {
        mesn;
        mesq l("...What? Which item? Sorry, too much smoke around here.");
        next;
        return;
    }
    // Multiple
    if (countitem(.@handle) != 1) {
        mesn;
        mesq l("Sorry, but you have multiple %s.", getitemlink(.@handle));
        next;
        return;
    }
    // Permission NG: (Not Granted)
    if (array_find(.disallow_equip, .@handle) >= 0) {
        mesn;
        mesq l("Sorry, but I won't dare touch a %s.", getitemlink(.@handle));
        next;
        mesn;
        mesq l("Depending on the case, Nicholas, in Hurnscald, can do a better job than me.");
        next;
        return;
    }
    // Aleady slotted
    if (getitemoptionidbyindex(.@id, 0) > 0) {
        mesn;
        mesq l("I'm not going to try to improve this masterpiece. Look its options!");
        next;
        return;
    }

    // Calculate price
    .@lv=getiteminfo(.@handle, ITEMINFO_ELV);
    .@price=.@lv**3; // Cubic function to determine price.
    // Lv 1: 3 GP | Lv 10: 1kGP | Lv 20: 8kGP | Lv 40: 64kGP | Lv 60: 216kGP
    // Lv 80: 512k GP | Lv 100: 1 Million GP!
    if (isequipped(.@handle)) {
        mesn;
        mesq l("Please unequip your %s first.", getitemlink(.@handle));
        close;
    }
    mesn;
    mesc l("Tweaking: %s", getitemlink(.@handle));//getinvindexlink(.@id));
    mesq l("That will be %s GP. Are you sure?", col(format_number(.@price), 1));
    next;

    if (Zeny < .@price || askyesno() == ASK_NO) {
        mesc l("Come back when you are willing to spend money.");
        next;
    } else {
        mes "";
        // Okay, time to force a tweaking
        Zeny=Zeny-.@price;

        // Options which Tolchi knows
        deletearray(@csys_attr);
        setarray @csys_attr, 0,
                IOPT_RICHNESS, 5,
                IOPT_EXPGAIN, 9,
                VAR_HPACCELERATION, 37,
                VAR_MAXHPAMOUNT, 49;

        // Armor fix
        if (getiteminfo(.@handle, ITEMINFO_TYPE) == IT_ARMOR) {
            csys_ArmorFix(.@handle);
            .@lv=max(1, .@lv/4);
        } else {
            csys_WeaponFix(.@handle);
        }

        // Select a bonus and purge it
        .@vartp=relative_array_random(@csys_attr);

        // Safety check
        if (.@vartp < 1)
            .@vartp=relative_array_random(@csys_attr);

        // First time player bonus
        if (!#FIRST_TIME) {
            .@vartp=IOPT_RICHNESS;
        }

        // Apply the bonus
        .@bonus=csys_Multiplier(.@lv);
        .@bonus=limit(1, .@bonus, 25);
        if (.@bonus > 5)
            .@bonus=rand2(5, .@bonus);
        if (.@vartp == VAR_MAXHPAMOUNT)
            .@bonus+=rand2(5, 30);
        setitemoptionbyindex(.@id, 0, .@vartp, .@bonus);

        // First time item bonus
        if (!#FIRST_TIME) {
            if (TOP3AVERAGELVL() > 50) {
                .@vartp=IOPT_EXPGAIN;
                //.@bonus=csys_Multiplier(.@lv);
                .@bonus=limit(10, (TOP3AVERAGELVL()/2)-20, 30);
                setitemoptionbyindex(.@id, 1, .@vartp, .@bonus);
            }
            #FIRST_TIME=true;
        }

        .@mylevel=BaseLevel*(REBIRTH+1) + max(1, .@lv);
        if (.@mylevel > 30)
            setitemoptionbyindex(.@id, 3, IOPT_WALKSPEED, rand2(-10, -5));
        mesn;
        mesc l("Well, here you are. I hope you enjoy ^^", 3);
        next;
    }
    return;
}

OnTimer1000:
    domovestep;

OnInit:
    initpath "move", 44, 35,
             "dir", UP, 0,
             "wait", 30, 0,
             "move", 43, 40,
             "dir", UP, 0,
             "wait", 30, 0,
             "move", 40, 35,
             "dir", UP, 0,
             "wait", 30, 0,
             "dir", LEFT, 0,
             "wait", 30, 0,
             "move", 42, 37,
             "dir", DOWN, 0,
             "dir", UP, 0,
             "wait", 30, 0,
             "move", 45, 37,
             "dir", DOWN, 0,
             "wait", 30, 0,
             "move", 33, 37,
             "dir", LEFT, 0,
             "wait", 30, 0,
             "move", 33, 37,
             "dir", RIGHT, 0,
             "wait", 30, 0;

    initialmove;
    initnpctimer;
    .distance = 5;

    setarray .disallow_equip, 0, SponsorNecklace, DiscordNecklace, Event1HSword, Event2HSword, EventBow, EventWand, Lightbringer, DemureAxe, Tyranny, Runestaff, AegisShield, BlacksmithAxe, Dustynator, SaviorShield, SaviorArmor, SaviorPants, SaviorBoots, Skypiercer, CreasedShirt, CreasedShorts;
    end;
}
