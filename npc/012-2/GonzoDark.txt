// TMW2 Script
// Author:
//  Jesusalva
// Description:
//      Monthly Sponsor Quest
// Variable:
//      SQuest_Sponsor
//      Quest ID: 2

012-2,50,81,0	script	GonzoDark Sponsor	NPC_LUCAS,{
    mesn;
    mesq lg("Yo, girl.", "Yo, man.");
    next;
    mesn;
    mesq l("I sponsor the alliance and all I got was a NPC and access to this ugly room.");
    next;
    mesn;
    mesq l("Well, I don't want to be an snob.");
    close;

OnInit:
    .sex = G_MALE;
    .distance = 5;
    end;
}
