// TMW2 Scripts
// Author:
//   The Mana World Brazil
// Description:
//   Traps.

001-4,275,204,0	script	#001-4_275x204	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_275x204", NPC_TRAP;
    end;
}

001-4,260,182,0	script	#001-4_260x182	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_260x182", NPC_TRAP;
    end;
}

001-4,231,173,0	script	#001-4_231x173	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_231x173", NPC_TRAP;
    end;
}

001-4,199,171,0	script	#001-4_199x171	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_199x171", NPC_TRAP;
    end;
}

001-4,200,144,0	script	#001-4_200x144	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    @object_name = .name$;
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_200x144", NPC_TRAP;
    end;
}

001-4,247,127,0	script	#001-4_247x127	NPC_TRAP,0,0,{
    @map$ = "001-4";
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_247x127", NPC_TRAP;
    end;
}

001-4,265,135,0	script	#001-4_265x135	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_265x135", NPC_TRAP;
    end;
}

001-4,271,110,0	script	#001-4_271x110	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_271x110", NPC_TRAP;
    end;
}

001-4,190,30,0	script	#001-4_190x30	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_190x30", NPC_TRAP;
    end;
}

001-4,163,33,0	script	#001-4_163x33	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_163x33", NPC_TRAP;
    end;
}

001-4,177,49,0	script	#001-4_177x49	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer5000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_177x49", NPC_TRAP;
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_177x49", NPC_TRAP;
    end;
}

001-4,164,89,0	script	#001-4_164x89	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_164x89", NPC_TRAP;
    end;
}

001-4,130,84,0	script	#001-4_130x84	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_130x84", NPC_TRAP;
    end;
}

001-4,126,47,0	script	#001-4_126x47	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_126x47", NPC_TRAP;
    end;
}

001-4,130,32,0	script	#001-4_130x32	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_130x32", NPC_TRAP;
    end;
}

001-4,107,33,0	script	#001-4_107x33	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_107x33", NPC_TRAP;
    end;
}

001-4,33,141,0	script	#001-4_33x141	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_33x141", NPC_TRAP;
    end;
}

001-4,82,149,0	script	#001-4_82x149	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_82x149", NPC_TRAP;
    end;
}

001-4,42,118,0	script	#001-4_42x118	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_42x118", NPC_TRAP;
    end;
}

001-4,24,106,0	script	#001-4_24x106	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_24x106", NPC_TRAP;
    end;
}

001-4,79,163,0	script	#001-4_79x163	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_79x163", NPC_TRAP;
    end;
}

001-4,42,175,0	script	#001-4_42x175	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_42x175", NPC_TRAP;
    end;
}

001-4,26,184,0	script	#001-4_26x184	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_26x184", NPC_TRAP;
    end;
}

001-4,32,198,0	script	#001-4_32x198	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_32x198", NPC_TRAP;
    end;
}

001-4,102,275,0	script	#001-4_102x275	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_102x275", NPC_TRAP;
    end;
}

001-4,128,274,0	script	#001-4_128x274	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_128x274", NPC_TRAP;
    end;
}

001-4,130,256,0	script	#001-4_130x256	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_130x256", NPC_TRAP;
    end;
}

001-4,128,220,0	script	#001-4_128x220	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_128x220", NPC_TRAP;
    end;
}

001-4,156,212,0	script	#001-4_156x212	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_156x212", NPC_TRAP;
    end;
}

001-4,166,246,0	script	#001-4_166x246	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_166x246", NPC_TRAP;
    end;
}

001-4,176,270,0	script	#001-4_176x270	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_176x270", NPC_TRAP;
    end;
}

001-4,160,270,0	script	#001-4_160x270	NPC_TRAP,0,0,{
    mesn strcharinfo(0);
    mesq l("Something seems off with that!");
    close;

OnTouchNPC:
OnTouch:
    SteelTrap();
    end;

OnTimer10000:
    stopnpctimer;
    setnpctimer 0;
    setnpcdisplay "#001-4_160x270", NPC_TRAP;
    end;
}

