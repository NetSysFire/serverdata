// This file is generated automatically. All manually added changes will be removed when running the Converter.
// Map 007-2: Tulimshar Volcano Underground conf

007-2,48,32,0	script	#007-2_48_32	NPC_CHEST,{
	TreasureBox();
	specialeffect(.dir == 0 ? 24 : 25, AREA, getnpcid()); // closed ? opening : closing
	close;
OnInit:
	.distance=2;
	end;
}

007-2,65,66,0	script	#007-2_65_66	NPC_HIDDEN,3,0,{
	end;
OnTouch:
	doevent "#DungeonCore::OnHeat";
	end;
}

007-2,54,26,0	script	#007-2_54_26	NPC_HIDDEN,0,1,{
	end;
OnTouch:
	doevent "#DungeonCore::OnHeat";
	end;
}
