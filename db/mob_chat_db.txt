// Monster Chat Database
//
// Structure of Database:
// Line_ID,Color_Code,Dialog

1,0x000000,Feed me cookies!
2,0x000000,Everything in this world will be mine!
3,0x000000,Weaklings, you are all a bunch of weaklings!
4,0x000000,You shall not resist!
5,0x000000,Come back here, coward weaklings!
6,0x000000,Thou shalt not survive!
7,0x000000,Come forth, my mouboos, and take back our world!
8,0x000000,Stop fleeing, puny mortals!
9,0x000000,Face my wrath!
10,0x000000,You dare to raise arms against me?! Perish!
